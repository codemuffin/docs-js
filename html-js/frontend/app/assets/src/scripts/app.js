import { init } from './app/boot/init.js';
import { uiLoader } from './app/ui/loading.js';

uiLoader();
init();
