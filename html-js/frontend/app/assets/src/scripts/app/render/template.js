import { getJQEl } from './../data/data-actions.js';

// Templates

/**
 * Templating engine
 *
 * @param {string} 	templateId	Identifier for the template. This refers to an existing container that has the attribute `data-template={templateId}` (the container will not be included in the generated HTML string)
 * @param {object}	data		Array object containing tag/replacement key/value pairs
 */
export function template(templateId, data)
{
	// Handle errors
	templateErrorHandling( templateId, data );

	const templateContainer = getJQEl('templates').find('[data-template="' + templateId + '"]');

	if (templateContainer.length === 0)
	{
		console.warn('[error_template] Invalid templateId');
		return;
	}

	// Get the template by data attr
	let templateHtml = getJQEl('templates').find( '[data-template="' + templateId + '"]' ).html();

	// Use the provided data
	$.each( data, ( tag, replacement ) =>
	{
		tag = '{{' + tag + '}}';

		// Ensure the replacement string exists within the template HTML
		if( templateHtml.indexOf( tag ) !== -1 )
		{
			templateHtml = templateHtml.replace( new RegExp( tag, 'g' ), replacement );
		}
	});

	return templateHtml;
}

/**
 * Error handling
 *
 * @TODO: This requires further testing
 */
export function templateErrorHandling(templateId, data)
{
	if( !templateId || !data )
	{
		console.warn( '[TEMPLATE_ERROR] Data passed to the Template method was undefined or null. Below is the passed template identifier and data array object, respectively:' );
		console.log( templateId );
		console.log( data );

		return ''; // return an empty string, to prevent errors in functions recieving this data
	}
}