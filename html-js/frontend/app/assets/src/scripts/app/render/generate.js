import { filterMembersExcerpt, generateTermClasses, constructRestRoute } from './../rest/rest-utilities.js';
import { autoExcerpt } from '../utils/content.js';
import { getAppOptions, getAppScreens } from './../data/data-actions.js';
import { hexFromString, dateToYMD } from '../utils/string.js';
import { template } from './template.js';
import { getFeaturedImageFromPostData } from '../rest/rest-utilities.js';
import { setDefault, sortObjectByKey } from '../utils/object.js';
import { getTerms, getPostsByType } from '../data/data-rest.js';
import { setAppGenerated } from '../data/data-state.js';
import { updateScreenHtml } from '../ui/screens.js';
import { termClick } from '../ui/posts.js';
import { getScreenByPostType } from '../data/data-screens.js';

/**
 * Generate an archive screen's HTML (post list + term buttons)
 *
 * @param   {string}  requestType  Type of REST request (posts/terms)
 * @param   {string}  screenId     The target screen ID. This will probably be the archive screen that originally requested the data fetch
 * @param   {string}  taxonomy     Taxonomy term
 * @param   {string}  postType     Post type name
 */
export function generateArchiveHtml(requestType, screenId, taxonomy, postType)
{
	switch (requestType)
	{
		case 'posts':

			updateScreenHtml(screenId, '.post-list', generatePostListHtml(screenId, postType, taxonomy));
			setAppGenerated('archives', postType, true);
			break;

		case 'terms':

			updateScreenHtml(screenId, '.term-filters-items', generateTermButtonsHtml(screenId, taxonomy));
			setAppGenerated('termBtns', taxonomy, true);

			// Set up term filter click events
			if (requestType === 'terms' && taxonomy)
			{
				termClick(screenId);
			}

			break;
	}
}

/**
 * Generates term buttons HTML, using Template
 */
export function generateTermButtonsHtml(screenId, taxonomy)
{
	// Extra validation, as some post types don't support taxonomies
	if (taxonomy)
	{
		// The required data should now be available in the fetched object
		const terms = getTerms(taxonomy);
		const termsSorted = sortObjectByKey(terms, 'name');

		// Set up the HTML output
		const termBtns = [];

		// Add an 'ALL' button to the start of the output
		termBtns.push(template('all-terms-filter-button', { screenId }));

		termsSorted.forEach(term =>
		{
			// Ignore terms with 0 attached posts
			if( term.count > 0 )
			{
				// description, link, parent (#unused)
				const { id, count, name, slug, taxonomy } = term;
				termBtns.push(template('term-filter-button', { count, id, name, screenId, slug, taxonomy }));
			}
		});

		return termBtns.join('\n');
	}
	else
	{
		return '';
	}
}

/**
 * Generates a post list, for an archive screen
 *
 * Accepts a target screenID. If this isn't passed, the current screen
 * is used. Same applies to taxonomy.
 *
 * @TODO: Support for post lists with no taxonomy
 *
 * @param   {number}  screenId  Target screen ID
 * @param   {string}  postType  Post type, for this post list
 * @param   {string}  taxonomy  Taxonomy, for this post list
 */
export function generatePostListHtml(screenId, postType, taxonomy)
{
	// The required data should now be available in the fetched object
	const posts = getPostsByType(postType);
	const postsSortedArr = sortObjectByKey(posts, 'title', 'rendered');

	// Override the REST API base for the taxonomy 'category'
	taxonomy = ( taxonomy === 'category' ) ? 'categories' : taxonomy;

	// Set up the HTML output
	const postListHtml = [];

	// Generate and add each single post HTML to the main output HTML
	postsSortedArr.forEach(p => postListHtml.push(generateSinglePostHtml(p, p.id, taxonomy, screenId)));

	return postListHtml.join('\n');
}

function generateSinglePostHtml(postData, postId, taxonomy, screenId)
{
	if (!postData.hasOwnProperty('content'))
	{
		console.warn( `[error_generateSinglePostHtml] postData has no content. Is this valid post data?`, postData );
		return false;
	}

	const host = getAppOptions('ajax', 'host');

	// Post colour (for the grid view)
	const hexColor = hexFromString( postData.title.rendered );

	// Dates (created/modified)
	// const cDate = new Date(postData.date);
	// const mDate = new Date(postData.modified);
	// const dateCreated  = `${cDate.getDate()} - ${cDate.getMonth()} - ${cDate.getFullYear()}`;
	// const dateModified = `${mDate.getDate()} - ${mDate.getMonth()} - ${mDate.getFullYear()}`;
	const dateCreated  = dateToYMD(postData.date);
	const dateModified = dateToYMD(postData.modified);

	// Featured images
	let featuredImage = getFeaturedImageFromPostData(postData);
	let featuredImageUrl = (featuredImage) ? featuredImage.large : ''; //@TODO: Probably just use medium size?
	let featuredImageCls = (featuredImage) ? 'has-image' : 'no-image';
	let inlineBgCss      = (featuredImage) ? `background-image: url('${featuredImageUrl}');` : '';

	// Terms
	let termsArr = postData[taxonomy];
	let terms = (termsArr ? termsArr.join(',') : '');
	let termClasses = (termsArr ? generateTermClasses(termsArr, taxonomy) : '');

	// Excerpt
	let excerpt = '';

	if ( postData.hasOwnProperty( 'excerpt' ) )
	{
		// Support for 'PMPro Members' plugin (restricted post)
		excerpt = filterMembersExcerpt(postData.excerpt.rendered);
		excerpt = autoExcerpt(excerpt);
	}

	// Other custom strings
	let editLink = `${host}/wp-admin/post.php?post=${postData.id}&action=edit`;

	// Build the template for each single post
	const postItemHtml = template( 'list-post-item',
	{
		'taxonomy'		: taxonomy,

		'id'			: postData.id,
		'title'			: postData.title.rendered,
		'content'		: postData.content.rendered,
		'postType'		: postData.type,
		'link'			: postData.link,

		terms,
		termClasses,
		excerpt,
		editLink,
		dateCreated,
		dateModified,
		hexColor,
		screenId, //@TODO: Is this required?
		inlineBgCss,

		// Featured Image (if applicable)
		'featuredImageUrl': featuredImageUrl,
		'featuredImageCls': `post-item-featuredimage--${featuredImageCls}`,

		/*
		// List of unused properties
		'acf'			: postData.acf, // This is a full object. We would actually need properties within this object
		'featuredMedia' : postData.featured_media, // USed by getFeaturedImageFromPostData()
		'dateGmt'		: postData.date_gmt,
		'modifiedGmt'	: postData.modified_gmt,
		'slug'			: postData.slug,
		'status'		: postData.status,
		'template'		: postData.template,
		'taxonomy'		: postData.taxonomy,

		// Posts only (unless support is added to your CPTs)
		'sticky'		: (postData.sticky ? postData.sticky : false),
		'author'		: postData.author,
		'tags'			: postData.tags,
		'tags'			: postData.format,
		*/
	});

	return postItemHtml;
}

// Returns an HTML string
export function generatePostViewHtml(data, postId)
{
	// Error handling
	if( !data )
	{
		console.warn(`[error_generatePostViewHtml] Tried to render post ID ${postId}, but no data was available. Have the posts loaded yet?`, { data, postId });
		return;
	}

	// Set up variables
	const title		= data.title.rendered;
	const content	= data.content.rendered;
	const link 		= data.link;
	const postID	= data.id;
	const host		= getAppOptions('ajax', 'host');
	const type      = data.type;

	const screen = getScreenByPostType(type);

	// Used for the "View X" buttons, e.g. "View Resource". Only applies to a few post types
	const hasNameSingle = ( screen.hasOwnProperty( 'nameSingle' ) && typeof screen.nameSingle === 'string' && screen.nameSingle !== '' );
	const typeNiceName = hasNameSingle ? screen.nameSingle : type.charAt( 0 ).toUpperCase() + type.slice( 1 );

	// ACF-dependant vars
	let widthClass  = 'post-view-maxwidth';
	let viewBtn = '';
	let scriptTag = '';
	let styleTag = '';

	// ACF
	if (data.acf)
	{
		let btnText = `View ${typeNiceName}`;

		// ACF: Links
		const { infobox_primary_link_url, infobox_primary_link_text, infobox_secondary_link_url, infobox_secondary_link_text } = data.acf;

		// ACF: Primary Link
		if ( infobox_primary_link_url )
		{
			viewBtn += template( 'acf-btn-primary', {
				url: infobox_primary_link_url,
				btnText: ( infobox_primary_link_text ) ? infobox_primary_link_text : btnText
			})
		}

		// ACF: Secondary Link
		if ( infobox_secondary_link_url )
		{
			viewBtn += template( 'acf-btn-secondary', {
				url: infobox_secondary_link_url,
				btnText: ( infobox_secondary_link_text ) ? infobox_secondary_link_text : btnText
			})
		}

		// viewBtn  = ( infobox_primary_link_url ) ? template('quick-post-acf', { resourceLink:data.acf.infobox_primary_link, resourceType:typeNiceName }) : '';
	}

	// Generate HTML
	const htmlContent = template('quick-post-content', { title, content, scriptTag, styleTag, widthClass });
	const htmlMeta    = template('quick-post-meta',    {}); // @TODO: Add meta info to quick posts (date modified, etc)
	let   htmlButtons = template('quick-post-buttons', { link, postID, host });

	// Create the HTML string
	// return [...htmlContent, htmlMeta, viewBtn, htmlButtons].join(''); //@TODO: weird "hole" glitch??
	return htmlContent.concat(htmlMeta, viewBtn, htmlButtons);
}

export function generateNav()
{
	const screens = getAppScreens();

	let htmlItems = '';
	let usedGroupTitles = []; // prevents duplicate gruop titles
	const hiddenClass = (getAppOptions('debug', 'showHiddenNavItems') ? '' : 'uk-hidden');

	for (const item in screens)
	{
		const navItem = screens[item];

		// Generate a single group title per group
		if (!usedGroupTitles.includes(navItem.group))
		{
			// This group title has been used! Let's make a note of that :)
			usedGroupTitles.push(navItem.group);

			// Generate the HTML, and append it to the main HTML
			htmlItems += template( 'nav-group-title', {
				'group'       : navItem.group,
				'devClass'    : (navItem.dev)    ? 'developers-only' : '',
				'hiddenClass' : (navItem.hidden) ? hiddenClass : '',
			});
		}

		const navItemHtml = template( 'nav-item',
			{
				'name' 			: navItem.name,
				'url'			: navItem.url,
				'color'			: navItem.color,

				'isDev'			: (navItem.dev)    ? 'true' : 'false',
				'devClass'		: (navItem.dev)    ? 'developers-only' : '',
				'hiddenClass'   : (navItem.hidden) ? hiddenClass : '',

				// 'group'			: navItem.group,
				// 'screenType' 	: navItem.screenType,
				// 'screenId'		: navItem.screenId,
				// 'postType' 		: navItem.postType,
				// 'archive' 		: navItem.archive,
				// 'taxonomy'		: navItem.taxonomy,
			});

		htmlItems += navItemHtml;
	}

	let htmlOutput = `
		<nav class="nav-main">
			<ul class="uk-list">
				${htmlItems}
			</ul>
		</nav>
	`;

	const $navContainer = $('.sidebar-nav');

	$navContainer.append(htmlOutput);
}

export function generateArchiveScreensHtml()
{
	const screens = getAppScreens();
	const archiveScreens = Object.values(screens).filter(s => s.screenType === "archive");

	let htmlOutput = '';

	for (const item in archiveScreens)
	{
		const screen = archiveScreens[item];
		const screenHtml = template('archive-screen', {
			'screenId': screen.screenId,
			'color':    screen.color
		});
		htmlOutput += screenHtml;
	}

	const $screensContainer = $('.app-screens');

	$screensContainer.append(htmlOutput);
}

export function generatePreloaderTableRows(screens, emptyTaxonomyHtml)
{
	let tableRows = [];

	// Loop through the screens, generate their table row HTML, using their set post type name and taxonomy term
	for (let s in screens)
	{
		// Exclude non-archive screens
		if (screens[s].screenType !== 'archive')
		{
			continue;
		}

		const name 		= screens[s].name; // screen name
		const url 		= screens[s].url; // screen URL
		let   postType 	= screens[s].postType;
		let   taxonomy 	= screens[s].taxonomy;
		// const archive 	= screens[s].archive;
		const color 	= screens[s].color; //#UNUSED
		const group 	= screens[s].group; //#UNUSED
		const dev       = screens[s].dev; //#UNUSED
		const postTypeActual = screens[s].postTypeActual; // can be used instead of the postType when assigning classes

		// Build some links!
		const screenUrl = `${window.location.origin}/${url}`;
		const postsUrl = constructRestRoute('posts', postType);
		const termsUrl = constructRestRoute('terms', taxonomy);

		const screenLink   = `<a href="${screenUrl}" data-app-screen-link>${name}</a>`;
		const postTypeLink = `<a href="${postsUrl}" target="_blank">${postType}</a>`;
		const taxonomyLink = (taxonomy ? `<a href="${termsUrl}" target="_blank">${taxonomy}</a>` : emptyTaxonomyHtml);

		// Support for rest bases that aren't just the post type name
		postType = setDefault(postTypeActual, postType);
		// postType = getNativeRestBaseFromPostType(postType);

		// HTML for single table output row
		let htmlTableRow = template('preload-posts-and-terms', { postType, name, taxonomy, color, group, postTypeLink, taxonomyLink, screenLink, dev });

		tableRows.push(htmlTableRow);
	}

	return tableRows.join();
}
