import { $app, getAppOptions, getAppScreens, } from './../data/data-actions.js';
import { getAllPostsFlat } from './../data/data-rest.js';
import { clearInputValue } from './../utils/elements.js';
import { generatePostListHtml } from '../render/generate.js';
import { loadingNotice, toggleLoaderNotice } from '../ui/loading.js';

const selectors = {
	'input'	: '.js-post-search-term',
	'submit': '.js-post-search-submit',
	'reset'	: '.js-post-search-reset',
	'count'	: '.js-post-search-count'
};

const searchScreenId = 'search';
const $searchScreen = $app.find(`[data-app-screen-id="${searchScreenId}"]`);

/**
 * Listener: Triggered when the preloader has loaded all posts data
 * @TODO: Replace with a promise/deferred in appData
 */
export function setupSearchPage()
{
	const preloadEnabled = getAppOptions('preload', 'enabled');

	setupEmptySearchPage(preloadEnabled);

	if (preloadEnabled)
	{
		$app.on('app:postsandtermsready', () =>
		{
			updateSearchPageHtml();
		});
	}
}

export function setupEmptySearchPage(preloadEnabled)
{
	const screenId = 'search';

	if (preloadEnabled)
	{
		loadingNotice('posts', 'searchPageWaiting', screenId);
	}
	else
	{
		loadingNotice('posts', 'searchUnavailablePreloadDisabled', screenId);
	}
}

/**
 * Set up the "Search All" page
 */
export function updateSearchPageHtml()
{
	showAllPostsOnSearchPage();
	searchPageButtonEventListeners();
}

/**
 * Populate search page, with every fetched post
 *
 * @param {object} postsData
 */
export function showAllPostsOnSearchPage()
{
	// Generate the post list HTML, and apply it
	let allPostsHtml = [];

	// Use all post types
	// const postTypes = getPostTypes();

	// Get all non-dev post types
	const nonDevPostTypes = Object.values(getAppScreens()).filter(s => s.screenType === 'archive' && s.dev === false).map(ndas => ndas.postType).sort();

	// Generate the post list HTML, for every post type
	// The archive pages use the same function, but just do this once
	nonDevPostTypes.forEach(postType =>
	{
		allPostsHtml.push(generatePostListHtml('search', postType));
	});

	$searchScreen.find('.post-list').html(allPostsHtml.join(''));

	toggleLoaderNotice('posts', false, 'search');
}

/**
 * Search page input click events
 */
export function searchPageButtonEventListeners()
{
	$app.on('blur',  selectors.input,  () => searchPageSubmitted() ); //@TODO: Replace with a proper "on tab out" event (on key press 'tab')
	$app.on('mousedown', selectors.submit, () => searchPageSubmitted() );
	$app.on('mousedown', selectors.reset,  () => resetSearchPageItems() );
}

export function searchPageSubmitted()
{
	// const $searchTermInput = ;
	const searchTerm = $(selectors.input).val();
	let count = 0;

	const flatPosts = getAllPostsFlat();

	if (searchTerm)
	{
		const searchTermUpper = searchTerm.toUpperCase();

		// Array to store the IDs of posts that match the search criteria
		let foundPostsIds = [];

		for (const postId in flatPosts)
		{
			const post = flatPosts[postId];
			const title = post.title.rendered.toUpperCase();
			const content = post.content.rendered.toUpperCase();

			const foundInTitle = title.includes(searchTermUpper);
			const foundInContent = content.includes(searchTermUpper);

			if (foundInTitle || foundInContent)
			{
				foundPostsIds.push(post.id);
			}
		}

		if (foundPostsIds.length)
		{
			filterSearchPageItems(foundPostsIds);

			count = foundPostsIds.length;
		}

		updateFoundPostsCount(count, searchTerm);
	}
	else
	{
		count = 0;

		// Empty search term, so reset the hide filters
		resetSearchPageItems();
	}
}

/**
 * Hide any posts with an ID which is not the array
 *
 * @param {array} foundPostsIds Array of post IDs
 */
export function filterSearchPageItems(foundPostsIds)
{
	unhideSearchPosts();

	const $searchPostItems = $searchScreen.find('.post-item');

	$searchPostItems.each((i, postItem) =>
	{
		const $postItem = $(postItem);
		const postId = parseInt($postItem.attr('data-post-id'));

		if (!foundPostsIds.includes(postId))
		{
			$postItem.parent('.post-item-container').hide();
		}
	});
}

function updateFoundPostsCount(count, searchTerm)
{
	$(selectors.count).text(`${count} results for "${searchTerm}"`);
}

function removeFoundPostsCount()
{
	$(selectors.count).empty();
}

/**
 * Unhide all filtered posts
 */
export function unhideSearchPosts()
{
	$searchScreen.find('.post-item-container').show();
}

/**
 * Clear the search page
 */
export function resetSearchPageItems()
{
	unhideSearchPosts();
	removeFoundPostsCount();
	clearInputValue(selectors.input);
}