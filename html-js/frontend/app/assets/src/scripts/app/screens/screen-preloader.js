import { setDefault } from './../utils/object.js';
import { fetchRest } from '../rest/rest-api.js';
import { hideAllElementsExcept } from './../utils/elements.js';
import { getAppOptions, getAppFetched } from './../data/data-actions.js';
import { $app } from '../data/data-actions.js';
import { generatePreloaderTableRows } from '../render/generate.js';
import { constructRestRoute } from '../rest/rest-utilities.js';
import { setupFetchPromises } from '../data/data-promises.js';
import { clearRestCache } from '../data/data-cache.js';
import { updateScreenHtml } from '../ui/screens.js';
import { getNativePostTypeFromRestBase } from '../utils/wp-posts.js';

//@TODO: Update to use promises

const preloaderScreenId = 'preloader';

export const preloadData = {
	'posts': {},
	'terms': {},
	'types': {},
	'taxonomies': {},
	'postsArr': [],
	'termsArr': [],
};

//@TODO: Move to appData.options.classes
const jqEls = {
	'status' : $('.preload-status-update'),
	'buttons': {
		'retryConditional'	: $('.js-preload-retry--conditional'),
	},
	'tables': {
		'typesAndTaxes' 	: $('.preloader-list-available-types-and-taxes'),
		'postsAndTerms' 	: $('.preloader-list-available'),
		'postsAndTermsTbody': $('.preloader-list-available-tbody'),
	},
};

const els = {
	'status' : '.preload-status-update',
	'buttons': {
		'clear'				: '.js-preload-clear',
		'fetch'				: '.js-preload-retry',
		'log'				: '[data-app-button="log-preload-data"]',
		'retryConditional'	: '.js-preload-retry--conditional',
	},
	'progress': {
		'postsCount' 		: '.preload-total-items-posts',
		'termsCount' 		: '.preload-total-items-terms',
		'postsComplete' 	: '.preload-total-complete-posts',
		'termsComplete' 	: '.preload-total-complete-terms',
		'postsErrors'		: '.preload-total-errors-posts',
		'termsErrors'		: '.preload-total-errors-terms',
	},
	'tables': {
		'typesAndTaxes' 	: '.preloader-list-available-types-and-taxes',
		'postsAndTerms' 	: '.preloader-list-available',
		'postsAndTermsTbody': '.preloader-list-available-tbody',
	},
};

const preloadStrings = {
	'waitingHtml': '<span class="preload-text-waiting">?</span>',
};

export function setupPreloader()
{
	if (getAppOptions('preload', 'enabled'))
	{
		setupPreloadHtml();	// Sets up the tables, which show progress and errors - Moved to screens setup
		preloaderButtons();	// Preloader UI buttons
	}
	else
	{
		// Used when preload is completely disabled
		hideAllElementsExcept('.preload-enabled', '.preload-disabled');
	}
}

/**
 * Set up the Preloader screen. Runs every time the screen is visited
 *
 * @param   {boolean}  autoInit  Start the preloader automatically? Defaults to the setting in the app config (preload.autoInit)
 */
export function setupPreloadHtml(autoInit)
{
	// Reset the progress data
	resetProgressDataCounts();

	// Autoinit?
	autoInit = setDefault(autoInit, getAppOptions('preload', 'autoInit'));

	// Get the list of posts to preload, from the configured screens
	const screens = getAppOptions('screens');

	// Set up array of post types and terms, to preload
	let postsArr = [];
	let termsArr = [];

	// Add to the types and terms arrays
	for (let s in screens)
	{
		// Exclude non-archive screens
		if (screens[s].screenType !== 'archive')
		{
			continue;
		}

		const screenId   = screens[s].screenId;
		const postType 	 = screens[s].postType;
		let taxonomy 	 = screens[s].taxonomy;

		postsArr.push( {screenId, postType, 'requestType': 'posts'} );

		// Logic for no taxonmy
		if (taxonomy && !termsArr.includes(taxonomy))
		{
			termsArr.push({ screenId, taxonomy, 'requestType': 'terms' });
		}
	}

	Object.assign(preloadData.postsArr, postsArr);
	Object.assign(preloadData.termsArr, termsArr);

	// Generate table rows
	const htmlTableRows = generatePreloaderTableRows(screens, '<span class="preload-text-waiting">-</span>');
	jqEls.tables.postsAndTermsTbody.empty().html(htmlTableRows);
	// $(els.tables.postsAndTermsTbody).empty().html(htmlTableRows);

	// If no taxonomy is specified, display some visual feedback
	$('.get-terms-state-false').html('NA');
	$('.get-terms-count-false').html('NA');

	// Update main status
	updatePreloaderStatus(autoInit ? 'readyAutoFetch' : 'readyNoAuto');

	// Pass the data to preload to a separate method
	setupPreloadGet(autoInit);
}

function setupPreloadGet(autoInit)
{
	const postsArr = preloadData.postsArr;
	const termsArr = preloadData.termsArr;

	// Hide retry button
	$(els.buttons.retryConditional).hide();

	// Set up a set of counts and error objects, for tracking when requests have been completed or error'd
	let progress =
	{
		'posts' : {
			'items': postsArr.length,
			'completed': 0,
			'errors': 0,
			'errorData': [],
		},
		'terms' : {
			'items': termsArr.length,
			'completed': 0,
			'errors': 0,
			'errorData': [],
		}
	};

	// Save progress & error objects
	Object.assign(preloadData, progress);

	// Display item counts in the Progress panel
	// @TODO: Can we refactor? This looks pretty ugly!
	updateScreenHtml(preloaderScreenId, els.progress.postsCount,    postsArr.items);
	updateScreenHtml(preloaderScreenId, els.progress.termsCount,    termsArr.items);
	updateScreenHtml(preloaderScreenId, els.progress.postsComplete, preloadStrings.waitingHtml);
	updateScreenHtml(preloaderScreenId, els.progress.termsComplete, preloadStrings.waitingHtml);
	updateScreenHtml(preloaderScreenId, els.progress.postsErrors,   preloadStrings.waitingHtml);
	updateScreenHtml(preloaderScreenId, els.progress.termsErrors,   preloadStrings.waitingHtml);

	// Defaults
	autoInit = setDefault(autoInit, true);

	// Get the data!
	preloadGet(autoInit);
}

/**
 * REST API Get
 *
 * @param   {object}  postTypes   Post types, to request
 * @param   {object}  terms       Taxonomy terms, to request
 */
function preloadGet(autoInit)
{
	const postsArr = preloadData.postsArr;
	const termsArr = preloadData.termsArr;

	// Get posts
	for (let p in postsArr)
	{
		const { screenId, postType, requestType } = postsArr[p];

		// If autoInit is disabled, only display locally cached data
		if (!autoInit)
		{
			const localPosts = getAppFetched(requestType, postType);

			if (localPosts)
			{
				preloaderFetchResolved(requestType, localPosts, postType);
			}

			continue;
		}

		fetchRest(screenId, requestType, postType, autoInit)
			.then(
				data  => preloaderFetchResolved(requestType, data, postType),
				error => preloaderFetchRejected(requestType, error, constructRestRoute(requestType, postType), error)
			).catch(e =>
			{
				console.error('[error_preloadGet:posts] Code Error:', e);
			});
	}

	// Get terms
	for (let t in termsArr)
	{
		const { screenId, taxonomy, requestType } = termsArr[t];

		// If autoInit is disabled, only display locally cached data
		if (!autoInit)
		{
			const localTerms = getAppFetched(requestType, taxonomy);

			if (localTerms)
			{
				preloaderFetchResolved(requestType, localTerms, taxonomy);
			}

			continue;
		}

		fetchRest(screenId, requestType, taxonomy, autoInit)
			.then(
				data  => preloaderFetchResolved(requestType, data, taxonomy),
				error => preloaderFetchRejected(requestType, error, constructRestRoute(requestType, taxonomy), error)
			).catch(e =>
			{
				console.error('[error_preloadGet:terms] Code Error:', e);
			});
	}
}

/**
 * Logic for single REST success
 */
export function preloaderFetchResolved(requestType, data, name)
{
	const dataArr = Object.values(data);

	// Item count
	const itemCount = dataArr.length;

	// Transform WP names into app-friendly names
	name = getNativePostTypeFromRestBase(name);

	// Selectors
	const tableClsState = `.get-${requestType}-state-${name}`;
	const tableClsCount = `.get-${requestType}-count-${name}`;

	// Update HTML
	$app.find(tableClsState).html('<span class="preloader-text-success">&check;</span>');
	$app.find(tableClsCount).html(itemCount);

	// Update progress counts (success +1)
	preloadData[requestType].completed ++;

	// Move on to error handling, if we're done!
	preloaderAfterFetch();
}

/**
 * Logic for single REST failure
 */
export function preloaderFetchRejected(requestType, response, restUrl, error)
{
	console.warn('[error_preloaderFetchRejected] An error occured:', {error, requestType, response, restUrl});

	// Update progress counts (error +1), and store the failed request data
	preloadData[requestType].errors ++;
	preloadData[requestType].errorData.push({ restUrl, response });

	// Move on to error handling, if we're done!
	preloaderAfterFetch();
}

/**
 * Handle errors (and proceed accordingly)
 */
function preloaderAfterFetch()
{
	const posts = preloadData.posts;
	const terms = preloadData.terms;

	const postsDone  = posts.items === (posts.completed + posts.errors);
	const termsDone  = terms.items === (terms.completed + terms.errors);
	const haveErrors = posts.errors > 0 || terms.errors > 0;
	const haveInvalidTotals = (posts.items < (posts.completed + posts.errors) || terms.items < (terms.completed + terms.errors));

	updateScreenHtml(preloaderScreenId, els.progress.postsCount, 	posts.items);
	updateScreenHtml(preloaderScreenId, els.progress.termsCount, 	terms.items);
	updateScreenHtml(preloaderScreenId, els.progress.postsComplete, posts.completed);
	updateScreenHtml(preloaderScreenId, els.progress.termsComplete, terms.completed);
	updateScreenHtml(preloaderScreenId, els.progress.postsErrors, 	posts.errors);
	updateScreenHtml(preloaderScreenId, els.progress.termsErrors, 	terms.errors);

	// Check for valid totals
	if (haveInvalidTotals)
	{
		console.warn(`[error_preloaderAfterFetch] We have more completed/error items than initial items!`);
	}

	// Don't continue until both terms and posts are done
	if (!postsDone || !termsDone)
	{
		return;
	}

	if( !haveErrors )
	{
		updatePreloaderStatus('success');
	}
	else
	{
		logPreloaderErrorUrls();

		const autoRetry = getAppOptions('preload', 'autoRetry');

		// Either auto-rety, or show the "conditional retry" button
		if (autoRetry)
		{
			updatePreloaderStatus('errorsAutoRetry');
			preloadRetry();
		}
		else
		{
			updatePreloaderStatus('errorsNoRetry');
			$(els.buttons.retryConditional).show();
		}
	}
}

/**
 * Log a list of error urls to the console
 */
function logPreloaderErrorUrls()
{
	const progress = preloadData;

	if (progress.posts.errors > 0)
	{
		let postErrorURls = [];

		for (let errorKey in progress.posts.errorData)
		{
			const restUrl = progress.posts.errorData[errorKey].restUrl;
			postErrorURls.push(restUrl);

			//@TODO: Make this less ugly
			$(`.preloader-list-available-tbody a[href="${restUrl}"]`).addClass('preloader-text-error');
		}

		const postErrorsString = postErrorURls.join("\n");
		console.log(`[Post Error URLs]\n${postErrorsString}`);
	}

	if (progress.terms.errors > 0)
	{
		let termErrorURls = [];

		for (let errorKey in progress.terms.errorData)
		{
			const restUrl = progress.terms.errorData[errorKey].restUrl;
			termErrorURls.push(restUrl);

			//@TODO: Make this less ugly
			$(`.preloader-list-available-tbody a[href="${restUrl}"]`).addClass('preloader-text-error');
		}

		const termErrorsString = termErrorURls.join("\n");
		console.log(`[Term Error URLs]\n${termErrorsString}`);
	}
}

/**
 * Update the status notice
 *
 * @param  {string}  statusId  ID for the status to display
 */
function updatePreloaderStatus(statusId)
{
	const $preloadStatus = jqEls.status;
	let statusHtml = '';

	switch (statusId)
	{
		case 'readyAutoFetch':
			statusHtml = '<p>Waiting for all requests to finish....</p>';
			break;

		case 'readyNoAuto':
			statusHtml = '<p>Click Fetch to begin loading data.</p>';
			break;

		case 'success':
			statusHtml = `
				<p>All done!</p>
				<p>Everything has been saved to your computer. You can now browse without loading times!</p>`;
			break;

		case 'errorsAutoRetry':
			statusHtml = '<p>Some data wasn\'t fetched. Retrying...</p>';
			break;

		case 'errorsNoRetry':
			statusHtml = `
				<p>Something went wrong!</p>
				<p>Click the Retry button to fetch any missing data.</p>`;
			break;
	}

	// Update status
	$preloadStatus.html( statusHtml );
}

export function resetProgressDataCounts()
{
	preloadData.posts.items = 0;
	preloadData.posts.completed = 0;
	preloadData.posts.errors = 0;
	preloadData.terms.items = 0;
	preloadData.terms.completed = 0;
	preloadData.terms.errors = 0;
}

/**
 * Restart the prelaoder
 *
 * Resets dynamic tables and counters, and attempts to fetch any failed requests
 *
 * @TODO| Add a retry counter, to prevent infinite loops!
 */
export function preloadRetry(autoInit)
{
	// Restart the preload process
	setupPreloadHtml(autoInit);
}

/**
 * Custom action buttons, specific to the preloader
 */
export function preloaderButtons()
{
	// Log preload data
	$(els.buttons.clear).on('click', e =>
	{
		e.preventDefault();
		resetProgressDataCounts();
		clearRestCache();
		setupFetchPromises(); // reset 'fetched' promises
		setupPreloadHtml(false);

		$('.preload-status-update').text('Preload data cleared!');
	});

	// Retry (used when fetch attempts fail)
	$(els.buttons.fetch).on('click', e =>
	{
		e.preventDefault();
		preloadRetry(true);
	});

	// Log preload data
	$(els.buttons.log).on('click', e =>
	{
		e.preventDefault();
		console.log(preloadData);
	});
}

export function logPreloadData()
{
	console.log(preloadData);
}