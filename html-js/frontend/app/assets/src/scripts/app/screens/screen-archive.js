import { toggleLoaderNotice } from './../ui/loading.js';
import { log } from '../utils/logger.js';
import { generateArchiveHtml } from '../render/generate.js';
import { loadingNotice } from '../ui/loading.js';
import { fetchRest } from '../rest/rest-api.js';
import { getPostsByType } from '../data/data-rest.js';
import { getAppPromise } from '../data/data-promises.js';
import { openPost, removeActivePostClasses } from '../ui/posts.js';
import { getAppOptions, getAppScreens, getAppCurrent } from '../data/data-actions.js';
import { isFetched, isGenerated } from '../data/data-state.js';
import { setDefault } from '../utils/object.js';
import { setScreenTo404, updateScreenHtml } from '../ui/screens.js';
import { prepareUrlPath, getUrlPathPart } from '../utils/url-string.js';
import { removePostFromHeader } from '../ui/header.js';


// Exports
// ============================================================================

/**
 * Actions for displaying a post type archive screen
 *
 * @param  {object} screenData  Screen data object
 */
export function changedScreenToArchive(screenData)
{
	removePostFromHeader();

	// Set up posts and terms (via the REST API)
	const screenID = screenData.screenId;
	const postType = screenData.postType;
	const taxonomy = screenData.taxonomy;

	setupArchiveScreen(screenID, postType, taxonomy);

	// Check for a child path, in case there's a post to open
	const currentPath = prepareUrlPath(window.location.pathname);
	const childPath   = getUrlPathPart(currentPath, 2);

	if (childPath)
	{
		setupArchivePost(childPath, postType);
	}
	else
	{
		removeActivePostClasses();
	}
}

/**
 * Set up posts and terms, for the requested screen
 */
export function setupArchiveScreen(screenId, postType, taxonomy)
{
	// Options
	const autoFetch = getAppOptions('archives', 'autoFetch');

	// States
	const isPostsFetched = isFetched('posts', postType);
	const isTermsFetched = isFetched('terms', taxonomy);
	const isArchiveGenerated  = isGenerated('archives', postType);
	const isTermBtnsGenerated = isGenerated('termBtns', taxonomy);

	if (isArchiveGenerated)
	{
		// If the archive has already been generated, there's nothing more to do
		return;
	}
	else if (isPostsFetched)
	{
		// If the posts have been fetched, but the archive hasn't been generated yet, then lets generate it
		afterArchiveFetch('posts', screenId, taxonomy, postType);
	}
	else
	{
		if (autoFetch)
		{
			// No fetched data and no generated archives. So let's fetch and generate
			loadingNotice('posts', 'postListLoading', screenId);
			fetchPosts(screenId, postType, taxonomy); // Set up this archive screen
		}
		else
		{
			loadingNotice('posts', 'autoFetchArchivesDisabled', screenId);
		}
	}

	// Some post types don't use taxonomies
	if (taxonomy)
	{
		if (isTermBtnsGenerated)
		{
			// If the term buttons have already been generated, there's nothing more to do
			return;
		}
		else if (isTermsFetched)
		{
			// If the terms have been fetched, but the term buttons haven't been generated yet, then lets generate them
			afterArchiveFetch('terms', screenId, taxonomy, postType);
		}
		else
		{
			if (autoFetch)
			{
				// No fetched data and no generated buttons. So let's fetch and generate
				loadingNotice('terms', 'termsListLoading', screenId);
				fetchTerms(screenId, taxonomy);
			}
			else
			{
				// No need to show two identical notices, so just hide the terms notice
				toggleLoaderNotice('terms', false, screenId);
			}
		}
	}
}

/**
 * Set up a single post. Triggered when the user navigates in their browser
 *
 * @param {url}  slug  Post slug
 */
export function setupArchivePost(slug, postType, updateUrl)
{
	updateUrl = setDefault(updateUrl, false);

	// Wait for all this screen's posts to be ready
	const postTypePromise = getAppPromise('posts', postType);

	if (!postTypePromise)
	{
		console.error(`[error_setupArchivePost] getAppPromise returned empty, for post type "postType"`, {slug, postType, updateUrl, postTypePromise});
	}

	postTypePromise.then(() =>
	{
		// Try to find a post with a slug to match the url
		const posts = getPostsByType(postType);

		if (posts)
		{
			const postObj = Object.values(posts).find(p => p.slug === slug);

			if (postObj)
			{
				const $showPost = $(`[data-post-id="${postObj.id}"]`);

				openPost($showPost, postObj, updateUrl);
			}
			else
			{
				console.warn(`[error_setupArchivePost_A] Found posts of type "${postType}", but no matches for the path "${slug}". Available posts:`, {posts});
				setScreenTo404(slug);
			}
		}
		else
		{
			console.warn(`[error_setupArchivePost_B] Child path is present, but no posts were found matching the postType "postType"`);
			setScreenTo404(slug);
		}
	});
}



/**
 * Actions to perform once the data has been fetched
 *
 * Note: The fetched data is saved to the 'fetched' object, so we don't need to pass
 * it to this function
 *
 * @param   {string}  requestType  Type of REST request (posts/terms)
 * @param   {string}  screenId     The target screen ID. This will probably be the archive screen that originally requested the data fetch
 * @param   {string}  taxonomy     Taxonomy term
 * @param   {string}  postType     Post type name
 */
export function afterArchiveFetch(requestType, screenId, taxonomy, postType)
{
	const autoGenerate = getAppOptions('archives', 'autoGenerate');
	const isArchiveGenerated  = isGenerated('archives', postType);
	const isTermBtnsGenerated = isGenerated('termBtns', taxonomy);

	let logMessage, logGroup, isTypeGenerated;

	switch (requestType)
	{
		case 'posts':
			logMessage = 'Post fetched. Processing...';
			logGroup = 'afterArchiveFetch: processPosts';
			isTypeGenerated = isArchiveGenerated;
			break;

		case 'terms':
			logMessage = 'Terms fetched. Processing...';
			logGroup = 'afterArchiveFetch: processTerms';
			isTypeGenerated = isTermBtnsGenerated;
			break;
	}

	if (autoGenerate && !isTypeGenerated)
	{
		// Populate the archive/term buttons
		generateArchiveHtml(requestType, screenId, taxonomy, postType);
		toggleLoaderNotice(requestType, false, screenId);
	}
	else
	{
		switch (requestType)
		{
			case 'posts':
				loadingNotice(requestType, 'autoGenerateArchivesDisabledButHasData', screenId);
				break;

			case 'terms':
				toggleLoaderNotice('terms', false, screenId);
				break;
		}
	}

	// Update logger
	log(logMessage, logGroup, 'success');

	if (requestType === 'posts')
	{
		afterPostsFetch();
	}
}

function afterPostsFetch()
{
	//@TODO
}

export function clearGeneratedHtml(screenId, type)
{
	switch (type)
	{
		case 'posts':
			updateScreenHtml(screenId, '.post-list', '');
			break;

		case 'terms':
			updateScreenHtml(screenId, '.term-filters-items', '');
			break;
	}
}

/**
 * Use pre-cached data to generate any archive screens with available post data
 *
 * #unused
 *
 * @TODO: This was likely written before a refactor. The prelaoder should handle this now. Double-check, then remove
 */
export function preloadArchives()
{
	const screens = getAppScreens();

	for (const s in screens)
	{
		if (screens.hasOwnProperty(s))
		{
			const screen = screens[s];

			// Ignore non-archives & current screen
			const isArchive = (screen.screenType === 'archive');
			const isCurrent = (screen === getAppCurrent('screen'));

			if (isArchive && !isCurrent)
			{
				setupArchiveScreen(screen.screenId, screen.postType, screen.taxonomy);
			}

		}
	}
}


// Internal Functions
// ============================================================================

/**
 *  Fetch posts
 */
function fetchPosts(screenId, postType, taxonomy)
{
	if (postType)
	{
		fetchRest(screenId, 'posts', postType)
			.then(
				() => afterArchiveFetch('posts', screenId, taxonomy, postType), // (data) =>
				error =>  console.warn('[error_fetchPosts_A] The requested data could not be retreived', error)
			).catch( e => console.warn('[error_fetchPosts_B] An error occured. This is likely a bug in the code', e));
	}
	else
	{
		loadingNotice('posts', 'postListNoPostType', screenId);
	}
}

/**
 *  Fetch terms
 */
function fetchTerms(screenId, taxonomy)
{
	if (taxonomy)
	{
		fetchRest(screenId, 'terms', taxonomy)
			.then(
				() => afterArchiveFetch('terms', screenId, taxonomy), // (data) =>
				error =>  console.warn('[error_fetchTerms_A] The requested data could not be retreived', error)
			).catch( e => console.warn('[error_fetchTerms_B] An error occured. This is likely a bug in the code', e));
	}
	else
	{
		toggleLoaderNotice('terms', false, screenId);
	}
}


