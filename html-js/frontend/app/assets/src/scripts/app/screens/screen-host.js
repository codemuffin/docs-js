import { saveDevUserOption } from '../ui/options.js';
import { getAppScreens } from '../data/data-actions.js';
import { clearLocal, setLocal } from '../storage/localStorage.js';
import { toggleScreen } from '../ui/screens.js';
import { urlToObject } from '../utils/url-string.js';

/**
 * Hides all custom screens. Used when changing the REST host.
 */
function setScreensToBasic()
{
	const screens = getAppScreens();

	for (const s in screens)
	{
		if (screens.hasOwnProperty(s))
		{
			const screen = screens[s];

			// Delete the Home and FAQs pages
			if (screen.screenId === 'dev-faqs' || screen.screenId === 'home')
			{
				delete screens[s];
				// deleteObjectProperty(screens, s);
			}

			// Delete or modify archive pages
			if (screen.screenType === 'archive')
			{
				if (screen.postType === 'posts' || screen.postType === 'pages')
				{
					// Allow posts and pages, but set them to display like normal archives
					screen.dev   = false;
					screen.color = 'green';
					screen.group = 'Content';
					screen.hidden = false;
				}
				else
				{
					delete screens[s];
					// deleteObjectProperty(screens, s);
				}
			}
		}
	}
}

/**
 * Change the data source.
 *
 * This feature was added much later, so it may be buggy.
 */
export function setupSourceScreen()
{
	const els = {
		input:  document.querySelector('.js-host-input'),
		update: document.querySelector('.js-host-update'),
		reset:  document.querySelector('.js-host-reset'),
	};

	els.update.addEventListener('click', e =>
	{
		e.preventDefault();

		const newSource = els.input.value;

		if (newSource)
		{
			// Validation: Check for protocol and host
			const protocol = urlToObject(newSource).protocol;
			const host     = urlToObject(newSource).host;

			if (!protocol || !host)
			{
				console.log('Protocol or host are missing. Please enter a full URL');
			}
			else
			{
				if (host.indexOf('.') === -1)
				{
					console.log('Host seems to be invalid, as it\'s missing a dot');
				}
				else
				{
					// Delete everything from localStorage
					clearLocal();

					// Remove the main Docs screens, and makes pages & posts display by default
					setScreensToBasic();

					// Update the host
					saveDevUserOption('ajax', 'host', newSource);

					// Enable grid & featured images
					saveDevUserOption('ui', 'post-list-grid', true);
					saveDevUserOption('ui', 'post-list-featuredimgages', true);
					saveDevUserOption('ui', 'developers-only', false);

					// Enable embeds
					saveDevUserOption('query', 'embed', true);

					// Save a 'customHost' flag
					setLocal('customHost', true);

					console.log('Host updated! Reloading...');

					// Move to posts screen, then reload
					toggleScreen(getAppScreens('posts'), true);
					location.reload();
				}
			}
		}
		else
		{
			console.log('Yep, this input has validation :p');
		}
	});
}

