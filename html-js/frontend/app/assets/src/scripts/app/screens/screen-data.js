import { logLocal } from '../storage/localStorage.js';
import { $app, getAppData, getAppFetched, getAppScreens } from "../data/data-actions.js";
import { template } from '../render/template.js';
import { enlightenerInit } from '../libs/enlightener.js';
import { urlToObject, getUrlRestPath } from "../utils/url-string.js";
import { clearRestCacheItem } from '../data/data-cache.js';
import { dateToYMD, dateToTime } from "../utils/string.js";

const jqEls = {
	'cacheTable' : $('.localcache-items-table'),
	'cacheItems' : $('.localcache-items-list'),
	'cacheNotice': $('.localcache-items-notice'),
};

/**
 * Initial setup for the Data screen, which displays app configuration data,
 * and lists the locally cached REST data.
 *
 * Runs once, on app init
 */
export function setupDataScreen()
{
	// Hide notice & table elements
	jqEls.cacheNotice.hide();
	jqEls.cacheTable.hide();

	// Click events
	$app.on('click', '.js-btn-displaylocalCacheData', e =>
	{
		e.preventDefault();
		displayRestCache();
	});

	// #deprecated #unused
	// $app.on('click', '.js-btn-clearlocalCacheData',   () => clearRestCache());
	// $app.on('click', '.js-btn-displayConfigData',     () => displayConfigData());

	$app.on('click', '.js-local-cache-url', e =>
	{
		e.preventDefault();
		const itemUrl = $(e.currentTarget).attr('data-local-cache-url');
		logLocal(itemUrl);
	});

	$app.on('click', '.js-clear-local-cache-item', e =>
	{
		e.preventDefault();
		const itemUrl  = $(e.currentTarget).attr('data-local-cache-url');
		const itemData = $(e.currentTarget).closest('[data-local-cache-row-data]').attr('data-local-cache-row-data');
		clearRestCacheItem(itemUrl, JSON.parse(itemData).requestType, JSON.parse(itemData).restPath);
	});
}

export function displayRestCache()
{
	const restCacheData = getAppFetched('restCacheData');

	if (!restCacheData || Object.keys(restCacheData).length === 0)
	{
		jqEls.cacheTable.hide();

		jqEls.cacheNotice.show();
		jqEls.cacheNotice.text('No local cache data available to display');

		return;
	}

	jqEls.cacheNotice.hide();
	jqEls.cacheTable.show();

	let localItemsHtmlArr = [];

	for (const url in restCacheData)
	{
		const origin      = urlToObject(url).origin;
		const queryString = urlToObject(url).search.substring(1);
		const restPath    = getUrlRestPath(url);

		const { count, date, screenId, requestType } = restCacheData[url];

		// Date & Time
		const shortDate = dateToYMD(date);
		const time = dateToTime(date);

		// Screen
		const screens = getAppScreens();
		const screenValues = Object.values(screens);
		const screen = screenValues.filter(s => s.screenId === screenId)[0];

		let screenLink;

		if (screen)
		{
			screenLink = `<a data-app-screen-link href="${screen.url}">${screen.name}</a>`;
		}
		else
		{
			screenLink = '<span class="preload-text-waiting">?</span>'; //@TODO: Use a better class name
		}

		const templateData = { url, origin, restPath, queryString, count, shortDate, time, screenLink, requestType };
		const localItemHtml = template('localcache-item', templateData);

		localItemsHtmlArr.push(localItemHtml);
	}

	jqEls.cacheItems.html(localItemsHtmlArr.join(''));
}


/**
 * Updates the data screen panels
 *
 * #unused #deprecated
 */
export function displayConfigData()
{
	if ($('.data-panel').length)
	{
		$('.data-panel').each((i, element) =>
		{
			const dataKey = $(element).attr('data-data-panel-key');
			const dataName = $(element).attr('data-data-panel-name');

			let optionObject = getAppData(dataKey, dataName);

			if (!optionObject)
			{
				console.warn('[error_displayConfigData] No key/name present in appData', { dataKey, dataName });
			}

			let objectData = '';

			if (optionObject)
			{
				objectData = JSON.stringify(optionObject, null, 2);
				$(element).text(objectData);
			}
		});

		// enlightenerInit();
	}
	else
	{
		console.warn('[error_updateDataPanels__notfound] No data panels found (\'.data-panel\')');
	}
}