import { jwtAuthenticate, jwtGetPost, jwtPostUpdate, jwtValidate, tokenCookieName } from "../authentication/jwt.js";
import { clearInputValue } from '../utils/elements.js';
import { $app } from "../data/data-actions.js";
import { getCookie } from '../utils/cookies.js';

const postData = { title: 'REST Demo: Editable Title - CHANGED' };

// const host = hostOptions.self;

const els = {
	'buttons': {
		'authenticate': 	'.js-jwt-authenticate',
		'get': 				'.js-jwt-rest-get',
		'post': 			'.js-jwt-rest-post',
		'validate': 		'.js-jwt-validate',
	},
	'input': {
		'authenticate': 	'.js-jwt-authenticate-input',
		'endpointGet': 		'.jwt-endpoint-get',
		'endpointPost': 	'.jwt-endpoint-post',
		'host': 			'.jwt-host',
		'loginsPassword': 	'.jwt-logins-password',
		'loginsUsername': 	'.jwt-logins-username',
		'validate': 		'.js-jwt-validate-input',
	},
	'output': {
		'authenticate': 	'.js-jwt-authenticate-output',
		'currentToken':		'.jwt-current-token',
		'validate': 		'.js-jwt-validate-output',
	},
};

export function jwtEvents()
{
	jwtOnLoadEvents();
	jwtClickEvents();
}

function jwtOnLoadEvents()
{
	const cookieToken = getCookie(tokenCookieName);

	$(els.output.currentToken).val(cookieToken);
}

function jwtClickEvents()
{
	$app.on('click', els.buttons.authenticate, e =>
	{
		e.preventDefault();

		clearInputValue(els.output.authenticate);

		const host = jwtGetHost();
		const user = $(els.input.loginsUsername).val();
		const pass = $(els.input.loginsPassword).val();

		jwtAuthenticate(host, user, pass, updateJwtAuthTokenInput);
	});

	$app.on('click', els.buttons.validate, e =>
	{
		e.preventDefault();

		clearInputValue(els.output.validate);

		const host = jwtGetHost();
		const token = $(els.input.validate).val();

		jwtValidate(host, token, updateJwtValidateInput);
	});

	$app.on('click', els.buttons.get, e =>
	{
		e.preventDefault();

		const host = jwtGetHost();
		const endpointGet = $(els.input.endpointGet).val();

		jwtGetPost(host, endpointGet);
	});

	$app.on('click', els.buttons.post, e =>
	{
		e.preventDefault();

		const host = jwtGetHost();
		const endpointPost = $(els.input.endpointPost).val();
		jwtPostUpdate(host, endpointPost, postData);
	});
}

// Get value

function jwtGetHost()
{
	const host = $(els.input.host).val();

	return (host) ? host : false;
}

// Callbacks

function updateJwtAuthTokenInput(response)
{
	const token = response.token;

	$(els.output.authenticate).val(token);

	if(response.token)
	{
		$(els.output.currentToken).val(token);
	}
}

function updateJwtValidateInput(response)
{
	let responseStatus = '';

	if (response.status === 200)
	{
		responseStatus = `${response.status}: VALID`;
	}
	else
	{
		responseStatus = `${response.status}: ${response.responseJSON.code} - ${response.responseJSON.message}`;
	}

	$(els.output.validate).val(responseStatus);
}
