import { generateNav } from './../render/generate.js';
import { mergeLocalConfig } from './../data/data-actions.js';
import { setupOptions } from './../ui/options.js';
import { setupPreloader } from '../screens/screen-preloader.js';
import { setupScreens } from './../ui/screens.js';
import { setupSearchPage } from './../screens/screen-search.js';
import { jwtEvents } from './../screens/screen-jwt.js';
import { onKeyPress } from '../events/keyboard-events.js';
import { postInteractions } from './../ui/posts.js';
import { setupRestData } from '../data/data-rest.js';
import { setupHistory } from '../routing/routing-history.js';
import { toggler } from '../events/toggle.js';
import { setupLinkRoutingEvents } from '../routing/routing-links.js';

/**
 * Init
 */
export function init()
{
	// These run only once, on init
	loadStaticModules();
}

/**
 * Load static modules. Called from init
 *
 * Note: setupScreens also sets up posts and terms (archive screen)
 */
function loadStaticModules()
{
	mergeLocalConfig();	// Merge any previously-saved user options (stored in localStorage) into the current app config
	setupRestData();	// Set up REST data objects
	setupPreloader();	// Preload posts
	generateNav();		// Generate the nav, which contains screens and pages listed in the app config
	setupEvents();		// Events: Attaches and listens to custom/jQuery events
	setupOptions();		// Sidebar options: Updates the UI according to any options that have been set
	setupScreens();		// Screens: The main UI code. Handles routing, and setting up the archive screens
	setupHistory();		// Tracks user history, to ensure a smooth UI when changing the pop state (forward/back buttons) and create minimal loads
	setupSearchPage();	// Search page (requires preloaded posts)
}

/**
 * Set up events. Called from loadStaticModules
 *
 * Used to run actions. Actions are separated, so you can run them
 * without having to trigger an event.
 */
function setupEvents()
{
	setupLinkRoutingEvents(); // Routing for links (including navigation items)
	onKeyPress(); 		// Keyboard shortcuts
	postInteractions(); // When posts are opened, or filtered by their terms
	jwtEvents();		// Authentication dev panel
	toggler();			// Simple toggle
}
