import { getJQEl } from './../data/data-actions.js';
import { setElementBackground } from '../utils/elements.js';

export function setHeaderScreenName(title, screenUrl)
{
	if (title != null)
	{
		const $screenName = getJQEl('headerScreenName');

		if (screenUrl)
		{
			$screenName.html(`<a href="/${screenUrl}" data-app-screen-link>${title}</a>`);
		}
		else
		{
			$screenName.html(title);
		}
	}
}

/**
 * Removes the name of a post from the header
 */
export function removePostFromHeader()
{
	getJQEl('headerPostTitle').addClass('uk-hidden');
	getJQEl('headerPostTitleSep').addClass('uk-hidden');

	$( '.header-title-link' ).removeClass( 'header-title-has-child' );
}

export function setHeaderPostTitle(postTitle)
{
	if (postTitle != null)
	{
		getJQEl('headerPostTitle').html(postTitle).removeClass('uk-hidden').addClass('uk-active'); //@TODO: This feels ugly
		getJQEl('headerPostTitleSep').removeClass('uk-hidden');
	}
	else
	{
		removePostFromHeader();
	}
}

function setHeaderColor(color)
{
	const $el = getJQEl('header');
	setElementBackground(color, $el);
}

function setHeaderTypeClass(screenType)
{
	const $headerEl = getJQEl('header');
	const screenTypesArr = [ 'archive', 'page', 'search' ];
	const classPrefix = 'header-type-';

	screenTypesArr.forEach(st => $headerEl.removeClass( classPrefix + st ) );

	$headerEl.addClass( classPrefix + screenType );
}

/**
 * Set the view's header
 *
 * @param   {string}  title       Header title
 * @param   {string}  color       Header colour
 * @param   {string}  screenUrl   Valid screen URL
 * @param   {string}  screenType  Screen type ('archive'|'page')
 *
 * @return  {void}
 */
export function setHeader(title, color, screenUrl, screenType)
{
	setHeaderScreenName(title, screenUrl);
	setHeaderColor(color);
	setHeaderTypeClass(screenType);
}

/**
 * Set the header state class. This controls the header styles
 * (eg, if archive post list screen is active, archive name is
 * highlighted)
 *
 * @TODO: Rename?
 *
 * @param   {[type]}  hasChild  [hasChild description]
 *
 * @return  {[type]}            [return description]
 */
export function setHeaderScreenNameState(hasChild)
{
	const $screenName = getJQEl('headerScreenName');

	if (hasChild)
	{
		$screenName.addClass('header-title-has-child');
	}
	else
	{
		$screenName.removeClass('header-title-has-child');
	}
}