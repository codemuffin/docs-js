import { $app, getAppOptions, getAppFetched, getAppData, getAppCurrent, getJQEl, setAppOption } from './../data/data-actions.js';
import { log } from '../utils/logger.js';
import { setDefault } from './../utils/object.js';
import { setLocal, clearLocal, logLocal, logLocalSize, removeLocal } from '../storage/localStorage.js';
import { clearRestCache, clearArchiveScreenCache } from "../data/data-cache.js";
import { preloadRetry, logPreloadData } from '../screens/screen-preloader.js';
import { displayRestCache } from '../screens/screen-data.js';
import { logCurrentPost } from './posts.js';

// Sidebar options


// Exports
// ============================================================================

export function setupOptions()
{
	optionTogglesSetup();
	appButtonsSetup();
}

export function saveUserOption(option, state)
{
	// Save to config must come first, as we merge config into localStorage options next
	saveUiOptionToConfig(option, state);
	saveUiOptionToLocal();
}

// NEW: Added for the data source changer screen
export function saveDevUserOption(optionGroup, option, state)
{
	// Save to config must come first, as we merge config into localStorage options next
	setAppOption(optionGroup, option, state);

	// We have to add the whole options object from appOptions :O
	setLocal('devOptions', getAppOptions());
}


// Internal Functions
// ============================================================================

/**
 * Toggles
 *
 * Set up click events and initial settings
 */
function optionTogglesSetup()
{
	// Loop through each option button. Auto-apply its functions,
	// and attach a click handler to it
	let $optionToggles = $app.find('[data-app-option]');

	$optionToggles.each((i, button) =>
	{
		let $button = $(button);
		let autoApply = $button.data('app-option-onload');

		// Auto-apply buttons emulate user clicks to apply the
		// default settings from the markup
		if (autoApply)
		{
			optionToggleAutoApply($button);
		}

		// Button click event handler
		$button.on('click', () =>
		{
			onOptionToggleClick($button);
		});
	});
}

/**
 * Buttons
 *
 * Set up click events for developer buttons
 */
function appButtonsSetup()
{
	$app.on('click', '[data-app-button]', e =>
	{
		appButtonClickHandler(e.currentTarget);
	});

	/*
	let $optionButtons = $app.find('[data-app-button]');
	$optionButtons.each((i, button) =>
	{
		let $button = $(button);
	});
	*/
}

/**
 * Automatically run a button's 'on click' function, and disable
 * it from auto-running again.
 *
 * Used on page load.
 */
function optionToggleAutoApply($button)
{
	// Automatically run this button's 'on click' function
	onOptionToggleClick($button, true);

	// Disable further autoapplying
	$button.data('app-option-onload', false);
}

/**
 * Retrieve an option from a checkbox button, then toggle that option based on the button's state (active/inactive = true/false)
 *
 * @param {jQuery}	$button 	jQuery object, as an option button. The element can be of any type, but must contain the
 *                          	attribute 'data-app-option', with a unique hyphenated option name as the value
 * @param {boolean}	setupMode	Sets the option to run in setup mode, which overwrites its current option state with settings
 * 								from the config. See toggleOption for more details.
 */
function onOptionToggleClick($button, setupMode)
{
	// Defaults
	setupMode = setDefault(setupMode, false);

	const option = $button.data( 'app-option' );
	const state = $button.prop( 'checked' );

	if (!setupMode)
	{
		// Save the option to localStorage (to retain it on reload)
		saveUserOption( option, state );
	}

	// Apply the option
	toggleOption( option, state, setupMode );
}

/**
 * Apply a button state based on option (eg, if data says button=active, then make that button active)
 *
 * @param {string} option The option identifier
 * @param {boolean} state The active state (true = active)
 */
function applyButtonFromOption(option, state)
{
	const $button = $( '[data-app-option="' + option + '"]' );

	if( $button.length )
	{
		$button.prop( 'checked', state );
	}
}

/**
 * Buttons
 *
 * Used across, with the attribute data-app-button="{buttonId}"
 */
function appButtonClickHandler(button, writeLog)
{
	// Defaults
	writeLog = setDefault(writeLog, true);

	// Button option data
	let buttonId = $(button).data('app-button');

	// Logger
	let logMessage = null; // message string

	// used by: 'log-settings'
	let oData = null;
	const logOptionsAsTable = false; //@TODO: Make this option available in the config

	// Buttons switch
	switch( buttonId )
	{
		case 'log-app':

			console.log( 'DEPRECATED' );
			break;

		case 'log-all-data' :

			console.log( getAppData() );
			break;

		case 'log-settings':

			oData = getAppData('options');

			console.log( oData );

			if (logOptionsAsTable)
			{
				// Log in table format!
				const oKeys = Object.keys(oData);
				const oValues = Object.values(oData);

				for (const o in oKeys)
				{
					const optionKey = oKeys[o];
					const optionValue = oValues[o];

					console.log('%c' + optionKey, 'color:limegreen');
					console.table(optionValue);
				}
			}
			break;

		case 'log-fetched':
			console.log(getAppFetched());
			break;

		case 'log-current':
			console.log(getAppCurrent());
			break;

		/*
		case 'log-screens':
			console.table(getAppScreens());
			break;
		*/

		case 'log-localstorage-size':
			logLocalSize();
			logMessage = 'Logged localStorage size data to the console';
			break;

		case 'localStorageLog':
			logLocal();
			logMessage = 'Logged localStorage data to the console';
			break;

		case 'localStorageLogUserOptions':
			logLocal('userOptions');
			logMessage = 'Logged localStorage data "userOptions" to the console';
			break;

		case 'localStorageClear':
			clearLocal();
			logMessage = 'Cleared all localStorage data: Your local cache of post data has been reset, and posts will be retrived via AJAX. After retreival, the new post data will be locally saved again';
			break;

		case 'localUserClear':
			removeLocal('userOptions');
			logMessage = 'Cleared user options';
			break;

		case 'restCacheClear':
			clearRestCache();
			logMessage = 'Cleared REST cache';
			break;

		case 'restFetch':
			preloadRetry();
			logMessage = 'Performing requests for any uncached posts/terms...';
			break;

		case 'logPreloadData':
			logPreloadData();
			break;

		//#unused #deprecated
		// case 'displayConfigData':
		// 	displayConfigData();
		// 	break;

		case 'displayRestCache':
			displayRestCache();
			break;

		case 'logCurrentPost':
			logCurrentPost();
			break;

		case 'clearArchiveScreenCache':
			clearArchiveScreenCache();
			break;

	}

	// Log the buttonId change
	if( logMessage !== null && writeLog )
	{
		log(logMessage, 'DEV > ' + buttonId, 'success');
	}
}

/**
 * Toggle a single option
 *
 * @param {string} option Name of the option, set in [data-app-option]
 * @param {boolean} state Active state (true = active)
 * @param {boolean} setupMode Overrule a button's option state, using the user-defined config. This should only be done once, at the setup stage
 */
function toggleOption(option, state, setupMode)
{
	// Defaults
	setupMode = setDefault(setupMode, false);

	// If in setup mode, use config settings, unless we can use saved user options
	if (setupMode)
	{
		const optionValue = getAppOptions('ui', option);

		if (optionValue !== undefined)
		{
			// Set the option state, using the default|config|local state
			state = optionValue;

			// Set the button state (otherwise we can't tell which options are active!)
			applyButtonFromOption(option, optionValue);
		}
	}

	// Classes

	const optionClsBase = 'option-' + option.toLowerCase();
	const optionClsTrue = optionClsBase + '-true';
	const optionClsFalse = optionClsBase + '-false';

	const removeClass = (state) ? optionClsFalse : optionClsTrue;
	const addClass = (state) ? optionClsTrue : optionClsFalse;

	// Option toggle (options are handled with CSS)
	getJQEl('appOptions').removeClass(removeClass).addClass(addClass);
}



/**
 * Save a UI option to config
 *
 * @param {string} option The ID of a user option
 * @param {boolean} state The option state. True = active
 */
function saveUiOptionToConfig(option, state)
{
	setAppOption('ui', option, state);
}

/**
 * Save a user option to localStorage
 */
function saveUiOptionToLocal()
{
	// localStorage stores strings, not objects, so we have to add the whole ui object from appOptions
	setLocal('userOptions', getAppOptions('ui'));
}

