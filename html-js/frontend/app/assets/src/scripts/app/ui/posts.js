import { getScreenJQElById, setScreenByCurrentUrl, screenUpdateBrowser, getScreenNodeByID } from './screens.js';
import { setHeaderPostTitle, setHeaderScreenNameState, removePostFromHeader } from './header.js';
import { $app, getAppCurrent, setAppCurrent } from './../data/data-actions.js';
import { getSinglePost } from './../data/data-rest.js';
import { enlightenerInit } from './../libs/enlightener.js';
import { generatePostViewHtml } from '../render/generate.js';
import { setUrl, setDocumentTitle, setDocumentMetaDescription } from '../routing/routing-browser.js';
import { setDefault } from '../utils/object.js';
import { getNativeRestBaseFromPostType } from '../utils/wp-posts.js';
import { getScreenByPostType } from '../data/data-screens.js';
import { extractExcerpt } from '../rest/rest-utilities.js';

/**
 * User Interaction Events
 */
export function postInteractions()
{
	postClick();
	postCloseClick();
}

/**
 * User clicks a list post
 */
export function postClick()
{
	$app.on('click', '.post-item', e =>
	{
		e.preventDefault();

		const $post = $(e.currentTarget);
		openPost($post, false, true);

		// IF we're on the search page, open the click post's corresponding archive
		if (getAppCurrent('screen', 'screenId') === 'search')
		{
			setScreenByCurrentUrl();
		}
	});
}

/**
 * User clicks an open posts view's close button
 */
export function postCloseClick()
{
	$app.on('click', '.post-view-close', e =>
	{
		e.preventDefault();

		const $container = $(e.currentTarget).closest('.post-view-item'); // Closed view post
		const postId = $container.attr('data-post-view-id');
		const $post = $(`.post-item[data-post-id="${postId}"]`); // Corresponding list post

		closePost($post, true);
	});
}

/**
 * User clicks a term button
 */
export function termClick(screenId)
{
	const termEls = getScreenNodeByID(screenId).getElementsByClassName('term-filter');

	// Arrow functions FTW!!
	Array.from(termEls).forEach(t => t.addEventListener('click', e =>
	{
		e.preventDefault();
		toggleTerm(e.currentTarget, screenId)
	}));
}


// User Interaction Actions

/**
 * Open a post
 *
 * @param   {object}  $el        jQuery post element object
 * @param   {object=} postObj    Post data object
 * @param   {boolean} updateUrl  If true, the URL will be updated when the post is opened. Default: true
 */
export function openPost($el, postObj, updateUrl)
{
	updateUrl = setDefault(updateUrl, true);

	// Remove all previous post item active classes
	removeActivePostClasses($el);
	// closePost(null, false);

	// Get the post data, if it wasn't originally passed
	if (!postObj)
	{
		const postType = $el.attr('data-post-type');
		const postId   = $el.attr('data-post-id');
		postObj  = getSinglePost(postType, postId);
	}

	if (postObj)
	{
		// Set the post in the current data object
		setAppCurrent('post', postObj);

		// Handle WP's native post types, which have a REST path different to their post type name
		const postType = getNativeRestBaseFromPostType(postObj.type);
		const postTitle = postObj.title.rendered;
		const postExcerpt = extractExcerpt( postObj );

		// Find the correct screen
		const screenData = getScreenByPostType(postType);
		const $screen = getScreenJQElById(screenData.screenId);
		const $post = $screen.find('.post-view-item');

		// Set post view HTML
		$post.empty().html(generatePostViewHtml(postObj, postObj.id));

		//#UNUSED: Set post view data attrs
		// $screen.find('.post-view-item').attr('data-post-view-postID', postObj.id);

		// Set columns active class
		//@TODO: Currently there is one .post-columns el, but each archive screen should have their own .post-columns element
		$('.post-columns').addClass('uk-active');

		// Set the last clicked post item to active
		$el.addClass('uk-active');

		// Update the header title, appending it with the post name
		setHeaderPostTitle(postTitle);
		setHeaderScreenNameState(true);

		// Ensure the open post scroll is set to the top
		// Without this, posts would open at the same scroll position as the last open post
		$screen.find('.post-view-item')[0].scrollTop = 0;

		// Update the browser (URL)
		if (updateUrl)
		{
			const postUrl = postType + '/' + postObj.slug;
			setUrl(postUrl, postTitle);
		}

		// Update the document title (changes the tab title, and the page name in the browser history)
		setDocumentTitle(screenData.name, postTitle);

		setDocumentMetaDescription( postExcerpt );

		// Init Enlightener, which adds syntax highlighting to any code blocks in the post content
		// enlightenerInit();
	}
	else
	{
		console.warn('[error_openPost] Tried to open post, but no post data was available');
	}

}

/**
 * Removes active classes from posts.
 *
 * Used when highlighting the last active post is no longer needed,
 * e.g., when the user selects a new post or screen
 *
 * @param {object} $currentPostEl Current post jQuery object - this item will keeps its active class
 */
export function removeActivePostClasses($currentPostEl)
{
	if ($currentPostEl)
	{
		// Post el supplied, so don't close the post view, just remove the other active post highlight class
		$('.post-item').not($currentPostEl).removeClass('uk-active');
	}
	else
	{
		// If no post el was supplied, we remove all active post classes
		$('.post-item').removeClass('uk-active');
		$('.post-columns').removeClass('uk-active');
	}
}

/**
 * Close a post
 *
 * @param {object}  $post  jQuery post object
 */
export function closePost($post, updateUrl)
{
	updateUrl = setDefault(updateUrl, false);

	if( $post && $post.length )
	{
		// Remove the corresponding post's active class (in hindsight -- we could have just removed all post list item active classes? :O)
		//@TODO| Maybe don't remove the active class? It's useful to see which post item was just open. But definitely update the styles
		$post.removeClass( 'uk-active' );
	}

	// Remove the HTML from the quick view
	$('.post-view-item').html('');

	// Unset columns active class
	$('.post-columns').removeClass('uk-active');

	// Remove anything related to the post title
	removePostFromHeader();
	setHeaderScreenNameState(false);

	// Unset the current post data object
	setAppCurrent('post', false);

	// Update the URL
	// This is't the default behaviour, because it should only happen when a
	// post is manually closed, eg by pressing Esc or clicking the close button
	if (updateUrl)
	{
		const currentScreen = getAppCurrent('screen');
		screenUpdateBrowser(currentScreen, true);
	}
}


/**
 * Shows/hides posts, filtered by a term, and sets the term button active/inactive state
 *
 * @param   {object}  termBtn  Term button node
 */
export function toggleTerm(termBtn, screenId)
{
	const termTarget = termBtn.getAttribute('data-term-target');

	if (!screenId || !termTarget)
	{
		console.warn('[error_toggleTerm] Something went wrong screenId or termTarget (or both) have not been set.', {screenId, termTarget});
	}

	const screenNode = getScreenNodeByID(screenId);
	const showNodes  = screenNode.querySelectorAll(`.post-item.${termTarget}`);
	const hideNodes  = screenNode.querySelectorAll(`.post-item:not(.${termTarget})`);

	// Ensure the clicked button wasn't already active
	if (!termBtn.classList.contains('uk-active'))
	{
		// Deactivate all other buttons
		screenNode.querySelectorAll('.term-filter').forEach(t => t.classList.remove('uk-active'));

		// Activate this button
		termBtn.classList.add('uk-active');

		// Special condition for 'ALL' filter
		if (termTarget === 'ALL')
		{
			clearAllFilters();
		}
		else
		{
			showNodes.forEach(node => node.closest('.post-item-container').classList.remove('uk-hidden'));
			hideNodes.forEach(node => node.closest('.post-item-container').classList.add('uk-hidden'));

			// $show.closest('.post-item-container').removeClass('uk-hidden');
			// $hide.closest('.post-item-container').addClass('uk-hidden');
		}
	}
}

/**
 * Clear all active filters. Runs when the 'ALL' filter is clicked, or when the user changes screens
 *
 * @param  {string}  screenId  Target screen ID. If not specified, filters on every screen will be cleared
 */
export function clearAllFilters(screenId)
{
	let $base = $app;

	if (screenId)
	{
		$base = getScreenJQElById(screenId);
	}

	$base.find('.post-item-container').removeClass('uk-hidden');
	$base.find('.post-item').removeClass('uk-hidden');
}

export function logCurrentPost()
{
	const currentPost = getAppCurrent('post');

	if (currentPost)
	{
		console.log(currentPost);
	}
	else
	{
		console.log('No current post object unavailable');
	}
}

/*
// Removes all term buttons (#unused)
function removeTerms(screenId)
{
	getScreenJQElById(screenId).find('.term-filters-items .term-filter, .loader--terms').remove();
}

// Removes all listed posts (#unused)
function removePosts(screenId)
{
	getScreenJQElById(screenId).find('.post-list .post-item-container').remove();
}

// Hides every post (#unused)
function hideAllPosts(screenId)
{
	getScreenJQElById(screenId).find('.post-item-container').addClass('uk-hidden');
}
*/