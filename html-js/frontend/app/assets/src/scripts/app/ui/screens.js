import { $app, getAppScreens, getAppFetched, getAppOptions } from '../data/data-actions.js';
import { setUrl, setDocumentTitle, setDocumentMetaDescription } from '../routing/routing-browser.js';
import { changedScreenToArchive } from './../screens/screen-archive.js';
import { generateArchiveScreensHtml } from './../render/generate.js';
import { hideAllElementsExcept } from '../utils/elements.js';
import { removeActivePostClasses } from './posts.js';
import { setActiveMenuItem } from '../ui/navigation.js';
import { setAppCurrent } from './../data/data-actions.js';
import { setDefault, objectHasProp } from './../utils/object.js';
import { setHeader, removePostFromHeader, } from '../ui/header.js';
import { setupDataScreen, displayRestCache } from '../screens/screen-data.js';
import { prepareUrlPath, isUrlRelative, urlToRelative } from './../utils/url-string.js';
import { setupPreloadHtml } from '../screens/screen-preloader.js';
import { setupSourceScreen } from '../screens/screen-host.js';
import { getScreenByUrlProperty, getScreenByUrlPath } from '../data/data-screens.js';

// Update screens when the user clicks a different page, or when the site is initially loaded

//@TODO: A lot of this is very confusing, please refactor :)

/**
 * Initial setup for Screens
 */
export function setupScreens()
{
	// Set up the Archive screens
	// These are dynamically created from the screens data in config-screens
	// Note: This doesn't load posts data, it just generates the basic HTML for each archive
	generateArchiveScreensHtml();

	// Set up the Data screen (hidden unless Developer Mode is enabled)
	setupDataScreen();

	// Find and display the relevant screen, based on the current URL
	setScreenByCurrentUrl();

	// Preload archive screens
	// preloadArchives();

	// Dev: Source screen. This lets you change the REST data source
	setupSourceScreen();
}

/**
 * Set the screen based on the current URL
 *
 * Runs when the user initially loads a page, and when they navigate forward or
 * back in their browser
 */
export function setScreenByCurrentUrl()
{
	let screenData = getScreenByUrlPath(window.location.href, true);
	changeScreen(screenData, false);
}

/**
 * Set the screen, based on a supplied URL
 *
 * Used when the user clicks a navigation item (see routing-links.js)
 *
 * @param   {string}  url  The URL of the requested screen
 */
export function setScreenByUrlProperty(url, updateUrl)
{
	// Defaults
	updateUrl = setDefault(updateUrl, true);

	// Ensure relative URL & remove slashes
	url = prepareUrlPath((!isUrlRelative(url) ? urlToRelative(url) : url));

	let screenData = getScreenByUrlProperty(url, true);

	changeScreen(screenData, updateUrl);
}

/**
 * Change the active screen.
 *
 * Contains the main logic for displaying different sdcreen types
 *
 * @param   {object}   screenData  Screen data object
 * @param   {boolean}  updateUrl   Update the URL when the screen changes?? (default: false)
 * @param   {string}   path        Child path, used by screenUpdateBrowser and setHeader. Only needed if updateUrl is true
 */
export function changeScreen(screenData, updateUrl, path)
{
	// Changed screen to any
	toggleScreen(screenData, updateUrl, path); // Show this screen, hide all others

	// Screen-specific actions
	switch (screenData.screenType)
	{
		case 'page':
			changedScreenToPage(screenData);
			break;

		case 'archive':
			changedScreenToArchive(screenData);
			break;

		case 'search':
			changedScreenToSearch();
			break;
	}
}

/**
 * Actions for displaying a basic page screen
 */
function changedScreenToPage(screenData)
{
	// 'Page' screen type setup functions can go here!

	switch (screenData.screenId)
	{
		case 'preloader':
			setupPreloadHtml();
			break;

		case 'data':
			displayRestCache();
			break;
	}
}

function changedScreenToSearch()
{
	removeActivePostClasses();
	removePostFromHeader();
}

/**
 * Show a screen, and hide all other screens
 *
 * @param   {object}   screenData  Screen data pbject
 * @param   {boolean}  updateUrl   Update the URL after changing screens?
 * @param   {string}   path        URL path, used for updating the URL
 */
export function toggleScreen(screenData, updateUrl, path)
{
	// Close any open posts
	// removeActivePostClasses();
	// closePost(null, false);

	screenUpdateBrowser(screenData, updateUrl, path); // Update the URL etc

	// Save the currently selected page screenData to the 'screens' data object
	setAppCurrent('screen', screenData);

	// Update the currently active menu item
	setActiveMenuItem(screenData.name);

	// Show the selected screen, hide all others
	const showScreen = '[data-app-screen-id="' + screenData.screenId + '"]';
	const hideScreens = '[data-app-screen-id]';

	hideAllElementsExcept(hideScreens, showScreen);
}

export function screenUpdateBrowser(screenData, updateUrl, path)
{
	// Default: never update the URL unless specifically requested
	updateUrl = setDefault(updateUrl, false);
	path = setDefault(path, screenData.url);

	const { name, color, screenType, taxonomy } = screenData;

	// Update the browser url
	if (updateUrl)
	{
		setUrl(path, name);
	}

	setDocumentTitle(name);	// update the browser title (changes the tab title, and the page name in the browser history)
	setDocumentMetaDescription( extractScreenMeta( screenData ) );

	setHeader(name, color, path, screenType); // update the header title and colour

	setContainerTermsClass( (taxonomy) ? true : false );
}


function setContainerTermsClass( hasTerms )
{
	const $container  = $( '.post-columns' );
	const hasTermsCls = 'post-columns--has-terms';
	const noTermsCls  = 'post-columns--no-terms';

	if ( hasTerms )
	{
		$container.addClass( hasTermsCls );
		$container.removeClass( noTermsCls );
	}
	else
	{
		$container.addClass( noTermsCls );
		$container.removeClass( hasTermsCls );
	}
}

/**
 * Sets the HTML for a given target, on the specified screen
 *
 * @param   {string}  screenId  Screen ID
 * @param   {string}  target    Target (css selector)
 * @param   {string}  html      New HTML
 */
export function updateScreenHtml(screenId, target, html)
{
	getScreenJQElById(screenId).find(target).html(html);
}

export function getScreenJQElById(screenId)
{
	return $app.find(`[data-app-screen-id="${screenId}"]`);
}

export function getScreenNodeByID(screenId)
{
	return $app[0].querySelector(`[data-app-screen-id="${screenId}"]`);
}

export function setScreenTo404(search)
{
	changeScreen(getAppScreens('error404'), false);
	removePostFromHeader();

	// Search for other posts with this title

	const postTypes = getAppFetched('posts');
	const searchSafe = search.replace('-', ' ').toUpperCase();
	let matchHtmlArr = [];

	for (const postType in postTypes)
	{
		const matches = Object.values(postTypes[postType]).filter(p => p.title.rendered.toUpperCase().includes(searchSafe));

		if (matches.length)
		{
			console.log(`404 search results:`, matches);

			matches.forEach(match =>
			{
				//@TODO: Use generator with template
				matchHtmlArr.push(`<a href="${match.link}">${match.title.rendered}</a><br>`);
			});
		}
	}

	if (matchHtmlArr.length)
	{
		matchHtmlArr.unshift(`<p>A search for posts with a similar name returned ${matchHtmlArr.length} result(s):</p>`);
	}

	updateScreenHtml('error404', '.error404-suggestions', matchHtmlArr.join(''));
}


/**
 * Get the meta description for a given screen
 *
 * @param   {object}  screenData  Screen data object
 *
 * @return  {string}              Meta description
 */
export function extractScreenMeta( screenData )
{
	// let metaDesc = ( name === 'Home' ) ? getAppOptions( 'metaContent', 'defaultDescription' ) : `Archive: ${name}`;

	let metaDesc = '';

	if ( objectHasProp( screenData, 'metaDesc' ) )
	{
		metaDesc = screenData.metaDesc;
	}
	else
	{
		switch( screenData.screenType )
		{
			case 'archive':
				metaDesc = `Archive: ${screenData.name}`;
				break;

			case 'page':
				metaDesc = `Page content for ${screenData.name}`;
				break;

			case 'search':
				metaDesc = 'Search this site';
				break;

			default:
				getAppOptions( 'metaContent', 'defaultDescription' );
		}
	}

	return metaDesc;
}
