import { $app } from '../data/data-actions.js';

// UI Navigation
// For nav item routing, see routing-links.js

export function setActiveMenuItem(name)
{
	// Set the active menu class (after removing any other active meny item classes)
	$app.find( '.nav-item' ).removeClass( 'uk-active' );
	$app.find( '.nav-item:contains(' + name + ')' ).addClass( 'uk-active' );
}
