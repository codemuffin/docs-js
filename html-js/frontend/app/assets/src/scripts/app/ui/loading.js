import { setDefault } from './../utils/object.js';
import { getAppOptions } from './../data/data-actions.js';
import { getloadingText } from '../data/data-actions.js';
import { getScreenJQElById } from './screens.js';

const noticeEls = {
	'posts': '.post-list-notice',
	'terms': '.terms-list-notice',
	'search': '[data-app-screen-id="search"] .post-list-notice',
};

function validateNoticeTypes(suppliedType)
{
	const allowedTypes = ['posts', 'terms', 'search'];

	if (!allowedTypes.includes(suppliedType))
	{
		console.warn(`[validateNoticeTypes] Specified notice type "${suppliedType}" is not an accepted option`, { allowedTypes });
		return false;
	}
	else
	{
		return true;
	}
}

/**
 * Toggle the post/terms loading notice
 *
 * @param {string}  requestType Posts or terms? ('posts'/'terms')
 * @param {boolean} show        Show or hide this loading notice?
 * @param {string}  screenId    ID for the target screen
 */
export function toggleLoaderNotice(requestType, show, screenId)
{
	if (!validateNoticeTypes(requestType))
	{
		return;
	}

	// Defaults
	show = setDefault(show, true);

	const $screen = getScreenJQElById(screenId);
	let $target;

	switch (requestType)
	{
		case 'posts':
			$target = $screen.find(noticeEls.posts);
			break;

		case 'terms':
			$target = $screen.find(noticeEls.terms);
			break;

		case 'search':
			$target = $screen.find(noticeEls.search);
			break;
	}

	if (show)
	{
		$target.removeClass('uk-hidden');
	}
	else
	{
		$target.addClass('uk-hidden');
	}
}

/**
 * Simple loading messages, shown while fetching posts and terms
 *
 * @param {string}  requestType Posts or terms? ('posts'/'terms')
 * @param {string}  id          Loading mesage ID
 * @param {string}  screenId    Target screen ID
 */
export function loadingNotice(requestType, id, screenId)
{
	if (!validateNoticeTypes(requestType))
	{
		return;
	}

	if (typeof screenId !== 'string')
	{
		console.warn('[error_loadingNotice]', `Expected screenId as a string, but supplied type was: ${typeof screenId}`, {requestType, id, screenId});
	}

	const $screen = getScreenJQElById(screenId);
	const $target = $screen.find(noticeEls[requestType]);

	// Ensure the notice is visible
	toggleLoaderNotice(requestType, true, screenId);

	switch (id)
	{
		// Posts
		case 'autoGenerateArchivesDisabled': //#UNUSED
			$target.html('Archive HTML generation is currently disabled.');
			break;
		case 'autoGenerateArchivesDisabledButHasData':
			$target.html('Data has been fetched, but archive generation is disabled.');
			break;
		case 'autoFetchArchivesDisabled':
			$target.html('No local data is available, and autoFetch is currenly disabled for archives. Post data will not be loaded until being manually fetched with the Preloader.');
			break;
		case 'postListLoading':
			$target.html('Loading posts... <br><br>Check the status on the <a href="/dev-preloader">Preloader page</a>');
			break;
		case 'postListNoPostType':
			$target.html('No post type was specified.');
			break;

		// Terms
		case 'termsListLoading':
			$target.html('Loading terms...');
			break;

		// Search page
		case 'searchPageWaiting':
			$target.html('Search requires all posts to be loaded. <br><br>Check their status on the <a href="/dev-preloader">Preloader page</a>.');
			break;
		case 'searchUnavailablePreloadDisabled':
			$target.html('Search is unavailable, because preloading is disabled.');
			break;

		// Errors & warnings
		case 'warningEmptyData':
			$target.html('Warning: Response data was empty. This means that the post type/taxonomy exists, it just has no posts/terms.');
			break;
		case 'errorUnauthorised':
			$target.html('Error: You are not currently authorised to retreive this data. Please try clearing your cookies and localStorage data.');
			break;

		// General errors/warnings
		case 'ajaxWarning':
			$target.html('AJAX warning. Check the console for details.');
			break;
		case 'ajaxError':
			$target.html('AJAX error. <br><br>Check <a href="/dev-preloader">Preloader page</a> and console for details.');
			break;
	}
}

/**
 * Loading screen.
 *
 * A white screen is shown while the JS loads, and then removed by this function.
 *
 * @TODO: Timings are currently hardcoded, so replace times with AJAX progress
 */
export function uiLoader()
{
	$('.loading-screen').fadeOut(); //@TODO: These class names could be improved
}
