
export function toggler()
{
	const toggleNodes = document.querySelectorAll('[data-toggle]');

	toggleNodes.forEach(t =>
	{
		const targetStr = t.getAttribute('data-toggle');
		const targetNode = document.querySelectorAll(targetStr);

		t.addEventListener('mousedown', e =>
		{
			e.preventDefault();

			t.classList.toggle('uk-active');

			targetNode.forEach(tn =>
			{
				tn.classList.toggle('uk-active');
			});

		});

	});
}