import { closePost } from '../ui/posts.js';

/**
 * Attach handlers to keyboard shortcut events
 */
export function onKeyPress()
{
	document.addEventListener('keydown', e =>
	{
		switch (e.key)
		{
			case 'Escape':
				onPressEsc();
				break;

			case 'F9':
				onPressF9();
				break;

			case 'F10':
				onPressF10();
				break;
		}
	});
}

function onPressEsc()
{
	closePost(null, true);
}

/**
 * Primary dev hotkey. Used for testing
 */
function onPressF9()
{
	// Dev code goes here
}

/**
 * Secondary dev hotkey. Used for testing
 */
function onPressF10()
{
	// Dev code goes here
}
