import { setDefault } from './../utils/object.js';
import { log } from '../utils/logger.js';

// Local Storage Helper

/**
 * Returns parsed localStorage data, by key.
 *
 * Retreived data is parsed, after being previously stringified by the Set method.
 *
 * @param {string}  key     	Local storage key
 * @param {boolean} logThis 	Log this call?
 *
 * @returns {object|boolean}    Parsed JSON, if the requested data exists in localStorage. Returns null otherwise.
 */
export function getLocal(key, logThis)
{
	logThis = setDefault(logThis, true);

	const localData = localStorage.getItem(key);
	let output;

	if (localData)
	{
		if (logThis)
		{
			log(`Retreived: ${key}`, 'GET > LOCAL', 'warning');
		}

		output = JSON.parse(localStorage.getItem(key));
	}
	else
	{
		output = null;
	}

	return output;
}

/**
 * Save data to localStorage, via a key/value pair.
 *
 * Data is stringified before saving, allowing you to save a mixed value (string|int|object).
 *
 * @param {string} key Key for your data. Use this to access the data in the future
 * @param {*} value Value of the data. This can be a string, object, integer etc, as your data will be stringified.
 */
export function setLocal(key, value)
{
	log(`Set value in ${key}`, 'setLocal', 'success', 'warning' );

	localStorage.setItem( key, JSON.stringify( value ) );
}

/**
 * Clears all data saved to localStorage
 *
 * This includes locally cached AJAX requests, making this useful for clearing the local posts cache
 */
export function clearLocal()
{
	localStorage.clear();
}

export function removeLocal(key)
{
	localStorage.removeItem(key);
}

/**
 * Logs all localStorage data to the console, as a single object.
 *
 * This shows all of the currently saved key/value pairs.
 */
export function logLocal(key)
{
	if (key)
	{
		console.log(getLocal(key));
	}
	else
	{
		console.log(localStorage);
	}
}

/**
 * Log the size of the localStorage data to the console.
 * Code adapted from tennisgent on StackOverflow.
 *
 * @see https://stackoverflow.com/questions/4391575/how-to-find-the-size-of-localstorage
 */
export function logLocalSize()
{
	let allStrings = '';

	for ( let key in window.localStorage )
	{
		if ( window.localStorage.hasOwnProperty( key ) )
		{
			allStrings += window.localStorage[key];
		}
	}

	// Internal browser limit
	const storageLimit = '~5MB';

	// Actual usage
	const size = allStrings ? 3 + ( ( allStrings.length*16 ) / ( 8*1024 ) ) : false;
	const friendlySize = (size) ? Math.floor( size ) + ' kb' : 'EMPTY';

	const msg = `Used: ${friendlySize} - Available: ${storageLimit}`;

	console.log( msg );
}
