/**
 * IndexedDB API (#unused)
 *
 * Uses promises to handle indexedDB data. The first section is an API,
 * the second section uses promises with that API.
 *
 * IndexedDb is interesting but would add a lot of complexity to
 * this app, reducing its maintainability.
 *
 * This is left here as a working example with full context
 */

/* eslint-disable no-unused-vars */

/*
const dbName = "Docs Posts";
const dbStoreName = 'posts';
const dbKeyPath = 'id';
const dbData = [
	{ id: 10, title: "First post", author: "Biff" },
	{ id: 25, title: "Second post", author: "Chip" }
];

// Used for testing
const dbKey = 66;
const postId = 25;
const postTitle = 'Second post';
const postData = { id: 66, title: "Third post", author: "Kipper" };

// Methods: Create
idb_createDatabase(dbName, dbStoreName, dbKeyPath, dbData);
idb_addData(dbName, dbStoreName, postData);

// Methods: Access
idb_getPostById(dbName, dbStoreName, postId);
idb_getPostByTitle(dbName, dbStoreName, postTitle);
idb_getAllPosts(dbName, dbStoreName);
idb_getAllPostIds(dbName, dbStoreName);
*/

function idb_createDatabase(dbName, dbStoreName, dbKeyPath, dbData)
{
	const idb = window.indexedDB;

	let request = idb.open(dbName, 2);

	request.onerror = (e) =>
	{
		console.warn('Error creating IDB database:', e.target.error);
	};

	request.onupgradeneeded = function (e)
	{
		const db = e.target.result;

		// Create an objectStore to store our data
		// Note: dbKeyPath should always be unique (eg, post ID). If it's not, this code will need updating to include an auto-incrementer
		const objectStore = db.createObjectStore(dbStoreName, { keyPath: dbKeyPath });

		// Create index: Title
		objectStore.createIndex('title', 'title', { unique: false });

		// Create Index: Author
		objectStore.createIndex('author', 'author', { unique: false });

		// Ensure objectStore creation is finished before adding data
		objectStore.transaction.oncomplete = () => // (e)
		{
			// Store values in the newly created objectStore.
			let readWriteObjectStore = db.transaction(dbStoreName, 'readwrite').objectStore(dbStoreName);

			dbData.forEach(post => readWriteObjectStore.add(post) );
		};
	};
}

function idb_addData(dbName, dbStoreName, postData)
{
	const idb = window.indexedDB;
	const request = idb.open(dbName);

	request.onsuccess = (e) =>
	{
		const db = e.target.result;

		const transaction = db.transaction(dbStoreName, 'readwrite');
		const objectStore = transaction.objectStore(dbStoreName);
		const request = objectStore.add(postData);

		request.onsuccess = () => // (e)
		{
			console.log('DATA ADDED!');
		};

		request.onerror = (e) =>
		{
			console.warn(`Couldn't add new data: ${e.target.error.message} (${e.target.error.name})`);
		};
	};
}

function idb_getPostById(dbName, dbStoreName, postId)
{
	const idb = window.indexedDB;
	const request = idb.open(dbName);

	request.onerror = function(e)
	{
		console.log('error', e.target.error);
	};

	request.onsuccess = function(e)
	{
		const db = e.target.result;

		const transaction = db.transaction(dbStoreName, 'readonly');
		const objectStore = transaction.objectStore(dbStoreName);
		const getPostById_Request = objectStore.get(postId);

		getPostById_Request.onerror = (e) =>
		{
			console.warn('getPostById_Request_error', e.target.error);
			return e.target.error;
		};

		getPostById_Request.onsuccess = (e) =>
		{
			return e.target.result;
		};

	};
}

function idb_getAllPosts(dbName, dbStoreName)
{
	const idb = window.indexedDB;
	const request = idb.open(dbName);

	request.onerror = function (e)
	{
		console.log('error', e.target.error);
	};

	request.onsuccess = function (e)
	{
		const db = e.target.result;

		const transaction = db.transaction(dbStoreName, 'readonly');
		const objectStore = transaction.objectStore(dbStoreName);

		if ('getAll' in objectStore)
		{
			objectStore.getAll().onerror = (e) =>
			{
				return e.target.error;
			};

			objectStore.getAll().onsuccess = (e) =>
			{
				return e.target.result;
			};
		}

	};
}

function idb_getAllPostIds(dbName, dbStoreName)
{
	const idb = window.indexedDB;
	const request = idb.open(dbName);

	request.onerror = function (e)
	{
		console.log('error', e.target.error);
	};

	request.onsuccess = function (e)
	{
		const db = e.target.result;
		const transaction = db.transaction(dbStoreName, 'readonly');
		const objectStore = transaction.objectStore(dbStoreName);

		if ('getAllKeys' in objectStore)
		{
			objectStore.getAllKeys().onerror = (e) =>
			{
				return e.target.error;
			};

			objectStore.getAllKeys().onsuccess = (e) =>
			{
				return e.target.result;
			};
		}

	};
}

function idb_getPostByTitle(dbName, dbStoreName, postTitle)
{
	const idb = window.indexedDB;
	const request = idb.open(dbName);

	request.onerror = function (e)
	{
		console.log('error', e);
	};

	request.onsuccess = function (e)
	{
		const db = e.target.result;

		const transaction = db.transaction(dbStoreName, 'readonly');
		const objectStore = transaction.objectStore(dbStoreName);
		const request = objectStore.index('title').get(postTitle);

		request.onerror = (e) =>
		{
			console.log('getPostByTitleRequest_error:', e.target.error);
		};

		request.onsuccess = (e) =>
		{
			console.log('postByTitle:', e.target.result);
		};
	};
}



// UPDATED TO USE PROMISES!

function idb_getDatabase(dbName)
{
	const idb = window.indexedDB;
	const request = idb.open(dbName);

	return new Promise((resolve, reject) =>
	{
		request.onsuccess = (e) =>
		{
			resolve(e.target.result);
		};

		request.onerror = (e) =>
		{
			reject(e.target.error);
		};
	});
}

function idb_getObjectStore(db, dbStoreName, readonly)
{
	// Defaults
	readonly = (readonly == null) ? true : readonly;

	const transaction = db.transaction(dbStoreName, (readonly) ? 'readonly' : 'readwrite');
	const objectStore = transaction.objectStore(dbStoreName);

	return objectStore;
}

function idb_getDataByKey(objectStore, key)
{
	const request = objectStore.get(key);

	return new Promise((resolve, reject) =>
	{
		request.onsuccess = (e) =>
		{
			console.log('success');
			resolve(e.target.result);
		};

		request.onerror = (e) =>
		{
			console.log('error');
			reject(e.target.error);
		};
	});
}

// EXAMPLE WITH PROMISES:

idb_getDatabase(dbName, dbStoreName)
	.then(result =>
	{
		const db = result;
		const dbObjectStore = idb_getObjectStore(db, dbStoreName, true);

		idb_getDataByKey(dbObjectStore, dbKey)
			.then(result =>
			{
				console.log('dbRequestPromise__success', result);
			})
			.catch( error => {
				console.warn('dbRequestPromise__ERROR', error);
			});

	})
	.catch(error =>
	{
		console.warn('idb_getDatabase_error:', error);
	});
