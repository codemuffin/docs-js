import { $app } from "../data/data-actions.js";
import { getAppOptions } from './../data/data-actions.js';

/*global EnlighterJS*/

/**
 * Initialise Enlightener, a code highlighter
 *
 * This init is required every time a post is opened
 */
export function enlightenerInit()
{
	if (typeof EnlighterJS === 'object')
	{
		const theme = getAppOptions('libs', 'enlightenerTheme');

		EnlighterJS.init('pre:not(.data-panel)', 'code',
		{
			language: 'javascript',
			theme: theme,
			indent: 2,
			linenumbers: false,
		});

		// Remove the Enlightener button badge
		$app.find('.enlighter-btn-website').addClass('uk-hidden');
	}
	else
	{
		console.warn('[ENLIGHTENER] No Enlightener object is available (window.EnlighterJS). Have you included the script?');
	}
}
