# JWT Authentication (#unused)

Debug JWT with this hidden page:

  - https://codemuffin.com/dev-authenticate

There's a bit of setup involved to support JWT.

## Plugins

Install 2 plugins:

  - https://wordpress.org/plugins/jwt-authentication-for-wp-rest-api/
  - https://wordpress.org/plugins/disable-rest-api-and-require-jwt-oauth-authentication/

### CORS Requests

If you're authenticating from a separate domain (CORS), the 2nd plugin requires a manual edit:

  1. Go to line 40, which starts with `return new WP_Error`
  1. Replace the status code `401` with `200`.

This edit ensures potentially valid CORS requests do not fail at the preflight stage.

Otherwise, failing at preflight would give you an error saying http status 200/"ok" is required to progress.

_Note: If you intend to use this in production I recommend publishing a fork of the edited plugin._

## htaccess

Add this to .htaccess:

    # Enable HTTP Authorization Header, for REST API: JWT Token Authentication
    RewriteEngine on
    RewriteCond %{HTTP:Authorization} ^(.*)
    RewriteRule ^(.*) - [E=HTTP_AUTHORIZATION:%1]
    SetEnvIf Authorization "(.*)" HTTP_AUTHORIZATION=$1

## wp-config

Finally, add this to wp-config:

    define( 'JWT_AUTH_SECRET_KEY', 'random_secret_key' );

## Require Login

If you want to require a login to access the data source site, this plugin has been tested with the described JWT setup:

  - https://wordpress.org/plugins/fx-private-site/
