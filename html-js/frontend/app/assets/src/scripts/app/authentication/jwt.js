import { setCookie, getCookie } from '../utils/cookies.js';

// JWT Authentication (#unused)
// See jwt.md for details


// Exports
// ============================================================================

export const tokenCookieName = 'jwt-auth-token';

export function jwtAuthenticate(host, user, pass, callback)
{
	const authUrl = `${host}/wp-json/jwt-auth/v1/token`;
	const logins = {
		'username': user,
		'password': pass
	};

	console.log('[jwtAuthenticate] Pending...', { host, authUrl, logins });

	$.post(authUrl, logins)

		.then(function (response)
		{
			console.log('[jwtAuthenticate] Response', response);

			jwtCookieSet(response.token);

			if (typeof callback === 'function')
			{
				callback(response);
			}
		})

		.catch(function (error)
		{
			console.warn('[jwtAuthenticate] Error', error);

			if (typeof callback === 'function')
			{
				callback(error);
			}
		});
}

export function jwtValidate(host, token, callback)
{
	const authUrl = `${host}/wp-json/jwt-auth/v1/token/validate`;

	console.log('[jwtValidate] Pending...', { host, token });

	$.ajax({
		url: authUrl,
		type: 'post',
		headers: {
			'Authorization': `Bearer ${token}`,
			'Content-Type': 'application/json'
		},
		dataType: 'json',
		success: function (response)
		{
			console.log('[jwtValidate] Success', response);
		},
		error: function (error)
		{
			console.warn('[jwtValidate] Error', error);
		},
		complete: function (data)
		{
			if (typeof callback === 'function')
			{
				callback(data);
			}
		}
	});
}

export function jwtGetPost(host, endpoint, callback)
{
	const requestUrl = `${host}/wp-json/wp/v2/${endpoint}`;
	const token = jwtCookieGet();

	console.log('[jwtGetPost] Requesting post...', { host, endpoint, requestUrl, token });

	$.ajax({
		url: requestUrl,
		type: 'get',
		headers: {
			'Authorization': `Bearer ${token}`,
			'Content-Type': 'application/json'
		},
		dataType: 'json',
		success: function (response)
		{
			console.log('[jwtGetPost] Success:', response);
		},
		error: function (error)
		{
			console.warn('[jwtGetPost] Error:', error);
		},
		complete: function (data)
		{
			if (typeof callback === 'function')
			{
				callback(data);
			}
		}
	});
}

export function jwtPostUpdate(host, endpoint, postData)
{
	const postUrl = `${host}/wp-json/wp/v2/${endpoint}`;
	const token = jwtCookieGet();

	// Stringify postData, if an object was passed
	postData = (typeof postData !== 'string') ? JSON.stringify(postData) : postData;

	console.log('[jwtPostUpdate] Pending...', { host, endpoint, postUrl, postData, token });

	$.ajax({
		url: postUrl,
		type: 'post',
		data: postData,
		headers: {
			'Authorization': `Bearer ${token}`,
			'Content-Type': 'application/json'
		},
		dataType: 'json',
		success: function (response)
		{
			console.log('[jwtPostUpdate] success', response);
		},
		error: function (error)
		{
			console.warn('[jwtPostUpdate] error', error);
		},
	});
}


// Internal
// ============================================================================

function jwtCookieSet(token)
{
	setCookie(tokenCookieName, token);
}

function jwtCookieGet()
{
	getCookie(tokenCookieName);
}
