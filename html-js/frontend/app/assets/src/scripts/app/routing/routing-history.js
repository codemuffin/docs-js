import { setScreenByCurrentUrl } from '../ui/screens.js';
import { prepareUrlPath, isUrlSibling, isUrlParentOf, getUrlPathPart } from "../utils/url-string.js";
import { setupArchivePost } from '../screens/screen-archive.js';
import { getAppCurrent, getAppOptions } from "../data/data-actions.js";
import { closePost } from '../ui/posts.js';

// Array containing the browser's URL history
const userHistory = [];

/**
 * Sets the initial pathName, on first load
 */
export function setupHistory()
{
	userHistory.push(window.location.pathname);

	// Track popstate change events (back/forward button used)
	onPopState();
}

export function updateHistory()
{
	userHistory.push(window.location.pathname);
}

/**
 * User changes their history, by using the browser's back/foward buttons
 */
function onPopState()
{
	window.onpopstate = () =>
	{
		if (getAppOptions('debug', 'logRouting'))
		{
			console.log('history:pop ', window.location.pathname);
		}

		// Add this new item to the history object
		updateHistory();

		const currentScreen = getAppCurrent('screen');

		if (currentScreen.screenType === 'archive')
		{
			// Check the current path, against the previous one:
			// Has the pathname base (ie archive page) changed?
			// If the base path hasn't changed, we're probably on the same archive page,
			// so we don't need to reload everything, and can just display the single post :)
			const curr = prepareUrlPath(userHistory[userHistory.length - 1]);
			const prev = prepareUrlPath(userHistory[userHistory.length - 2]);
			const isSibling = isUrlSibling(curr, prev);
			const isParent  = isUrlParentOf(curr, prev);

			if (isSibling)
			{
				// User went back/forward to view a post in the same archive,
				// so we only need to load that post
				const postType  = currentScreen.postType;
				const slug = getUrlPathPart(curr, 2);

				setupArchivePost(slug, postType);
				return;
			}
			else if (isParent)
			{
				// Current path is a parent of the last, so must be the parent archive
				closePost();
				return;
			}
		}

		// Pages always require a full screen change
		setScreenByCurrentUrl();
	};
}

