import { $app } from "../data/data-actions.js";
import { setScreenByUrlProperty, changeScreen, setScreenTo404 } from "../ui/screens.js";
import { isUrlRelative, isUrlRestDomain, isUrlSameDomain, urlToObject, getUrlPathPart, isUrlFilename } from "../utils/url-string.js";
import { getScreenByUrlPath } from '../data/data-screens.js';

export function setupLinkRoutingEvents()
{
	onLinkClick();
}

/**
 * User clicks an HTML link
 */
function onLinkClick()
{
	$app.on('click', 'a', e =>
	{
		/**
		 * @TODO: Ignore file extensions and wp-admin links
		 */

		e.preventDefault();

		const link = e.currentTarget;
		let url = link.getAttribute('href');

		if (url === '#')
		{
			e.preventDefault();
		}
		else if (isUrlFilename(url))
		{
			window.open(url, '_blank');
		}
		else if (link.getAttribute('target') === '_blank')
		{
			window.open(url, '_blank');
		}
		else if (link.hasAttribute('data-app-screen-link'))
		{
			// Internal screen links
			e.preventDefault();
			setScreenByUrlProperty(url);
		}
		else if (isUrlRelative(url) || isUrlSameDomain(url) || isUrlRestDomain(url))
		{
			// Internal URLs
			e.preventDefault();

			const screen = getScreenByUrlPath(url);
			const urlObj = urlToObject(url);

			// Screens
			if (screen)
			{
				// Check for children (posts)
				const childPath = getUrlPathPart(urlObj.pathname, 2);

				if (childPath)
				{
					changeScreen(screen, true, urlObj.pathname);
					// setupArchivePost(childPath, screen.postType, false);
				}
				else
				{
					changeScreen(screen, true);
				}
			}
			else
			{
				// Relative, but no screen found
				setScreenTo404(urlObj.pathname);
			}
		}
		else
		{
			// External: Open in a new tab
			window.open(url, '_blank');
		}
	});
}

