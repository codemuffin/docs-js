import { updateHistory } from '../routing/routing-history.js';
import { setDefault } from '../utils/object.js';
import { prepareUrlPath, decodeEntity } from '../utils/url-string.js';
import { getAppOptions } from '../data/data-actions.js';

// Browser utilities

/**
 * Update the page title.
 *
 * This will be displayed as the tab name.
 * If the setUrl method is used (or any other pushState function uses this
 * method), the new title will also be displayed in the user's history
 *
 * @param {string} title The new page title
 */
export function setDocumentTitle(title, subtitle)
{
	if( title )
	{
		const siteName  = getAppOptions( 'metaContent', 'defaultTitle' );
		const sep       = getAppOptions( 'metaContent', 'titleSep' );
		// const subSep    = getAppOptions( 'metaContent', 'titleSubSep' );

		// Old version (Base > Page)
		// title = (subtitle) ? title.concat(subSep, decodeEntity(subtitle)) : title;
		// document.title = titleBase + sep + title;

		title = decodeEntity( ( subtitle ) ? subtitle : title );

		document.title = `${title} ${sep} ${siteName}`;
	}
}

/**
 * Upodate the page meta description
 *
 * Test in the browser:
 * document.querySelector( 'meta[name="description"]' ).getAttribute( 'content' );
 *
 * @param   {string}  description  New meta description, eg. psot excerpt
 *
 * @return  {void}
 */
export function setDocumentMetaDescription( description )
{
	const newDescription = ( description ) ? description : getAppOptions( 'metaContent', 'defaultDescription' );

	$( 'meta[name="description"]' ).attr( 'content', newDescription );
}

/**
 * Set the URL to a given path, and change the page title, using history.pushState
 *
 * @param {string}  path                URL to change to
 * @param {string=} [title=null]        New page title. Generally ignored in the pushState method, so instead, we change the page title with setDocumentTitle()
 * @param {object=} [stateObject=null]  Object to store data in. Can be accessed with window.onpopstate
 */
export function setUrl(path, title, stateObject)
{
	if (getAppOptions('debug', 'logRouting'))
	{
		console.log('history:push', path);
	}

	// Defaults
	title 		= setDefault(title, null);
	stateObject	= setDefault(stateObject, null);

	// Sanitise path
	path = prepareUrlPath(path);

	window.history.pushState( stateObject, title, '/' + path );

	// Update the history
	updateHistory();
}