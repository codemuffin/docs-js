
// Object utilities
// Generally usable with any object, including arrays and strings.
// Note that dedicated string functions have their own utility

/**
 * Check if an object has the supplied property, via hasOwnProperty
 *
 * @param   {object}  obj   Object to check
 * @param   {string}  prop  Property to check for
 *
 * @return  {bool|null}     True if it contains the property, false if not, null if no object or property string were provided
 */
export function objectHasProp( obj, prop )
{
	if ( typeof obj !== 'object' || typeof prop !== 'string' || prop.length === 0 )
	{
		return null;
	}

	return ( Object.prototype.hasOwnProperty.call( obj, prop ) );
}

/**
 * Set a default value for a passed variable, if that variable is either null
 * or undefined. Does not affect initial falsey values.
 *
 * @param   {mixed}  currentVar  The current variable
 * @param   {mixed}  defaultVar  If currentVar is null/undefuned, this value will be returned
 *
 * @return  {mixed}              The current value if it was defined, the default value if not
 */
export function setDefault(currentVar, defaultVar)
{
	return (currentVar == null) ? defaultVar : currentVar;
}

/**
 * Traverses down an object, returning the value of the key specified
 *
 * @param  {object}        obj   Object to traverse
 * @param  {array|string}  keys  Key(s) to retrieve the value of. Accepts string or array
 * @return {mixed}				 The final found value of the specified key
 */
export function getObjectPropertyRecursive(obj, ...keys)
{
	const logWarnings = false; // use only when debugging, as null data may be excpected, but will still log a warning

	let output = null;

	if (keys && keys.length > 0)
	{
		if (keys.length === 1)
		{
			const key = keys[0];

			if (obj.hasOwnProperty(key))
			{
				output = obj[key];
			}
			else if (logWarnings)
			{
				console.warn(`[getObjectPropertyRecursive]\nRequested "${key}" is not a property of this object:`, obj);
			}
		}
		else
		{
			// If we have more keys to go throughy, loop this function into itself
			// @TODO: Could we implement this better, without oversimplifying, and while maintaining the error warning?
			const key = keys.shift();
			output = getObjectPropertyRecursive(obj[key], ...keys);
		}
	}
	else
	{
		output = obj;
	}

	return output;
}

/**
 * Convert an array of objects (containing a unique key value) into a new object,
 * with the key values as the new object keys.
 *
 * @param   {array}   array  Array containing objects with a unique key property
 * @param   {string}  key    Name of key property (e.g., 'id')
 *
 * @return  {object}         Object, converted from array
 */
export function arrayToObject(array, key)
{
	// Original
	// const arr2obj = {};
	// return array.forEach(item => Object.assign(arr2obj, { [item[key]]: item }));

	// Faster version, using reduce
	// Appends item to obj, starting from an empty object
	// https://medium.com/dailyjs/rewriting-javascript-converting-an-array-of-objects-to-an-object-ec579cafbfc7
	const arr2obj = array.reduce((obj, item) =>
	{
		obj[item[key]] = item;
		return obj;
	}, {});

	return arr2obj;
}

/**
 * Custom deferred promise object. A promise that you can resolve
 * from outside of its constructor. Intended to replace jQuery
 * "deferreds", until a larger re-write is done.
 *
 * https://gist.github.com/stefanmaric/895f51652060a820e2ee7f164af87948
 *
 * @example
 *
 * const def = new Deferred();
 * def.then(data => console.log(data));
 * setTimeout(() => def.resolve('done!'), 5000);
 *
 */
export function Deferred()
{
	/**
	 * The resolve and reject methods are normally only available from within
	 * a promise object's executor function. Here, we're making them available
	 * from outside of the promise, by assigning the resolve and reject methods
	 * to the promise object itself.
	 *
	 * How this works:
	 * 	Create a new promise.
	 * 	In its executor, assign the resolve & reject methods to propsHolder.
	 * 	Outside of the promise, assign the new resolve/reject methods to the original promise.
	 */

	const propsHolder = {};

	return Object.assign(
		new Promise((resolve, reject) => Object.assign(propsHolder, { resolve, reject })),
		propsHolder
	);
}

/**
 * Sorts an object by a specified key, and returns an array
 *
 * @param   {object}  obj  Object to sort
 * @param   {string}  key  Sort key
 *
 * @return  {array}        Sorted object, as an array
 */
export function sortObjectByKey(obj, key, subKey)
{
	const objArr = Object.values(obj).sort((a,b) =>
	{
		const sA = (!subKey ? a[key] : a[key][subKey]).toUpperCase();
		const sB = (!subKey ? b[key] : b[key][subKey]).toUpperCase();

		if (sA < sB)
		{
			// Lower sort order, if A comes before B
			return -1;
		}
		else if (sA > sB)
		{
			// Higher sort order, if A comes after B
			return 1;
		}
		else
		{
			// Same sort order, if items are identical
			return 0;
		}
	});

	// Return as an object
	// const returnObj = Object.assign({}, objArr);

	// Return as an array
	return objArr;
}

export function deleteObjectProperty(obj, prop)
{
	if (obj.hasOwnProperty(prop))
	{
		delete obj.prop;
	}
}
