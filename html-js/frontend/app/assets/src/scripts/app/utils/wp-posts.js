
/**
 * Update WP's native post types, which have wonky naming
 *
 * @param   {string} restBase   Original post type
 *
 * @return  {string}            Updated post type name
 */
export function transformWpPostTypes(postType)
{
	postType = (postType === 'post' ? 'posts' : postType);
	postType = (postType === 'page' ? 'pages' : postType);

	return postType;
}

/**
 * Returns the REST base for native WP post types.
 *
 * For the inverse of this, use getNativePostTypeFromRestBase
 *
 * 	postType: 'post' / restBase: 'posts'
 * 	postType: 'page' / restBase: 'pages'
 *
 * @param   {string}  postType  Name of native post type ("post" or "page")
 *
 * @return  {string}            REST base for this native post type ("posts" or "pages")
 */
export function getNativeRestBaseFromPostType(postType)
{
	postType = (postType === 'post' ? 'posts' : postType);
	postType = (postType === 'page' ? 'pages' : postType);

	return postType;
}

/**
 * Returns the post type used by a native WP REST base.
 *
 * For the inverse of this, use getNativeRestBaseFromPostType
 *
 * 	postType: 'post' / restBase: 'posts'
 * 	postType: 'page' / restBase: 'pages'
 *
 * @param   {string}  restBase  Native post type's REST base ("posts" or "pages")
 *
 * @return  {string}            REST base for this native post type ("post" or "page")
 */
export function getNativePostTypeFromRestBase(restBase)
{
	restBase = (restBase === 'posts' ? 'post' : restBase);
	restBase = (restBase === 'pages' ? 'page' : restBase);

	return restBase;
}