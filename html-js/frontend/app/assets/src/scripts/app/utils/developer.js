
// Developer utilities

/**
 * Get the events atatched to an element
 *
 * @param  {string|jQuery} element The element to check for attached events. Accepts both standard elements -- eg $('.myDiv')[0] -- and jQuery element objects
 *
 * @return {array} Array containing events atatched to an element
 */
export function getAttachedEvents(element)
{
	element = ( element instanceof jQuery ) ? element[0] : element;

	return jQuery._data( element, 'events' );
}
