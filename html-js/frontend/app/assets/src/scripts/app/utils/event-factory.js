import { $app } from "../data/data-actions.js";

/**
 * Event Factory (#unused)
 *
 * This was an experiment. It could be useful in some situations, but it removes the ability
 * to view the original event attacher module in devtools (in the Event Listeners tab).
 * Still, it could be expanded to avoid having to register a ton of event handlers
 */

let factoryEvents = {};

function eventFactoryObjectBuilder(eventType, target, callback)
{
	const eventDataId = `${eventType}:${target}`;

	const eventData = {
		[eventDataId]: {
			'type': eventType,
			'target': target,
			'callback': callback.name,
		}
	};

	Object.assign(factoryEvents, eventData);
}

// Helps keep code DRY, and avoids the need to manually add each event listener to an init function
export function eventFactory(eventType, target, callback)
{
	$app.on(eventType, target, (e, ...args) =>
	{
		if (typeof callback === 'function')
		{
			callback(e, args);
		}
	});

	// Not necessary, but can help with debugging events
	eventFactoryObjectBuilder(eventType, target, callback);
}
