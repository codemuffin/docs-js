
// Cookie utilities

/**
 * Get a cookie value by key
 *
 * @param {string}             key  Cookie name key
 *
 * @return {string|undefined}       Cookie value, or undefined if the cookie does not exist
 */
export function getCookie( key )
{
	const cookiesSplit = document.cookie.split( ';' );

	for ( let i = 0; i < cookiesSplit.length; i++ )
	{
		const pair = cookiesSplit[i].trim(); // trim whitespace after each ";"

		if ( pair.indexOf( key + '=' ) === 0 )
		{
			return pair.split( '=' )[1];
		}
	}

	return undefined;
}

/**
 * Set a cookie. Creates new cookies or updates existing ones
 *
 * @todo: Add expiry param, cookies expire in 2030 atm
 *
 * @param   {string}  key    Cookie name key
 * @param   {string}  value  Cookie value
 *
 * @return  {void}
 */
export function setCookie( key, value )
{
	const keyValue = `${key}=${value}`;
	const expiry   = new Date( 'January 1, 2030' ).toUTCString();
	const path     = 'path=/';

	const cookieString = [ keyValue, expiry, path ].join( ';' );

	document.cookie = cookieString;
}
