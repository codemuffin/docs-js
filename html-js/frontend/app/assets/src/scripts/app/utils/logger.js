import { getJQEl, getSelector } from './../data/data-actions.js';
import { setDefault } from './object.js';

// Logger

/**
 * Check for the presence of the logger markup
 */
export function checkLoggerExists()
{
	if (!document.getElementsByClassName('logbox').length)
	{
		console.warn( 'The Logger has been used, but no .logbox element exists' );
	}
}

/**
 * Log data to the logbox
 *
 * @param {(string|array)} 	message 	Message to log. Accepts an array of message strings
 * @param {string} 			title   	Title. Optional, but recommended
 * @param {string} 			titleClass	Title colour class, adds uk-text-* to the title element. E.g., 'warning', 'success'
 */
export function log(message, title, titleClass)
{
	let $log = getJQEl('logboxCls');
	let logboxLiClass = getSelector('logboxLiCls');

	// Required data was passed. Proceed!
	if( message !== undefined )
	{
		// Title (if applicable)
		if( title !== undefined )
		{
			// Add a title class. You can pass null if you need an empty title, for some reason.
			titleClass = ( titleClass !== undefined && titleClass !== null )
				? 'uk-text-' + titleClass
				: '';

			// Log the title
			$log.append( '<li class="logbox-message-title ' + titleClass + '">' + title + '</li>' );
		}

		// Support for passing an array (of strings) as the message (instead of a simple string)
		// Arrays are listed as <li>s, nested within a single <ul>
		if( Array.isArray(message) )
		{
			$log.append( '<li class="' + logboxLiClass + '"><ul>' );

				$.each( message, function( i, messageLine )
				{
					$log.append( '<li>' + messageLine + '</li>' );
				});

			$log.append( '</ul></li>' );
		}

		// Standard string logging. This is the minimum requirement for using the logger, although a title is highly recommended, for clarity
		else
		{
			$log.append( '<li class="' + logboxLiClass + '">' + message + '</li>' );
		}
	}

	// Error handling: Undefined message
	else
	{
		console.warn( 'The Log method was used, but no message was passed' );
	}
}

/**
 * Logs the app progress to Chrome's developer console
 *
 * @param   {string}  message      Console log message
 * @param   {string}  borderColor  Color, as a CSS colour value (e.g., "yellow", "#fff")
 */
export function logProgress(message, borderColor)
{
	borderColor = setDefault(borderColor, 'white');

	const bg = '#292929'; // in case Dark Mode isn't being used
	const css = `
		background-color: ${bg};
		border-left: 3px solid ${borderColor};
		color: #fff;
		padding-left: 5px;
	`;

	console.log('%c' + message, css);
}