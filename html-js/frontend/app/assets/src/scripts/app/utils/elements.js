
/**
 * Clear the value of a specified input field
 *
 * @param {string} element Input element selector
 */
export function clearInputValue(element)
{
	$(element).val('');
}

/**
 * Hide all elements, except the element(s) specified
 *
 * Toggles element visibiilty using the uk-hidden class
 *
 * @param {string} hide Selector for elements to hide
 * @param {string} show Selector for element(s) to show
 */
export function hideAllElementsExcept(hide, show)
{
	const $show = $(show);
	const $hide = $(hide).not($show);

	$show.removeClass('uk-hidden');
	$hide.addClass('uk-hidden');
}

export function setElementBackground(color, $el)
{
	// Allowed colours
	const colorsArr = ['blue', 'green', 'orange', 'purple', 'red', 'yellow'];

	// Validation
	if (!setBackgroundColorValidation(color, $el, colorsArr)) { return; }

	const bgClassPrefix = 'uk-background-color-';

	colorsArr.forEach(c =>
	{
		$el.removeClass(bgClassPrefix + c);
	});

	$el.addClass(bgClassPrefix + color);
}

function setBackgroundColorValidation(color, $el, colorsArr)
{
	if (color == null)
	{
		console.warn(`[error_setBackgroundColor] A colour is required. Acceptable colours are:`, colorsArr);
		return false;
	}
	else if (!colorsArr.includes(color))
	{
		console.warn(`[error_setBackgroundColor] Requested colour "${color}" is not an allowed colour. Acceptable colours are:`, colorsArr);
		return false;
	}
	else if (!$el || $el.length === 0)
	{
		console.warn('[error_setBackgroundColor] The specified element does not appear to exist. Your element was:', $el);
		return false;
	}

	return true;
}