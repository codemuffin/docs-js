import { arrayToObject } from './object.js';

/**
 * Produce a colour from a given string
 *
 * @param {string} str String to convert to a colour
 */
export function hexFromString(str)
{
	return '#' + stringToColor(str);
}

/**
 * Convert a string to a hex colour
 *
 * Tbh I don't understand a lot of this! But it works well,
 * and one copied snippet across the whole app isn't too bad :D
 *
 * via: https://www.designedbyaturtle.co.uk/demos/hex-colour/
 *
 * @param   {string}  str  String to convert to colour
 * @return  {string}       Hex colour string
 */
function stringToColor(str)
{
	// Hash the string into an integer value, by
	// getting each character's Unicode int code
	let hash = 0;

	for (let ch of str)
	{
		hash = ch.charCodeAt(0) + ((hash << 5) - hash); // not sure what this is doing!
	}

	// intToARGB
	// Convert an int to hexadecimal with a max length
	// of six characters.
	let hex = ((hash >> 24) & 0xFF).toString(16) +
		((hash >> 16) & 0xFF).toString(16) +
		((hash >> 8) & 0xFF).toString(16) +
		(hash & 0xFF).toString(16);

	// Add trailing zeros to prevent strings < 6 characters
	hex += '000000';

	// Return only the first 6 characters
	let output = hex.substring(0, 6);

	return output;
}

/**
 * Convert a standard date string to a Y-M-D string
 *
 * @param   {string}  date  Date string (standard format)
 * @param   {boolean} isObj Set to true if you're passing a date object, rather than a date string
 *
 * @return  {string}        Date, as Y-M-D
 */
export function dateToYMD(date, isObj)
{
	const dateObj = (!isObj ? new Date(date) : date);

	// const shortDate = `${dateObj.getFullYear()}-${dateObj.getMonth()}-${dateObj.getDate()}`;
	// const shortDate = new Intl.DateTimeFormat('en-GB').format(dateObj);

	const datePartsArr = new Intl.DateTimeFormat('en-GB').formatToParts(dateObj);
	const datePartsObj = arrayToObject(datePartsArr, 'type');
	const { day, month, year } = datePartsObj;
	const YMD = `${year.value}-${month.value}-${day.value}`;

	return YMD;
}

export function dateToTime(date)
{
	const dateObj = new Date(date);

	let minutes = dateObj.getMinutes().toString();
	let seconds = dateObj.getSeconds().toString();
	minutes = (minutes.length === 1 ? '0' + minutes : minutes); // add leading zero if necessary
	seconds = (seconds.length === 1 ? '0' + seconds : seconds);
	const time = `${dateObj.getHours()}:${minutes}:${seconds}`;

	return time;
}