import { getAppOptions } from '../data/data-actions.js';
import { setDefault } from './object.js';
import { getScreenByPostType } from '../data/data-screens.js';

/**
 * Prepares a URL path, removing all unnecessary slashes
 *
 * Example: "//hello/world/" becomes "hello/world"
 */
export function prepareUrlPath(url)
{
	// Defaults
	url = setDefault(url, '');

	// Replace any double slashes with a single slash
	url = url.replace( /\/\//g, '/' );

	// Trim leading and trailing slashes from a string
	url = url.replace( /^\/|\/$/g, '' );

	return url;
}

/**
 * Get properties of a URL, like with window.location.{partName}
 *
 * @param   {string}  url   Input URL
 * @param   {string}  part  URL part to return
 *
 * @return  {string}        URL part
 */
export function getUrlPart(url, part)
{
	switch (part)
	{
		case 'protocol':	return getUrlProtocol(url);
		case 'host':		return getUrlHost(url);
		case 'origin':		return getUrlOrigin(url);
		case 'restPath':	return getUrlRestPath(url);
		case 'queryString':	return getUrlQueryString(url);
	}
}

/**
 * Returns the protocol from a URL string, eg "https://"
 *
 * @param   {string}  url  URL string
 *
 * @return  {string}       Protocol string, or an empty string if the URL contains no protocol
 */
export function getUrlProtocol(url)
{
	const http 	= 'http://';
	const https = 'https://';

	const isHttp  = url.includes(http);
	const isHttps = url.includes(https);

	let protocol = (isHttp ? http : (isHttps ? https : false));

	// Error handling & set empty string
	if (!protocol)
	{
		//@TODO: Is this warning needed?
		// console.warn('[error_getUrlProtocol] URL is invalid, as it does not contain either "http://" or "https://". Supplied URL:', url);
		protocol = '';
	}

	return protocol;
}

/**
 * Returns the host from a URL string, eg "www.website.com"
 *
 * @param   {string}  url  URL string
 *
 * @return  {string}       Host string, or an empty string if the URL contains no host
 */
export function getUrlHost(url)
{
	const protocol = getUrlProtocol(url);
	let noProtocol = url.replace(protocol, '');

	// Ensure there's a trailing slash; this allows us to seperate the host from any other URL parts
	noProtocol = (noProtocol.lastIndexOf('/') === -1 ? noProtocol.concat('/') : noProtocol);

	const slashIndex = noProtocol.indexOf('/');
	const host = (slashIndex ? noProtocol.substring(0, slashIndex) : '');

	return host;
}

/**
 * Returns the origin from a URL string, eg "https://www.website.com"
 *
 * @param   {string}  url  URL string
 *
 * @return  {string}       Origin string, or an empty string if the URL contains no origin
 */
export function getUrlOrigin(url)
{
	const protocol = getUrlProtocol(url);
	const host = getUrlHost(url);
	const origin = protocol + host;
	return origin;
}

export function getUrlRestPath(url)
{
	const urlObj = urlToObject(url);
	const origin = urlObj.origin;
	const qs = urlObj.search;

	// Remove origin, query string, and REST route base
	url = url.replace(origin, '');
	url = url.replace(qs, '');
	url = url.replace(getAppOptions('ajax', 'routeBase'), '');

	return url;
}

/**
 * Returns the query string from a URL string, eg "?showTabs=true&hideNumbers=false"
 *
 * @param   {string}  url                 URL string
 * @param   {string}  returnQuestionMark  Return the leading ? in the string (same as with window.location.search)
 *
 * @return  {string}                      Query string, or an empty string if the URL contains no query string
 */
export function getUrlQueryString(url, returnQuestionMark)
{
	returnQuestionMark = setDefault(returnQuestionMark, false);

	const qsIndex = url.indexOf('?', 0);
	let qs = (qsIndex ? url.substring(qsIndex, url.length) : '');

	if (!returnQuestionMark)
	{
		qs = qs.replace('?', '');
	}

	return qs;
}

export function isUrlSameDomain(url, ignoreProtocol)
{
	ignoreProtocol = setDefault(ignoreProtocol, true);

	const part = (ignoreProtocol) ? 'host' : 'origin';
	const urlOrigin    = urlToObject(url)[part];
	const windowOrigin = window.location[part];

	return (urlOrigin === windowOrigin);
}

export function isUrlRestDomain(url, ignoreProtocol)
{
	ignoreProtocol = setDefault(ignoreProtocol, true);

	const part = (ignoreProtocol) ? 'host' : 'origin';
	const urlOrigin = urlToObject(url)[part];
	let restDomain  = getAppOptions('ajax', 'host');

	restDomain = (ignoreProtocol) ? removeUrlProtocol(restDomain) : restDomain;

	return (urlOrigin === restDomain);
}

/**
 * Check two URLs to see if they're siblings (i.e., their base paths match).
 *
 * Used to check current URLs against the last URL in the user's history.
 *
 * @TODO: This currently returns a child path. Is that the expected behaviour?
 *
 * @param   {string}  curr  Current URL
 * @param   {string}  prev  Previous URL
 *
 * @return  {Boolean}       True if the curr and prev are siblings, false otherwise
 */
export function isUrlSibling(curr, prev)
{
	const currBase  = getUrlPathPart(curr, 1);
	const currChild = getUrlPathPart(curr, 2);

	const prevBase  = getUrlPathPart(prev, 1);
	// const prevChild = getUrlPathPart(prev, 2);

	// Share base, and has child? If so, return the child path
	return (currBase === prevBase && currChild ? currChild : false);
}

export function isUrlParentOf(parent, child)
{
	const parentScreen = getScreenByPostType(parent);

	if (parentScreen)
	{
		const childParentPath = (child.indexOf('/') ? child.split('/')[0] : false);
		let childParentScreen = false;

		if (childParentPath)
		{
			childParentScreen = getScreenByPostType(childParentPath);
		}

		return (parentScreen === childParentScreen);
	}
	else
	{
		return false;
	}
}

/**
 * Removes the protocol (http:// or https://) from the start of a URL string
 *
 * @param   {string}  url  Input URL
 *
 * @return  {string}       URL, with the protocol removed. If the URL contained no protocol, the original URL is returned
 */
export function removeUrlProtocol(url)
{
	const regex = new RegExp('^http[s]*://');

	return (regex.test(url)) ? url.replace(regex, '') : url;
}

/**
 * Checks a given URL string for either a leading slash
 * or a host. Returns false if neither are found
 *
 * @param   {string}  url  URL to check
 *
 * @return  {boolean}      True if the URL is relative; false otherwise
 */
export function isUrlRelative(url)
{
	return (url.startsWith('/') || url === removeUrlProtocol(url));
}

export function isUrlFilename(url)
{
	return (urlToObject(url).pathname.split('/').pop().includes('.'));
}

/**
 * Convert an absolute URL to a relative URL.
 *
 * @TODO: Use this instead:
 * 	url = (!isUrlRelative(url)) ? new URL(url).pathname : url;
 *
 * @param   {string}  url     URL to convert
 * @param   {string}  origin  Absolute URL origin (protocol+hostname). Defaults to the current window origin
 *
 * @return  {string}          Relative URL. If the input URL was already relative, returns the input URL
 */
export function urlToRelative(url, origin)
{
	origin = setDefault(origin, window.location.origin);

	return (!isUrlRelative(url)) ? url.replace(`${origin}/`, '') : url;
}

/**
 * Convert a relative URL to an absolute one, by prepending a supplied origin
 *
 * @param   {string}  url     Relative URL
 * @param   {string}  origin  Origin (protocol+hostname), to prepend to the URL. Defaults to the current window origin
 *
 * @return  {string}          Absolute URL
 */
export function urlToAbsolute(url, origin)
{
	origin = setDefault(origin, window.location.origin);

	url = (!url.startsWith('/')) ? `/${url}` : url;

	return (isUrlRelative(url)) ? origin+url : url;
}

/**
 * Returns a new URL object. Converts a relative to an absolute URL if required.
 *
 * @param   {string}  url  URL string
 *
 * @return  {object}       new URL object
 */
export function urlToObject(url)
{
	url = (isUrlRelative(url)) ? urlToAbsolute(url) : url;

	return new URL(url);
}

/**
 * Get part of a URL path. "Parts" are separated by slashes
 *
 * @param  {string=}  [path='']  URL path to extra the path part from. Defaults to the current window's pathname
 * @param  {index=}   [index=1]  Index of the desired path part. NOT zero-based. (Maybe used to be? @TODO: Check src and improve this comment)
 *
 * @return {string}              URL path part
 */
export function getUrlPathPart(path, index)
{
	// Defaults
	path  = setDefault(path, window.location.pathname);
	index = setDefault(index, 0);

	path = (path.startsWith('/')) ? path.slice(1) : path;

	const pathParts = getUrlPathParts(path);

	return pathParts[index - 1];
}

/**
 * Get all parts of a URL path, as an array
 *
 * @param  {string} path  URL path to split into an array. Defaults to the current window's pathname
 *
 * @return {array}        Array containing each part of the URL path
 */
export function getUrlPathParts(path)
{
	// Defaults
	path = setDefault(path, window.location.pathname);

	const parts = path.split( '/' );

	return parts;
}

/**
 * Converts an html characterSet into its original character
 *
 * Example: &#72;&#101;&#108;&#108;&#111;
 *
 * via: https://ourcodeworld.com/articles/read/188/encode-and-decode-html-entities-using-pure-javascript
 *
 * @param {String} str htmlSet entities
 */
export function decodeEntity(str)
{
	return str.replace(/&#(\d+);/g, (match, dec) =>
	{
		return String.fromCharCode(dec);
	});
}

// /**
//  * Get the value of a query string parameter
//  *
//  * #unused: Would need refactoring if used
//  *
//  * @param {string} variable	The query string parameter
//  * @return {string}	Returns a string containing the query string value, or null of no value is available
//  */
// function getQueryStringParamValue(url, variable)
// {
// 	const qs = urlToObject(url).search;
//
// 	if (qs)
// 	{
// 		const params = qs.substring(1).split( '&' );
//
// 		for ( let i in params )
// 		{
// 			const pair = params[i].split( '=' );
//
// 			if( pair[0] === variable )
// 			{
// 				return pair[1];
// 			}
// 		}
// 	}
//
// 	return null;
// }
