import { setDefault } from './object.js';

// Content utilities


/**
 * Removes the last period from a given string, if the period is the last character
 *
 * @param {string} content Content string
 */
export function removeLastPeriod(content)
{
	if( ( content.lastIndexOf( '.' ) - 2 ) === content.length )
	{
		content.slice( 0, ( content.length - 1 ) );
	}

	return content;
}

/**
 * Adds trailing ellipses (...) to a given string
 *
 * @param {string} content Content string
 */
export function addTrailingEllipses(content)
{
	return content + '<span class="uk-display-inline">...</span>';
}

/**
 * Trims content to a desired character length, trimming only full words
 *
 * @param {string} content       Content to trim down
 * @param {Number} maxCharacters Maximum allowed chyaracters (optional)
 */
export function autoExcerpt(content, maxCharacters)
{
	if (!content)
	{
		console.warn('[error_autoExcerpt] content is required, but was not passed');
		return ''; // give the caller something useful back :)
	}

	maxCharacters = setDefault(maxCharacters, 100);

	// Remove <p> tags and last period
	content = content.replace( '<p>', '' ).replace( '</p>', '');
	content = removeLastPeriod( content );

	// If the content isn't long enough to trim, return it unaltered
	if( content.length < maxCharacters )
	{
		return content;
	}

	// Get the index of the last space, and trim after that
	const lastSpace = content.indexOf( ' ', maxCharacters );

	// Trim the content at the last space
	const trimmed = content.slice( 0, lastSpace );

	// Add ellipses
	const output = addTrailingEllipses( trimmed );

	return output;
}
