import { getAppFetched, setAppData } from './data-actions.js';
import { setDefault, arrayToObject } from '../utils/object.js';
import { getLocal } from '../storage/localStorage.js';
import { setupFetchPromises, resolveAppPromise } from './data-promises.js';
import { getNativeRestBaseFromPostType } from '../utils/wp-posts.js';

/*---------------------------------------------------------------------------
 * REST: Setup "Fetched" object
 *---------------------------------------------------------------------------
 * Actions related to REST data, including getting posts and setting up
 * promises. All related data is stored in the fetched app data object.
 *
 * For actual REST requests, see screen-archive and screen-preloader, which
 * perform the original fetch requests. The request response data is then
 * saved to the "fetched" app data object, which is interacted with in this
 * module.
 *
 * For saving/removing cached requests to localStorage: storage/localStorage
 *---------------------------------------------------------------------------*/

export function setupRestData()
{
	setupFetchPromises();
	setupFetchedData();
}

/**
 * Assigns the index of localStorage URLs (stored in localStorage, with the key "restCacheStored")
 * to the fetched data object ("restCacheData").
 *
 * Only runs once, on init
 */
function setupFetchedData()
{
	const restCacheStored = getLocal('restCacheStored');

	// If we have restCacheStored, save it to restCacheData
	if (restCacheStored)
	{
		// Set up the fetched object's cache of fetched REST requests
		setAppFetchedRestCacheData(restCacheStored);

		// Add each localStorage item to the fetched object
		for (const url in restCacheStored)
		{
			const localData = getLocal(url);

			if (localData)
			{
				const { requestType, name } = restCacheStored[url];

				// Save to the fetched cache object
				setAppFetchedRestRequest(requestType, name, localData);

				// Resolve this item's promises
				resolveAppPromise(requestType, name);
			}
		}
	}
}

/*---------------------------------------------------------------------------
 * SET
 *---------------------------------------------------------------------------*/

/**
 * Update the fetched data object.
 *
 * The fetched object stores promises that resolve when data is retreived,
 * as well as restCacheData, which is a log of locally cached data, and
 * actual request response data made by REST AJAX calls.
 *
 * This is private method, used by other accessor methods below to set data
 *
 * @param   {string}   key     Data key (eg, restCacheData, promises, or {requestType} ie posts/terms)
 * @param   {object}   data    Data object to store
 * @param   {boolean}  assign  Assign this data, or set it? (Assign adds your data to the target object, merging at the highest level; set replaces all data with your new data)
 */
function setAppFetched(key, data, assign)
{
	assign = setDefault(assign, true);
	setAppData('fetched', key, data, assign);
}

/**
 * Remove data from the fetched object
 *
 * Used by the preloader, when the user manually clears saved data
 *
 * @param   {string}  key  Fetched data key (posts/terms/promises/restCacheData)
 */
export function clearAppFetched(key)
{
	setAppFetched(key, {}, false);
}

/**
 * Set or update fetched data promises
 *
 * These promises tell us when posts/terms have been loaded
 *
 * @param   {string}  requestType  Type of data (posts/terms)
 * @param   {object}  promiseData  Object containing promises data
 */
export function setAppFetchedPromises(requestType, promiseData)
{
	const data = {
		[requestType] : promiseData
	};

	setAppFetched('promises', data, true);
}

/**
 * Update the local cache (restCacheData), by adding a new item
 *
 * The restCache objects list the routes that have been saved to localStorage,
 * and contain a few other bits of useful data. The request URL is used as
 * the index.
 *
 * This is only used once, when retreived REST data is saved to local storage
 */
export function setAppFetchedRestCacheData(restCacheItem)
{
	setAppFetched('restCacheData', restCacheItem);
}

/**
 * Save request response data to the fetched object
 *
 * @param   {[type]}  requestType  [requestType description]
 * @param   {[type]}  name         [name description]
 * @param   {[type]}  restDataArr  [restDataArr description]
 *
 * @return  {[type]}               [return description]
 */
export function setAppFetchedRestRequest(requestType, name, restDataArr)
{
	// Store data as objects (instead of the default array)
	const objData = {
		[name]: arrayToObject(restDataArr, 'id')
	};

	setAppFetched(requestType, objData);
}

/**
 * Save the huge array containing all posts.
 *
 * This array is created and used by the search, then cached
 *
 * @param   {object}  postsFlat  Object containing all posts, from every fetched post type
 */
function setAllPostsFlat(postsFlat)
{
	setAppFetched('allPosts', postsFlat);
}

/*---------------------------------------------------------------------------
 * GET
 *---------------------------------------------------------------------------*/

/**
 * Returns all posts data, ordered by post type
 *
 * @return  {object}  Object containing all fetched posts data, ordered by post type
 */
export function getAllPosts()
{
	return getAppFetched('posts');
}

/**
 * Returns an array of fetched post types
 */
export function getPostTypes()
{
	return Object.keys(getAllPosts());
}

/**
 * Returns a huge array, containing every fetched post, indexed by post ID
 */
export function getAllPostsFlat()
{
	// Check the cache first
	const allPostsCache = getAppFetched('allPosts');

	if (Object.keys(allPostsCache).length)
	{
		return allPostsCache;
	}

	// No cache was available, so this must be the first request to getAllPostsFlat
	const postsByType = getAllPosts();
	const postsFlat = {};

	for (const type in postsByType)
	{
		if (postsByType.hasOwnProperty(type))
		{
			const posts = postsByType[type];
			Object.assign(postsFlat, posts);
		}
	}

	// Cache the new data object
	setAllPostsFlat(postsFlat);

	return postsFlat;
}

/**
 * Return all posts of a specified post type
 *
 * @param   {string}  postType  Post type
 *
 * @return  {object}            Object containing the post type's posts, indexed by post ID
 */
export function getPostsByType(postType)
{
	const posts = getAppFetched('posts', postType);

	if (posts)
	{
		return posts;
	}
	else
	{
		console.warn(`[error_getPosts] Tried to retreive data for the post type "${postType}, but no data was stored in the fetched app data object"`);
		return null;
	}
}

/**
 * Return all terms of a specified taxonomy
 *
 * @param   {string}  taxonomy  Taxonomy name

 * @return  {object}            Object containing the taxonomy's terms, indexed by term ID
 */
export function getTerms(taxonomy)
{
	const terms = getAppFetched('terms', taxonomy);

	if (terms)
	{
		return terms;
	}
	else
	{
		console.warn(`[error_getTerms] Tried to retreive data for the post type "${taxonomy}, but no data was stored in the fetched app data object"`);
		return null;
	}
}

/**
 * Returns single post data for a specified post
 *
 * @param   {string}  postType  Post type name (eg, guides, posts)
 * @param   {string}  postId    Index of the post @TODO: Replace with post ID
 *
 * @return  {object}            Posts data object
 */
export function getSinglePost(postType, postId)
{
	// Handle WP's native post types, which have a REST path different to their post type name
	postType = getNativeRestBaseFromPostType(postType);

	const postTypeData = getAppFetched('posts', postType);

	let output;

	if (postTypeData)
	{
		const postData = postTypeData[postId];

		// console.log(postData);

		if (postData)
		{
			output = postData;
		}
		else
		{
			console.warn(`[error_getSinglePost_A] Found posts data for the type "${postType}", but couldn't find post data for the given postId "${postId}"`);
			output = null;
		}
	}
	else
	{
		console.warn(`[error_getSinglePost_B] No posts data was found for the post type "${postType}", with post ID "${postId}"`);
		output = null;
	}

	return output;
}
