import { $app, getAppFetched, getAppScreens } from './data-actions.js';
import { setAppFetchedPromises } from './data-rest.js';

/**
 * Return an object containing promise objects, by key (eg, posts)
 *
 * @param   {string}  key   Request type. Options: posts, terms
 *
 * @return  {object}        An object containing the requested promises
 */
export function getAppPromises(key)
{
	const promisesData = getAppFetched('promises');

	return Object.values(promisesData[key]);
}

/**
 * Sets up promises to indicate when each group of posts & terms have loaded.
 *
 * Only runs once, on init.
 *
 * @TODO Replace deferreds with actual promises
 */
export function setupFetchPromises()
{
	const appScreensArr = Object.values(getAppScreens());

	const postTypesArr  = appScreensArr.filter(s => s.postType).map(s => s.postType);
	const taxonomiesArr = appScreensArr.filter(s => s.taxonomy).map(s => s.taxonomy);

	const postTypesDeferreds = {};
	const taxonomyDeferreds  = {};

	postTypesArr.forEach(p => Object.assign(postTypesDeferreds, { [p]: $.Deferred() }));
	taxonomiesArr.forEach(t => Object.assign(taxonomyDeferreds, { [t]: $.Deferred() })); // new Deferred()

	setAppFetchedPromises('posts', postTypesDeferreds);
	setAppFetchedPromises('terms', taxonomyDeferreds);

	const postPromises = Object.values(getAppPromises('posts'));
	const termPromises = Object.values(getAppPromises('terms'));
	const postAndTermPromises = [...postPromises, ...termPromises];

	// Trigger custom events when data is ready
	Promise.all(postPromises).then(() => $app.trigger('app:postsready'));
	Promise.all(termPromises).then(() => $app.trigger('app:termsready'), error => console.log(error));
	Promise.all(postAndTermPromises).then(() => $app.trigger('app:postsandtermsready'));

	// Useful for debugging: Log when promises are fulfilled
	const logWhenReady = false;

	if (logWhenReady)
	{
		$app.on('app:postsready', () => console.log('READY: posts'));
		$app.on('app:termsready', () => console.log('READY: terms'));
		$app.on('app:postsandtermsready', () => console.log('READY: posts + terms'));
	}
}

/**
 * Return a specific promise object, by key and name (eg, posts.guides)
 *
 * @param   {string}  key   Request type. Options: posts, terms
 * @param   {string}  name  Name of post type/taxonomy (eg: guides, guide_categories)
 *
 * @return  {object}        An object containing the requested promises
 */
export function getAppPromise(key, name)
{
	const promisesData = getAppFetched('promises');
	return promisesData[key][name];
}

/**
 * Resolve a fetched promise object
 *
 * @param   {string}  requestType  'postTypes' or 'taxonomies'
 * @param   {string}  name         Name of the post type or taxonomy (eg, 'guides', 'my_custom_taxonomy' etc)
 */
export function resolveAppPromise(requestType, name)
{
	// const fetched = getAppData('fetched');
	const promisesData = getAppFetched('promises');

	if (promisesData.hasOwnProperty(requestType))
	{
		if (promisesData[requestType].hasOwnProperty(name))
		{
			const def = promisesData[requestType][name];
			def.resolve();
		}
		else
		{
			console.log(name, promisesData[requestType]);
			console.warn(`[error_resolveAppPromise] Invalid property name "${name}" (level 2). Valid properties:`, Object.keys(promisesData[requestType]));
		}
	}
	else
	{
		console.warn(`[error_resolveAppPromise] Invalid property name "${requestType}" (level 1). Valid properties:`, Object.keys(promisesData));
	}
}

// /**
//  * Return the state of an promise object
//  *
//  * #unused
//  *
//  * @param   {string}  key   Promise key. Options: posts, terms
//  * @param   {string}  name  Name of post type/taxonomy (eg: guides, guide_categories)
//  *
//  * @return  {string}		Returns the promise state, as a string. Potential outcomes: resolve, pending, rejected
//  */
// function getAppPromiseState(key, name)
// {
// 	const promisesData = getAppFetched('promises');
// 	return promisesData[key][name].state();
// }
