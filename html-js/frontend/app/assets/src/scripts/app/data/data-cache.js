import { getAppFetched, getAppOptions, getAppCurrent } from './data-actions.js';
import { removeLocal, setLocal, getLocal } from '../storage/localStorage.js';
import { setAppFetchedRestCacheData, clearAppFetched } from './data-rest.js';
import { setupPreloadHtml } from '../screens/screen-preloader.js';
import { setupFetchPromises } from './data-promises.js';
import { constructRestRoute } from '../rest/rest-utilities.js';
import { closePost } from '../ui/posts.js';
import { setAppGenerated } from './data-state.js';
import { objectHasProp } from '../utils/object.js';
import { clearGeneratedHtml, setupArchiveScreen } from '../screens/screen-archive.js';


/*---------------------------------------------------------------------------
 * Cache: LocalStorage Actions
 *---------------------------------------------------------------------------
 * Interact with localStorage, to store or remove cached REST requests.
 *
 * For all other REST actions:                data/data-rest
 * For direct interactions with localStorage: storage/localStorage
 *---------------------------------------------------------------------------*/

const dataScreenEls = {
	'cacheTable' : $('.localcache-items-table'),
	'cacheItems' : $('.localcache-items-list'),
	'cacheNotice': $('.localcache-items-notice'),
};

const preloaderEls = {
	'status' : $('.preload-status-update'),
};

/**
 * Save REST response data to localStorage, and update the fetched data
 *
 * This should only run once per user, because after the REST data is initially
 * fetched, data will be retrived from either localStorage (if the app is
 * reloaded), or the fetched data object (if the user stays within the app)
 *
 * @param   {string}  route         Route, as a full REST URL
 * @param   {object}  data          REST response data
 * @param   {string}  screenId      The screen that made the original request
 * @param   {object}  requestType   The type of the original request (posts/terms)
 * @param   {object}  name          The name of the requested item (post type name or taxonomy name, eg "guides")
 */
export function saveRestToLocal(route, data, screenId, requestType, name)
{
	const localSave = getAppOptions('ajax', 'localStorageSave');

	if (localSave)
	{
		// const date = new Date(); // the current date, as we're saving this data right now
		// const count = data.length;

		// Assign data for a locally cached URL to the fetched object.
		// We can refer back to this at any point, to see which REST URLs are available within the cache.
		const restCacheItem = {
			[route]: {
				date  : new Date(),  // Stores when this data was retrived. We're using the current date here, as we're saving this data right now
				count : data.length, // The number of items in the retreived REST data
				screenId,            // The ID of the archive screen that this data corresponds to
				requestType,         // The type of request (posts/terms)
				name                 // Depending on the requestType, either the name of the post type, or the taxonomy term
			}
		};

		setAppFetchedRestCacheData(restCacheItem);

		// Save full route data to localStorage
		setLocal(route, data);

		// Track this saved data to the restCache object in localStorage
		setLocal('restCacheStored', getAppFetched('restCacheData'));
	}
}

export function clearRestCache()
{
	const restCacheStored = getLocal('restCacheStored');

	if (!restCacheStored)
	{
		dataScreenEls.cacheNotice.show();
		dataScreenEls.cacheNotice.text('No local cache data available to clear');
		return;
	}

	// Delete all data from localStorage and appData
	deleteAppLocal();

	// Data: Update screen
	dataScreenEls.cacheTable.hide();
	dataScreenEls.cacheNotice.show().text('Cleared all locally cached REST data');

	// Preloader: Reset
	// resetProgressDataCounts(); // already done, with setupPreloadHtml

	// Reset the preloader and fetched obj promises
	setupFetchPromises();
	setupPreloadHtml(false);

	// Preloader: Update screen
	preloaderEls.status.text('Preload data cleared!');

	console.log('REST Cache: Flushed cache');
}

export function clearRestCacheItem(key, requestType, name)
{
	// Delete item from appData.fetched.restCacheData
	deleteAppLocal(key, requestType, name);

	// Reset the preloader and fetched obj promises
	setupFetchPromises();
	setupPreloadHtml(false);

	// Remove row from data screen's cache items table
	$(`[data-local-cache-row="${key}"]`).remove();

	// Hide table on data screen, if this was the last row (note: we're using 1, instead of 0, because the hidden template counts)
	if ($('[data-local-cache-row]').length === 1)
	{
		dataScreenEls.cacheTable.hide();
		dataScreenEls.cacheNotice.show().text('Cleared all locally cached REST data... the long way ;)');
	}

	console.log('REST Cache: Removed single item');
}

/**
 * Deletes locally cached REST data
 *
 * Data can be deleted as a whole, or specific localStorage items can be
 * deleted individually, by specifying a restUrl
 *
 * @param   {string}  restUrl      REST URL. Optional. Ommit this to delete all REST data cached to localStorage
 * @param   {string}  requestType  Request type: posts, terms
 * @param   {string}  name         Name of post/term (eg "guides", "snippet_categories") -- called restPath
 */
export function deleteAppLocal(restUrl, requestType, name)
{
	const restCacheData = getAppFetched('restCacheData');

	// Delete by specified restUrl
	if (restUrl)
	{
		if (!requestType || !name)
		{
			console.error('[error_deleteAppLocal] Deleting cached data by REST URL also requires requestType and name', {restUrl, requestType, name});
			return;
		}

		if (restCacheData.hasOwnProperty(restUrl))
		{
			// Remove the named item's data from the fetched object (appData.fetched[requestType])
			const types = getAppFetched(requestType);

			if (types.hasOwnProperty(name))
			{
				delete types[name];
			}

			// Remove item from appData.fetched.restCacheData
			delete restCacheData[restUrl];

			// Remove item from localStorage
			removeLocal(restUrl);

			// Update restCacheStored, with the item removed
			setLocal('restCacheStored', restCacheData);
		}
	}
	else
	{
		// Remove all restCache URLs from localStorage
		// @TODO_01: This won't remove data that wasn't stored in the restCacheStored object.
		// 		This should only occur when a dev is tinkering, but adding a fallback might
		//		be useful in case of corrupted data
		Object.keys(restCacheData).forEach(url => removeLocal(url));

		// Empty localeCache data
		clearAppFetched('restCacheData');
		clearAppFetched('posts');
		clearAppFetched('terms');

		// Remove localStorage data
		removeLocal('restCacheStored');
	}
}


/**
 * Clear the fetched (cahceD) posts and terms for the current archive screen
 */
export function clearArchiveScreenCache()
{
	const screen = getAppCurrent('screen');

	if ( !objectHasProp( screen, 'screenType' ) )
	{
		console.error( '[error_clearArchiveScreenCache] screen object does not have expected property "screenType"', { screen } );

		return;
	}

	if (screen.screenType === 'archive')
	{
		const postsRoute = constructRestRoute('posts', screen.postType);
		deleteAppLocal(postsRoute, 'posts', screen.postType);
		setAppGenerated('archives', screen.postType, false);
		clearGeneratedHtml(screen.screenId, 'posts');

		if ( objectHasProp( screen, 'taxonomy' ) && screen.taxonomy )
		{
			const taxRoute = constructRestRoute('terms', screen.taxonomy);
			deleteAppLocal(taxRoute, 'terms', screen.taxonomy);
			setAppGenerated('termBtns', screen.taxonomy, false);
			clearGeneratedHtml(screen.screenId, 'terms');
		}

		closePost(null, true);
		setupArchiveScreen(screen.screenId, screen.postType, screen.taxonomy);
	}
}