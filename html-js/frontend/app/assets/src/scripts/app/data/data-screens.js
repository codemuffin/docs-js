import { getUrlRestPath } from '../utils/url-string.js';
import { getAppScreens } from '../data/data-actions.js';
import { prepareUrlPath, getUrlPathPart, urlToObject } from '../utils/url-string.js';
import { setDefault } from '../utils/object.js';

/**
 * Get screen data, based on the current browser URL
 *
 * @param  {string}   url        URL to check with. Defaults to the window's current URL
 * @param  {boolean}  return404  Should the 404 screen be returned, if no valid screen was found? If set to false, a boolean of "false" is returned. Default: false
 *
 * @return {object}              Screen data
 */
export function getScreenByUrlPath(url, return404)
{
	url = setDefault(url, window.location.href);
	return404 = setDefault(return404, false);

	const urlObj = urlToObject(url);

	// Get the URL path (base & child)
	const currentPath = prepareUrlPath(urlObj.pathname);

	let basePath 	= getUrlPathPart(currentPath, 1);
	const childPath	= getUrlPathPart(currentPath, 2);

	// Homepage support
	// @TODO: Replace this hardcoded reference to the homepage URL
	basePath = ( basePath === '' ) ? basePath = 'home' : basePath;

	// Get the relevant screen, using the screen data
	const screens = getAppScreens();
	let screenData = screens[basePath];

	// If the URL isn't directly to a base path...
	if (!screenData)
	{
		const screensArr = Object.values(screens);

		// Search for this basepath inside our screens data object
		const foundScreenData = screensArr.filter(s => s.postType === basePath);

		if (foundScreenData.length !== 0)
		{
			if (foundScreenData.length > 1)
			{
				console.warn('[warning_deriveScreenDataFromUrl] More than 1 screen was found for the supplied URL');
			}

			//@TODO: Support for cases where there's more than 1 found screen
			screenData = foundScreenData[0];
		}
		else
		{
			// Debug notice, for when a 404 occurs (no screen found for the current URL)
			console.warn('[error_deriveScreenDataFromUrl] No screen available (404). URL data:', { basePath, childPath });

			// Set the screen data to the 404 screen?
			screenData = (return404) ? screens.error404 : false;
		}
	}

	return screenData;
}

/**
 * Get screen data corresponding to a screen's URL property.
 *
 * Used by links with the attribute "data-app-screen-link", which have an
 * href corresponding exactly to the "url" property of a certain screen.
 *
 * For general link routing, see /routing/*
 *
 * @param  {string}   url        Screen ID (which should be the screen key)
 * @param  {boolean}  return404  Should the 404 screen be returned, if no valid screen was found? If set to false, a boolean of "false" is returned. Default: false
 *
 * @return {object}              Screen data
 */
export function getScreenByUrlProperty(url, return404)
{
	return404 = setDefault(return404, false);

	const screens = getAppScreens();
	let screenData;

	if (screens.hasOwnProperty(url))
	{
		screenData = screens[url];
	}
	else
	{
		console.log(`[error_getScreenByUrlProperty] The specified url "${url}" did not correspond to a screen. Available screens are:`, Object.keys(screens).sort());

		// Set the screen data to the 404 screen?
		screenData = (return404) ? screens.error404 : false;
	}

	return screenData;
}



/**
 * Get screenData by post type.
 *
 * Checks all post type properties: postType, postTypeActual,
 * postTypeRest, and archive
 *
 * @param   {string} postType Post type
 * @return  {object}          Screen data object
 */
export function getScreenByPostType(postType)
{
	const screens = getAppScreens();
	const screen = Object.values(screens).filter(s =>
	{
		return s.screenType === 'archive' && (s.postType === postType || s.postTypeActual === postType || s.postTypeRest === postType || s.archive === postType);
	});

	return (screen ? screen[0] : null);
}

/**
 * Get screen data corresponding to a REST route
 *
 * @param   {string}  route  Full REST route (URL string, including the protocol)
 *
 * @return  {object}         Screen data object
 */
export function getScreenByRestRoute(route)
{
	let screenData = false;
	const routePath = getUrlRestPath(route);

	// Check screen data
	const screens = getAppScreens();

	for (let s in screens)
	{
		// Skip if screenData has been found
		if (screenData)
		{
			continue;
		}

		const screen = screens[s];

		const matchPostType 	  = (screen.hasOwnProperty('postType') && screen.postType === routePath);
		const matchPostTypeActual = (screen.hasOwnProperty('postTypeActual') && screen.postTypeActual === routePath);
		const matchTaxonomy 	  = (screen.hasOwnProperty('taxonomy') && screen.taxonomy === routePath);

		if (matchPostType || matchPostTypeActual || matchTaxonomy)
		{
			// screenId = screen.screenId;
			screenData = screen;
		}
	}

	// return screenId;
	return screenData;
}