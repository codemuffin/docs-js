import { getObjectPropertyRecursive, setDefault } from './../utils/object.js';
import { defaultAppData } from './../config/appData.js';
import { getLocal } from './../storage/localStorage.js';
import { loadingText } from '../config/loading-text.js';

// Handles get and set of app data
// Data with unique get/set functions are in their own module

/**
 * The main data store for the app
 *
 * This data is only accessible by functions in this module,
 * so interactions are handled like an API
 */
let appData = defaultAppData;

/**
 * Root $app jQuery object
 *
 * Used as the target for events.
 *
 * Get other config-set selectors/els with getSelector and getJQEl.
 */
export const $app = getJQEl('app');
export const appNode = document.getElementById('app');

/**
 * Main app config get
 *
 * @param {*} 			obj 	App data object
 * @param {...string} 	keys	One or more strings, referencing properties to traverse through -- eg, obj.prop.subprop = .(obj, 'prop', 'subProp')
 */
export var getAppData = (obj, ...keys) =>
{
	// Support calling getAppData() with no agrs
	if (obj == null)
	{
		return appData;
	}

	// Support calling getAppData() directly, specifying the data object name (rather than the full data object itself)
	if (typeof obj !== 'object' && appData.hasOwnProperty(obj))
	{
		obj = appData[obj];
	}

	return getObjectPropertyRecursive(obj, ...keys);
};

export var getAppCurrent = (...keys) => getAppData(appData.current, ...keys);
export var getAppFetched = (...keys) => getAppData(appData.fetched, ...keys);
export var getAppOptions = (...keys) => getAppData(appData.options, ...keys);
export var getAppScreens = (...keys) => getAppData(appData.options.screens, ...keys);


/**
 * Replace or assign data to an appData obect
 *
 * @TODO: Add deep assign, maybe support an array for 'name'
 *
 * @param   {string}  dataKey  Property of appData (eg 'options')
 * @param   {string}  name     Property of first property (eg 'ui')
 * @param   {object}  data     Data to assign or set to named property
 * @param   {boolean} useAssign  Set this data, or assign it? (set = replace all existing data; assign = add new data to the property)
 * @param   {boolean} createProp Create the naed property, if it doesn't already exist? If false, a warning will be logged
 */
export function setAppData(dataKey, name, data, useAssign, createProp)
{
	useAssign  = setDefault(useAssign, true);
	createProp = setDefault(createProp, false);

	if (appData.hasOwnProperty(dataKey))
	{
		// Check that the named property exists
		if (appData[dataKey].hasOwnProperty(name))
		{
			if (useAssign)
			{
				Object.assign(appData[dataKey][name], data);
			}
			else
			{
				appData[dataKey][name] = data;
			}
		}
		else
		{
			// If the property doesn't exist yet, should we create it?
			if (createProp)
			{
				Object.assign(appData[dataKey]);
				setAppData(dataKey, name, data, useAssign, false);
			}
			else
			{
				console.warn(`[setAppData] Error: Attempted to save to "appData.${dataKey}.${name}", but the specified property "${name}" does not exist`);
			}
		}
	}
	else
	{
		console.warn(`[setAppData] Error: Attempted to save to "appData.${dataKey}", but the specified propery "${dataKey}" does not exist`);
	}
}

export function setAppCurrent(key, data)
{
	setAppData('current', key, data, false);
}

//@TODO: Use setAppData() instead
export function setAppOption(optionGroup, option, data)
{
	if (appData.options.hasOwnProperty(optionGroup))
	{
		if (appData.options[optionGroup].hasOwnProperty(option))
		{
			console.log(optionGroup, option, data);

			// appData.options[optionGroup][option] = data;

			setAppData('options', optionGroup, { [option]: data }, true);
		}
		else
		{
			console.warn(`[setAppOption] Error: Attempted to save options data, but the specified option ${option} does not exist`);
		}
	}
	else
	{
		console.warn(`[setAppOption] Error: Attempted to save options data, but the specified optionGroup ${optionGroup} does not exist`);
	}
}

/**
 * Merge user UI options (saved to localStorage) into current config
 *
 * Used on init
 */
export function mergeLocalConfig()
{
	const userOptions = getLocal('userOptions');
	const devOptions  = getLocal('devOptions');

	if (userOptions != null)
	{
		// Merge local user options into current
		Object.assign(appData.options.ui, userOptions);
	}

	if (devOptions != null)
	{
		Object.assign(appData.options, devOptions);
	}
}

/**
 * Get a random loading message
 *
 * @TODO: Merge loading messages into core data?
 *
 * @return  {string} Loading message
 */
export function getloadingText()
{
	return loadingText[Math.floor(Math.random() * loadingText.length)];
}

// Get selectors (via options.classes)
// Note: This, and getJQEl, allow us to keep all our selectors in one place
// @TODO: Move to elements.js
export function getSelector(className)
{
	let output;

	if (appData.options.classes.hasOwnProperty(className))
	{
		output = appData.options.classes[className];
	}
	else
	{
		console.warn(`[getSelector] Requested class "${className}" does not exist, in appData.options.classes`);
		output = false;
	}

	return output;
}

// Get specified selectors' jQuery elements
// @TODO: Move to elements.js
export function getJQEl(objectName)
{
	return $(getSelector(objectName));
}


// /**
//  * Assign a post to the rest data object
//  *
//  * #unused
//  *
//  * @TODO: Remove this 'rest' data object, and assign all posts data to
//  * the cache object instead (edit: now done?)
//  *
//  * @param {string} type     Data type (posts|terms)
//  * @param {object} newData  New data, to add to the rest data pbject
//  */
// function setAppRest(type, newData)
// {
// 	let dataStore = {};
//
// 	// Create an object, with each post's ID as the key
//
// 	// @TODO: Do we need to use the ID as the key?
// 	// If not, we can save the performance hit from this loop
// 	newData.forEach(d =>
// 	{
// 		let dataObj = { [d.id]: d };
//
// 		Object.assign(dataStore, dataObj);
// 	});
//
// 	// Create {type} property if none exists
// 	if (!appData.rest.hasOwnProperty(type))
// 	{
// 		const emptyType = { [type]: {} };
// 		Object.assign(appData.rest, emptyType);
// 	}
//
// 	Object.assign(appData.rest[type], dataStore);
// }