import { getAppFetched, getAppData, setAppData } from './data-actions.js';

/*---------------------------------------------------------------------------
 * Data: State
 *---------------------------------------------------------------------------
 * Set or check for fetched/generated states, eg, "have posts been fetched",
 * or "have page archives been generated".
 *---------------------------------------------------------------------------*/


/**
 * Check the fetched object, to see if post or term data has been
 * fetched yet
 *
 * @param   {string}  requestType  Request type (posts/terms)
 * @param   {string}  name         Name of post type/term
 *
 * @return  {boolean}              True if the data has been fetched, false otherwise
 */
export function isFetched(requestType, name)
{
	return (getAppFetched(requestType).hasOwnProperty(name));
}

/**
 * Check if an archive etc. has been generated yet
 *
 * @param   {string}  key   Object key (eg, archives)
 * @param   {string}  name  Object name (eg, name of post type)
 *
 * @return  {boolean}       True if generated, false otherwise
 */
export function isGenerated(key, name)
{
	const generated = getAppData('generated');
	let isGenerated = false;

	if (generated.hasOwnProperty(key) && generated[key].hasOwnProperty(name))
	{
		isGenerated = generated[key][name];
	}

	return isGenerated;
}

/**
 * Update the Generated state object
 *
 * Examples:
 *
 *    setAppGenerated('archives', 'articles', true);
 *    setAppGenerated('termBtns', 'categories', true);
 *
 * @param   {string}  key    Eg, 'archives' or 'termBtns'
 * @param   {name}    name   Property name to update, eg, the name of the post type or term
 * @param   {boolean} state  Boolean state
 */
export function setAppGenerated(key, name, state)
{
	setAppData('generated', key, { [name]: state }, true, true);
}