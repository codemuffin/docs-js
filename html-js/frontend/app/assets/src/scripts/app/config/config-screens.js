// Note: Screen key = url

export const configScreens = {


	// Main
	// ------------------------------------------------------------------------

	'home': {
		'name' 		: 'Home',
		'group'		: 'Main',
		'color'		: 'purple',
		'url'		: 'home',
		'metaDesc'  : 'Code, mods and gaming guides',

		'screenType': 'page',
		'screenId'	: 'home',

		'postType'	: false,
		'taxonomy'	: false,

		'dev'		: false,
	},

	'dev-search': {
		'name' 		: 'Search',
		'group'		: 'Main',
		'color'		: 'purple',
		'url'		: 'dev-search',
		'metaDesc'  : 'Find projects and resources',

		'screenType': 'search',
		'screenId'	: 'search',

		'postType'	: false,
		'taxonomy'	: false,

		'dev'		: false,
	},


	// Default (Hidden)
	// ----------------------------------------------------------------------------

	// Top-level key is the page URL
	'posts': {
		'name':           'Posts',         // Label, shown in the sidebar menu & header breadcrumb
		'group':          'Default',       // Menu group
		'color':          'yellow',        // Valid colour, corresponding to background colour map
		'url':            'posts',         // Should match the top-level key
		'metaDesc':       'Default posts', // Meta description for the single page

		'screenType':     'archive',       // page|search|archive
		'screenId':       'archive-post',  // Unique internal ID

		'postType':       'posts',         // In the code, this is used as the REST base (@TODO)
		'taxonomy':       'categories',

		'postTypeActual': 'post',          // The actual post type, in case the set `postType` is not the rest path. In postData, this is the property `type`
		'postTypeRest':   'posts',
		'taxonomyRest':   'category',
		'archive':         '',

		'dev':		       false,          // Only shown when the Developer Tools option is enabled
		'hidden':          true,           // Always hides the screen, even in dev mode
	},

	'pages': {
		'name':           'Pages',
		'group':          'Default',
		'color':          'yellow',
		'url':            'pages',
		'metaDesc':       'Default pages',

		'screenType':     'archive',
		'screenId':       'archive-pages',

		'postType':       'pages',
		'taxonomy':        false,

		'postTypeActual': 'page',
		'postTypeRest':   'pages',
		'taxonomyRest':    false,
		'archive':        'pages', // Not technically correct as pages don't have an archive

		'dev':             false,
		'hidden':          true,
	},


	// Development
	// ----------------------------------------------------------------------------

	'javascript': {
		'name' :          'JavaScript',
		'nameSingle':     'JavaScript',
		'group':          'Code',
		'color':          'blue',
		'url':            'javascript',
		'metaDesc':       'JavaScript projects, including extensions and utilities',

		'screenType':     'archive',
		'screenId':       'archive-javascript',

		'postType':       'javascript',
		'taxonomy':       'js_categories',
		'archive':        'javascript',

		'postTypeActual': 'javascript',
		'postTypeRest':   'javascript',
		'taxonomyRest':   'js_categories',

		'dev':		       false,
	},

	'wp': {
		'name' :          'WordPress',
		'nameSingle':     'WordPress',
		'group':          'Code',
		'color':          'blue',
		'url':            'wp',
		'metaDesc':       'WordPress projects, including plugins and utility scripts',

		'screenType':     'archive',
		'screenId':       'archive-wp',

		'postType':       'wp',
		'taxonomy':       'wp_categories',
		'archive':        'wp',

		'postTypeActual': 'wp',
		'postTypeRest':   'wp',
		'taxonomyRest':   'js_categories',

		'dev':		       false,
	},

	'misc': {
		'name' :          'Misc',
		'nameSingle':     'Misc',
		'group':          'Code',
		'color':          'blue',
		'url':            'misc',
		'metaDesc':       'General code-related things I have made and use often',

		'screenType':     'archive',
		'screenId':       'archive-misc',

		'postType':       'misc',
		'taxonomy':       'misc_categories',
		'archive':        'misc',

		'postTypeActual': 'misc',
		'postTypeRest':   'misc',
		'taxonomyRest':   'misc_categories',

		'dev':		      false,
	},


	// Mods
	// ----------------------------------------------------------------------------

	'mods_deadbolt': {
		'name' 		: 'Deadbolt',
		'nameSingle': 'Deadbolt',
		'group'		: 'Mods',
		'color'		: 'orange',
		'url'		: 'mods_deadbolt',
		'metaDesc'  : 'Mods for the game DEADBOLT by Hopoo, the developer behind Risk of Rain 2',

		'screenType': 'archive',
		'screenId'	: 'archive-mods_deadbolt',

		'postType' 	: 'mods_deadbolt',
		'taxonomy'	: 'mods_deadbolt_categories',
		'archive' 	: 'mods_deadbolt',

		'postTypeActual': 'mods_deadbolt',
		'postTypeRest': 'mods_deadbolt',
		'taxonomyRest': 'mods_deadbolt_categories',

		'dev'		: false,
	},

	'mods_dkas': {
		'name' 		: 'DK:AS',
		'nameSingle': 'DK:AS',
		'group'		: 'Mods',
		'color'		: 'orange',
		'url'		: 'mods_dkas',
		'metaDesc'  : 'Mods for Door Kickers: Action Squad, a side-scrolling co-op platformer & tactical shooter',

		'screenType': 'archive',
		'screenId'	: 'archive-mods_dkas',

		'postType' 	: 'mods_dkas',
		'taxonomy'	: 'mods_dkas_categories',
		'archive' 	: 'mods_dkas',

		'postTypeActual': 'mods_dkas',
		'postTypeRest': 'mods_dkas',
		'taxonomyRest': 'mods_dkas_categories',

		'dev'		: false,
	},

	'mods_bloodstained': {
		'name' 		: 'Bloodstained',
		'nameSingle': 'Bloodstained',
		'group'		: 'Mods',
		'color'		: 'orange',
		'url'		: 'mods_bloodstained',
		'metaDesc'  : 'Mods for the game Bloodstained: Ritual of the Night, a spiritual successor to Castlevania',

		'screenType': 'archive',
		'screenId'	: 'archive-mods_bloodstained',

		'postType' 	: 'mods_bloodstained',
		'taxonomy'	: 'mods_bloodstained_categories',
		'archive' 	: 'mods_bloodstained',

		'postTypeActual': 'mods_bloodstained',
		'postTypeRest': 'mods_bloodstained',
		'taxonomyRest': 'mods_bloodstained_categories',

		'dev'		: false,
	},


	// Gaming
	// ----------------------------------------------------------------------------

	'gameguides': {
		'name' 		: 'Game Guides',
		'nameSingle': 'Game Guide',
		'group'		: 'Gaming',
		'color'		: 'green',
		'url'		: 'gameguides',
		'metaDesc'  : 'Game guides, generally published on Steam',

		'screenType': 'archive',
		'screenId'	: 'archive-gameguides',

		'postType' 	: 'gameguide',
		'taxonomy'	: 'gameguide_categories',
		'archive' 	: 'gameguides',

		'postTypeActual': 'gameguides',
		'postTypeRest': 'gameguides',
		'taxonomyRest': 'gameguide_categories',

		'dev'		: false,
	},

	'spreadsheets': {
		'name' 		: 'Spreadsheets',
		'nameSingle': 'Spreadsheet',
		'group'		: 'Gaming',
		'color'		: 'green',
		'url'		: 'spreadsheets',
		'metaDesc'  : 'Spreadsheets showing tons game data, mainly used for modding',

		'screenType': 'archive',
		'screenId'	: 'archive-spreadsheet',

		'postType' 	: 'spreadsheet',
		'taxonomy'	: 'spreadsheet_categories',
		'archive' 	: 'spreadsheet',

		'postTypeActual': 'spreadsheet',
		'postTypeRest': 'spreadsheet',
		'taxonomyRest': 'spreadsheet_categories',

		'dev'		: false,
	},


	// Developer Tools
	// ----------------------------------------------------------------------------

	'dev-preloader': {
		'name' 		: 'Preloader',
		'group'		: 'Dev Tools',
		'color'		: 'yellow',
		'url'		: 'dev-preloader',
		'metaDesc'  : 'View the current state of fetched and cached data, and reset your local cache',

		'screenType': 'page',
		'screenId'	: 'preloader',

		'postType'	: false,
		'taxonomy'	: false,

		'dev'		: true,
	},

	'dev-data': {
		'name' 		: 'REST Cache',
		'group'		: 'Dev Tools',
		'color'		: 'yellow',
		'url'		: 'dev-data',
		'metaDesc'  : 'Lists REST response data cached to your localStorage',

		'screenType': 'page',
		'screenId'	: 'data',

		'postType'	: false,
		'taxonomy'	: false,

		'dev'		: true,
	},

	'dev-host': {
		'name' 		: 'REST Host',
		'group'		: 'Dev Tools',
		'color'		: 'yellow',
		'url'		: 'dev-host',
		'metaDesc'  : 'Change the data source for this website, using another WordPress site with a public REST API',

		'screenType': 'page',
		'screenId'	: 'host',

		'postType'	: false,
		'taxonomy'	: false,

		'dev'		: true,
	},


	'dev-authenticate': {
		'name' 		: 'REST Auth',
		'group'		: 'Dev Tools',
		'color'		: 'yellow',
		'url'		: 'dev-authenticate',
		'metaDesc'  : 'Test JWT authentication, which allows you gain access to otherwise restricted REST routes',

		'screenType': 'page',
		'screenId'	: 'authenticate',

		'postType'	: false,
		'taxonomy'	: false,

		'dev'		: true,
		'hidden'    : true, // Always hides the screen, even in dev mode
	},


	// Developer Screens
	// ----------------------------------------------------------------------------

	'error404': {
		'name' 		: '404',
		'group'		: 'Dev Screens',
		'color'		: 'yellow',
		'url'		: 'error404',
		'metaDesc'  : 'Page not found',

		'screenType': 'page',
		'screenId'  : 'error404',

		'postType'	: false,
		'taxonomy'	: false,

		'dev'		: true,
		'hidden'    : true,
	},

};
