import { configScreens } from './config-screens.js';

// Options
// UI settings are configurable in the frontend

/**
 * Available hosts. Used in the main config var below.
 *
 * Using the local host is preferred during development, as access to
 * the test site data requires authentication.
 */
const hosts = {
	'origin': window.location.origin,
	'cmData': 'https://data.codemuffin.com',
	'demo1':  'https://techcrunch.com',
	'demo2':  'https://www.sonymusic.com',
};

const host = hosts.cmData;

/**
 * appData
 *
 * The core app settings
 */
export var configOptions = {

	/**
	 * Debug
	 *
	 * These are only used during development. Some options may make it into core.
	 * You can add more options as required.
	 */
	debug: {
		logMissingTerms: false, // Warn about posts without terms (this can get pretty annoying if you haven't set up your terms yet)
		showHiddenNavItems: false,
		logRouting: false,
		// quickLoad: true, // Skips the in-development loading screen #deprecated #unused
	},

	/**
	 * AJAX
	 *
	 * Used by AJAX requests.
	 */
	ajax: {
		host:      host,
		routeBase: '/wp-json/wp/v2/', // Base WP JSON path |@TODO: Move to 'rest'?
		ajaxCache: false,              // Allow AJAX requests to be cached by the browser. Only used with REST API get requests (use $.ajaxSetup for all)
		localStorageSave: true,		  // Save AJAX requests to localStorage, to avoid retreiving them from the server again later
		localStorageLoad: true,		  // Attempt to retrieve cached AJAX requests from localStorage
		useJwt:    false,             // Enable JWT authentication (in development @TODO)
	},

	/**
	 * REST API
	 *
	 * Currently only handles default post query args
	 */
	query: {
		per_page: 100,     // default is 10, limit is 100 | @TODO: Add loop to retreive posts over 100
		orderby:  'title',
		order:    'asc',   // asc (ABC, 321) || desc (CBA, 123)
		embed:    true,   // enables FEATURED IMAGES etc (the actual param is `_embed`) - Warning: can bloat locaStorage on large sites, switch to indexedDB if you need this
	},

	/**
	 * Preloader
	 */
	preload: {
		enabled:   true,  // Enable the preloader screen. If false, the screen becomes completely unusable, and the search won't work
		autoInit:  true, // Automatically preload all posts and terms data. Set to false to allow manual preload init
		autoRetry: false, // @TODO: Retry limiting hasn't been added yet, so this could trigger an infinite loop. Use with caution!
	},

	/**
	 * Archives
	 */
	archives: {
		autoFetch: true,   // When the user visits an archive screen, should its posts be automatically fetched?
		autoGenerate: true, // Should archives be automatically generated, once their fetched data is ready?
	},

	/**
	 * Default UI
	 *
	 * These will be triggered on every page load.
	 * verruled by user options set in the frontend.
	 */
	ui: {
		// Global
		'show-header'      : true,
		'show-terms-count' : true,

		// Posts List
		'show-terms-list'    : true,
		'show-excerpts-list' : true,
		'post-list-grid'     : true,
		'post-list-featuredimgages' : true, // requires query.embed=true, else only generated colour squares will show

		// Post Sidebar
		'show-terms-view'    : true,
		'show-excerpts-view' : false,
		'post-view-sidebar-right' : false,
		'post-view-sidebar-show'  : true,

		// Secrets
		'developers-only' : false,
	},


	/**
	 * Title tag
	 *
	 * @example getAppOptions( 'metaContent', 'defaultTitle' )
	 */
	metaContent: {
		defaultTitle: 'Code Muffin',
		defaultDescription: 'Code, mods and gaming guides',
		// titleSep: '▸',
		// titleSubSep: '─',
		titleSep: '-',
		titleSubSep: '─',
	},

	/**
	 * Selectors
	 *
	 * Using a single store of selectors makes finding and updating easy :)
	 */
	classes: {
		app:         '#app', // events listener target
		appOptions:  '#app-options', // options classes are added to this element |@TODO: Change to '#app-options'
		templates:   '#app-templates',
		logboxCls:   '.logbox-list',
		logboxLiCls: 'logbox-message-bottom',

		// Header
		header:             '.header', // used for dynamic header BG colours
		headerScreenName:   '.header-title-screen-name', // shows the active screen name
		headerPostTitle:    '.header-title-post-title',
		headerPostTitleSep: '.header-title-sep-post',
	},

	// Screens: For screen loading/switching & nav generation
	screens: configScreens,

	// Settings for third-party libs
	libs: {
		// All themes: https://enlighterjs.org/Theme.Enlighter.html
		// Choices to be added to app: 'atomic', 'beyond', 'enlighter', 'git'
		enlightenerTheme: 'beyond',
	}
};