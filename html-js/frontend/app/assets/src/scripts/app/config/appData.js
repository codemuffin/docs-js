import { configOptions } from './config-options.js';

/**
 * Defaults for config data
 */
export const defaultAppData =
{
	// Initial options
	options: configOptions,

	// Current screen data (posts, term buttons etc)
	current: {
		screen: {}, // Current screen data
		post: false // Current post data (false if no post is open)
	},

	// Retreived data (posts & terms)
	fetched: {
		// Posts and terms
		// Request response originally stores these as an array, but conversion
		// after the fetch transforms this data into an object, with post/term
		// IDs as keys
		posts: {},
		terms: {},

		// Huge array containing every fetched post, with post IDs as keys.
		// Used and first created by the search screen, the data is cached to
		// this object to prevent looping through the posts object more than once
		allPosts: {},

		// Used by the preloader
		//@TODO: Refactor fetch promises
		promises: {
			posts: {},
			terms: {},
		},

		// Lists ajax requests cached to localStorage: An object of objects, indexed by request URL.
		// The data stored in this object is identical to the `restCacheStored` object in localStorage.
		// This localStorage object is read on app init, and copied to this data object.
		restCacheData: {},
	},

	//@TODO: Deprecated but may still be useful?
	// Sitemap, listing all screens (pages/archives), and any children (posts)
	// Can be checked against to see if posts/terms have been fetched loaded yet
	sitemap: {
		posts: {},
		terms: {},
	},

	/**
	 * Generated (state booleans)
	 *
	 * States if archives etc have been generated
	 */
	generated: {
		archives: {},
		termBtns: {},
	},

	//@TODO
	// Keeps track of request states, to prevent the identical requests being fetched as once.
	requestStates: {
		/**
		 * Requests that have been made, but not yet resolved or rejected.
		 *
		 * ${requestUrl}: {
		 * 		attempts: [], // An array of request date strings (unicode format). Retry attempts add a new string. Used to prevent infinite retries
		 * 		requestDate: '2019-01-29T13:03:39.285Z',     // Date of this request, in unicode time string format
		 * 		lastRequestDate: '2019-01-29T13:03:30.000Z', // Date of the last request, if this is not the first attempt
		 * 		requestType: 'posts' // Request type (often referred to by the var requestType). Options: 'posts' or 'terms'
		 * 		screenObj: ${screenObj}, // The full screen object. This holds lots useful data, including the screenId, and the name of the post type/taxonomy term
		 * }
		 */
		pending: {
			posts: {},
			terms: {},
		},
		complete: {
			posts: {},
			terms: {},
		},
		failed: {
			posts: {},
			terms: {},
		}
	},
};