import { constructRestRoute } from './rest-utilities.js';
import { getAppOptions, getAppFetched } from '../data/data-actions.js';
import { getLocal } from '../storage/localStorage.js';
import { loadingNotice } from '../ui/loading.js';
import { log } from '../utils/logger.js';
import { setAppFetchedRestRequest } from '../data/data-rest.js';
import { resolveAppPromise } from '../data/data-promises.js';
import { saveRestToLocal } from '../data/data-cache.js';
import { getCookie } from '../utils/cookies.js';

// @TODO: Use fetch API

/**
 * Get REST data via AJAX
 *
 * taxonomies: The response doesn't include terms, just the taxonomies themselves
 * taxonomy:   Contains data on the attached post types. Does not get the terms of the taxonomy
 *
 * @param   {string}  screenId     Request screen ID. Used to save retreived data to a cache (also very useful for debugging)
 * @param   {string}  requestType  Sets the route. Options: posts, terms, types, type, taxonomies, taxonomy, terms
 * @param   {string}  name         The specifically requested item name, if applicable. Only used by some routes (eg, by 'posts' to get posts of a certain post type)
 *
 * @return  {object}		       A promise containing the AJAX response. Resolved on success, rejected on failure
 */
export function fetchRest(screenId, requestType, name)
{
	const requiresName = ['posts', 'terms', 'type', 'taxonomy', 'terms'];
	const saveLocally = ['posts', 'terms'];

	if (requiresName.includes(requestType) && !name)
	{
		console.warn(`[error_fetchRest] Requested data for ${requestType}, but the required arg 'name' was not supplied`);
		return; // @TODO: Should we return a rejected promise instead?
	}

	let route;
	let response;

	switch (requestType)
	{
		// Posts & post types
		case 'posts':
			log(`Retreiving ${requestType}, for the name "${name}"`, requestType);
			route = constructRestRoute(requestType, name);
			break;

		case 'types':
			log('Retreiving all post types', requestType);
			route = constructRestRoute(requestType);
			break;

		case 'type':
			log(`Retreiving single post type, "${name}"`, requestType);
			route = constructRestRoute(requestType, name);
			break;

		// Terms & taxonomies
		case 'terms':
			log(`Retreiving terms from taxonomy "${name}"`, requestType);
			route = constructRestRoute(requestType, name);
			break;

		case 'taxonomies':
			log('Retreiving all taxonomies', requestType);
			route = constructRestRoute(requestType);
			break;

		case 'taxonomy':
			log(`Retreiving the single taxonomy "${name}"`, requestType);
			route = constructRestRoute(requestType, name);
			break;
	}

	// Log the REST route URL
	log(`<a target="_blank" href="${route}">${route}</a>`, 'GET > REQUESTED');

	// Check for locally cached data
	let hasCachedRequest = false;

	if (name)
	{
		// Localstorage support: Loads the request response from the local
		// cache, if it's been cached, and local storage is enabled
		if (getAppOptions('ajax', 'localStorageLoad'))
		{
			log(`Retreived: ${route}`, 'GET > LOCAL', 'warning');

			let fetchedCache = getAppFetched(requestType, name);

			if (fetchedCache)
			{
				// Resolve, and return the cached request data
				response = new Promise(resolve => resolve(fetchedCache));
				hasCachedRequest = true;
			}
			else
			{
				// Fallback to localStorage, in case something went wrong when saving
				// the data from localStorage to the fetched object
				const localStorageCache = getLocal(route, false);

				if (localStorageCache)
				{
					/*
					// This should never happen, so add a warning
					const warning = [
						'[warning_fetchRest]',
						'While retreiving local data, a potential bug occured: The data was present in localStorage, but not in the fetched object.',
						'This means that REST data has previously been retrieved and saved to localStorage, but was not assigned to the fetched data object on app init.'
					];
					console.warn(warning.join('\n'));
					*/

					// Return a resolved promise, containing the requested data
					response = new Promise(resolve => resolve(localStorageCache));
					hasCachedRequest = true;
				}
			}
		}
	}

	// If no local data is cached, retreive data via AJAX
	if (!hasCachedRequest)
	{
		response = fetchRestAjax(route, screenId);

		response
			.then(data =>
			{
				if (name)
				{
					// Update the fetched posts/terms data, and resolve this items 'fetched' promise
					// Only applies to named item requests (posts of a certain type, named taxonomies, etc)
					setAppFetchedRestRequest(requestType, name, data);

					// Resolve promises, so we can check, elsewhere, that this request has been fulfilled
					resolveAppPromise(requestType, name);
				}

				if (saveLocally.includes(requestType))
				{
					saveRestToLocal(route, data, screenId, requestType, name);
				}
			},
				() => // (error)
				{
					// @TODO: Move the call to handleAjaxGetErrors here
				});
	}

	return response;
}

/**
 * Get a REST API route response
 *
 * @param  {string}  route     Full REST route. This should be constructed with constructRestRoute
 *
 * @return {object}            Promise object, containing either REST response data (on resolved) or error data (on rejected).
 */
function fetchRestAjax(route, screenId)
{
	const fetchRestPromise = new Promise((resolve, reject) =>
	{
		// Defaults
		if (!route)
		{
			console.warn('[error_fetchRestAjax] No route specified');
			reject();
		}

		// Get options
		const ajaxOptions = getAppOptions('ajax');
		const ajaxCache = ajaxOptions.ajaxCache;
		const ajaxHeaders = {};

		// JWT Authentication
		if (ajaxOptions.useJwt)
		{
			const jwtKey = getCookie('jwt-auth-token');
			console.log({jwtKey});
			const jwtHeaders = { headers: { 'Authorization': `Bearer ${jwtKey}` } };
			Object.assign(ajaxHeaders, jwtHeaders);
		}

		let ajaxParams = {
			url: route,
			type: 'get',
			context: this,
			content: 'application/json',
			dataType: 'json',
			cache: ajaxCache,
			ajaxHeaders
		};

		// Main request
		//@TODO: Replace with Fetch API, jquery is ugly!
		const ajaxResponse = $.ajax(ajaxParams);

		ajaxResponse.always(data =>
		{
			handleAjaxGetErrors(data, route, screenId)
				.then(
					data => resolve(data),
					rejectReason => reject(rejectReason)
				);
		});

	});

	return fetchRestPromise;
}

/**
 *  Handles Get method error handling, and logs progress to the Logger
 *
 *  On success & non-empty data, returns the data; else returns false.
 *
 *  @param {object}		response 	Response from an AJAX method ($.get or $.ajax)
 *  @param {string}		restUrl  		Original request URL. Used only for logging
 */
function handleAjaxGetErrors(response, restUrl, screenId)
{
	// Defaults
	// customEvent = setDefault(customEvent, null);

	const errorResponsePromise = new Promise((resolve, reject) =>
	{
		let hasError = false;

		// Main (response) error handling @TODO: Replace jQuery with fetch API
		$.when(response)

			// Success
			.done(data =>
			{
				if (data.length === 0)
				{
					// Data was returned, but empty
					log('Response data was empty. This means that no posts/terms have been set for the requested data type', 'Get : WARNING', 'danger');
					loadingNotice('posts', 'warningEmptyData', screenId);
					logAjaxErrors(data, restUrl, false);
					hasError = false;
				}
				else
				{
					/**
					 * Check for errors in data that responded with status 200
					 *
					 * Ordinarily, this wouldn't be needed, but I edited the 'disable
					 * REST' plugin to return error 200 on authentication error. Obvs
					 * this is bad! But it's required to get past the CORS preflight
					 * request, which doesn't allow authentication if http status 200
					 * is not received.
					 *
					 * @TODO: Investigate and find a better solution
					 */
					if (data.code !== undefined && data.code === 'rest_cannot_access')
					{
						log('Data could not be retrieved, as you are not currently authorised to retreive it', 'Get : AUTHENTICATION FAILURE', 'danger');
						loadingNotice('posts', 'errorUnauthorised', screenId);
						logAjaxErrors(data, restUrl, false);
						hasError = true;
					}
					else
					{
						// SUCCESS!
						log(`${restUrl}`, 'Get > SUCCESS', 'success');
					}
				}
			})

			// Failure
			.fail(data =>
			{
				logAjaxErrors(data, restUrl);
				loadingNotice('posts', 'ajaxError', screenId);
				hasError = true;
			})

			.always(data =>
			{
				if (hasError)
				{
					reject(data);
				}
				else
				{
					resolve(data);
				}
			});
	});

	return errorResponsePromise;
}

function logAjaxErrors(response, restUrl, consoleOnly)
{
	if (!consoleOnly)
	{
		if (response.responseJSON)
		{
			const code = response.responseJSON.code;
			const status = response.responseJSON.data.status;
			const message = response.responseJSON.message;

			log(`Error retrieving data (${code}): <br>${message}`, `Get : ERROR ${status}`, 'danger');
		}
		else
		{
			log(`Error retrieving data: ${response.status} <br>${restUrl}`, `Get > ERROR ${response.statusText}`, 'danger');
		}
	}

	// console.warn(`[error_handleAjaxGetErrors]`, setDefault(response.responseJSON, response));
}