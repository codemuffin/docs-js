import { loadingNotice, toggleLoaderNotice } from "../ui/loading.js";
import { fetchRest } from './rest-api.js';
import { afterArchiveFetch } from '../screens/screen-archive.js';
import { preloadData, preloaderFetchResolved, preloaderFetchRejected } from "../screens/screen-preloader.js";
import { getAppFetched } from '../data/data-actions.js';
import { constructRestRoute } from './rest-utilities.js';

/*---------------------------------------------------------------------------
 * REST: Fetch
 *---------------------------------------------------------------------------
 * Retrives posts and terms, via rest-api.
 *
 * Used by these screens: archives, search, preloader.
 *---------------------------------------------------------------------------*/

/**
 * Archive: Fetch posts
 */
export function archive_fetchPosts(screenId, postType, taxonomy)
{
	if (postType)
	{
		fetchRest(screenId, 'posts', postType)
			.then(
				() => afterArchiveFetch('posts', screenId, taxonomy, postType), // (data) =>
				error =>  console.warn('[error_fetchPosts_A] The requested data could not be retreived', error)
			).catch( e => console.warn('[error_fetchPosts_B] An error occured. This is likely a bug in the code', e));
	}
	else
	{
		loadingNotice('posts', 'postListNoPostType', screenId);
	}
}

/**
 * Archive: Fetch terms
 */
export function archive_fetchTerms(screenId, taxonomy)
{
	if (taxonomy)
	{
		fetchRest(screenId, 'terms', taxonomy)
			.then(
				() => afterArchiveFetch('terms', screenId, taxonomy, false), // (data) =>
				error =>  console.warn('[error_fetchTerms_A] The requested data could not be retreived', error)
			).catch( e => console.warn('[error_fetchTerms_B] An error occured. This is likely a bug in the code', e));
	}
	else
	{
		toggleLoaderNotice('terms', false, screenId);
	}
}

/**
 * REST API Get
 *
 * @param   {object}  postTypes   Post types, to request
 * @param   {object}  terms       Taxonomy terms, to request
 */
export function preloader_preloadGet(autoInit)
{
	const postsArr = preloadData.postsArr;
	const termsArr = preloadData.termsArr;

	// Get posts
	for (let p in postsArr)
	{
		const { screenId, postType, requestType } = postsArr[p];

		// If autoInit is disabled, only display locally cached data
		if (!autoInit)
		{
			const localPosts = getAppFetched(requestType, postType);

			if (localPosts)
			{
				preloaderFetchResolved(requestType, localPosts, postType);
			}

			continue;
		}

		fetchRest(screenId, requestType, postType, autoInit)
			.then(
				data  => preloaderFetchResolved(requestType, data, postType),
				error => preloaderFetchRejected(requestType, error, constructRestRoute(requestType, postType), error)
			).catch(e =>
			{
				console.error('[error_preloadGet:posts] Code Error:', e);
			});
	}

	// Get terms
	for (let t in termsArr)
	{
		const { screenId, taxonomy, requestType } = termsArr[t];

		// If autoInit is disabled, only display locally cached data
		if (!autoInit)
		{
			const localTerms = getAppFetched(requestType, taxonomy);

			if (localTerms)
			{
				preloaderFetchResolved(requestType, localTerms, taxonomy);
			}

			continue;
		}

		fetchRest(screenId, requestType, taxonomy, autoInit)
			.then(
				data  => preloaderFetchResolved(requestType, data, taxonomy),
				error => preloaderFetchRejected(requestType, error, constructRestRoute(requestType, taxonomy), error)
			).catch(e =>
			{
				console.error('[error_preloadGet:terms] Code Error:', e);
			});
	}
}