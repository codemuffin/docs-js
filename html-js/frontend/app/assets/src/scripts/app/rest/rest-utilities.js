import { getAppOptions } from '../data/data-actions.js';
import { autoExcerpt } from './../utils/content.js';
import { objectHasProp } from '../utils/object.js';

/**
 * Constructs an route, with different routes depending on the type
 *
 * @param  {string} type 	Type of route required. Options: posts, types, type, taxonomies, taxonomy, terms
 * @param  {string} data 	Post type or taxonomy. Not required for 'types' or 'taxonomies'
 *
 * @return {string}      	REST API route
 */
export function constructRestRoute(type, data)
{
	let route = '';

	// Possible data types (data var aliases, used for clarity)
	let postType = data;
	let taxonomy = data;
	// let term     = data;

	// Get options data
	let { per_page, orderby, order, embed } = getAppOptions('query');

	switch (type)
	{
		case 'posts':
			route = `${postType}?per_page=${per_page}&orderby=${orderby}&order=${order}`;
			break;

		case 'types':
			route = 'types';
			break;

		case 'type':
			route = `types/${postType}`;
			break;

		case 'taxonomies':
			route = 'taxonomies';
			break;

		case 'taxonomy':
			route = `taxonomies/${taxonomy}`;
			break;

		case 'terms':
			route = `${taxonomy}?per_page=${per_page}`;
			break;
	}

	// Add _embed? @TODO: Expand with more options!
	if (embed)
	{
		if (route.includes('?'))
		{
			route += '&_embed';
		}
		else
		{
			route += '?_embed';
		}
	}

	// Get route path from options
	const ajaxOptions = getAppOptions('ajax');
	const ajaxHost = ajaxOptions.host;
	const ajaxBase = ajaxOptions.routeBase;
	const ajaxUrl = ajaxHost + ajaxBase + route;

	return ajaxUrl;
}

/**
 * Returns the featured image from a single post response object
 *
 * Requires use of the _embed param:
 * @link https://developer.wordpress.org/rest-core/using-the-rest-api/linking-and-embedding
 *
 * @param   {object}  postData       Single post data objects
 *
 * @return  {object}                 An object containing the image size URLs, or false if no image is available
 */
export function getFeaturedImageFromPostData(postData)
{
	if (!postData)
	{
		console.log(`[error_getFeaturedImageFromPostData] postData is required, but was not passed`);
		return false;
	}

	let output = false;

	if (postData.hasOwnProperty('_embedded'))
	{
		if (postData._embedded.hasOwnProperty('wp:featuredmedia'))
		{
			const mediaObj = postData._embedded['wp:featuredmedia'];
			const imgObj = mediaObj[0];

			if (imgObj.hasOwnProperty('source_url'))
			{
				// Media sizes. Reverts to the next size down if a larger requested size isn't available
				const sizes	= imgObj.media_details.sizes;

				const full   = imgObj.source_url;
				const small  = (sizes.thumbnail) ? sizes.thumbnail.source_url : full;
				const medium = (sizes.medium)    ? sizes.medium.source_url    : small;
				const large  = (sizes.large)     ? sizes.large.source_url     : medium;

				output = { full, small, medium, large };
			}
		}
	}

	return output;
}

/**
 * Generate a string of term classes
 *
 * Example output: "snippet_categories-57 snippet_categories-49"
 *
 * @param   {object}  termsArr  Terms array, from a single postData object. Contains term IDs
 * @param   {string}  taxonomy  The taxonomy
 *
 * @return  {string}
 */
export function generateTermClasses(termsArr, taxonomy)
{
	let classesArr = [];

	termsArr.forEach(term =>
	{
		const termClass = `${taxonomy}-${term}`;
		classesArr.push(termClass);
	});

	return classesArr.join(' ');
}

/**
 * Get the post excerpt
 *
 * @param   {object}  postData  Full post data object
 *
 * @return  {string}            Excerpt
 */
export function extractExcerpt( postObj )
{
	let excerpt = false;

	if ( objectHasProp( postObj, 'excerpt' ) && objectHasProp( postObj.excerpt, 'rendered' ) )
	{
		excerpt = postObj.excerpt.rendered;

		excerpt = filterMembersExcerpt( excerpt ); // Support for 'PMPro Members' plugin (restricted post)
		excerpt = autoExcerpt( excerpt );
	}
	else
	{
		if ( objectHasProp( postObj, 'content' ) && objectHasProp( postObj.content, 'rendered' ) )
		{
			excerpt = autoExcerpt( postObj.content.rendered );
		}
		else
		{
			excerpt = 'This post currently has no content';
		}
	}

	return excerpt;
}

/**
 * Protection against PMPro Members plugin, which breaks everything
 *
 * @return  {string}
 */
export function filterMembersExcerpt(excerptRendered)
{
	const restrictedText = "Restricted Content (Members Only)";

	return (excerptRendered.includes('pmpro_content_message')) ? restrictedText : excerptRendered;
}
