<?php

/**
 * Stop WordPress fixing URLs it sees as invalid
 *
 * @see https://developer.wordpress.org/reference/functions/redirect_canonical/
 *
 * @param   string  $redirect_url  The URL that was requested, used to figure if redirect is needed
 *
 * @return  string                 New URL to redirect to (or the original $redirect_url if not applicable)
 */
function muffin_prevent_404_redirect_fix( $redirect_url )
{
	if ( is_404() )
	{
		return false;
	}

	return $redirect_url;
}

add_filter( 'redirect_canonical', 'muffin_prevent_404_redirect_fix' );
