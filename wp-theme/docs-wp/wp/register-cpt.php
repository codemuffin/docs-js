<?php

function muffin_register_cpt()
{

	// JavaScript
	// ------------------------------------------------------------------------

	$labels = [
		'name'                  => 'JavaScript',
		'singular_name'         => 'JS',
	];

	$args = [
		'label'                 => 'JavaScript',
		'labels'                => $labels,
		'description'           => '',
		'public'                => true,
		'publicly_queryable'    => true,
		'show_ui'               => true,
		'show_in_rest'          => true,
		'rest_base'             => 'javascript',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
		'has_archive'           => 'javascript',
		'show_in_menu'          => true,
		'show_in_nav_menus'     => true,
		'delete_with_user'      => false,
		'exclude_from_search'   => false,
		'capability_type'       => 'post',
		'map_meta_cap'          => true,
		'hierarchical'          => false,
		'rewrite'               => [ 'slug' => 'javascript', 'with_front' => false ],
		'query_var'             => true,
		'supports'              => [ 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'author' ],
	];

	register_post_type( 'javascript', $args );


	// WP
	// ------------------------------------------------------------------------

	$labels = [
		'name'                  => 'WordPress',
		'singular_name'         => 'WordPress',
	];

	$args = [
		'label'                 => 'WP',
		'labels'                => $labels,
		'description'           => '',
		'public'                => true,
		'publicly_queryable'    => true,
		'show_ui'               => true,
		'show_in_rest'          => true,
		'rest_base'             => 'wp',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
		'has_archive'           => 'wp',
		'show_in_menu'          => true,
		'show_in_nav_menus'     => true,
		'delete_with_user'      => false,
		'exclude_from_search'   => false,
		'capability_type'       => 'post',
		'map_meta_cap'          => true,
		'hierarchical'          => false,
		'rewrite'               => [ 'slug' => 'wp', 'with_front' => false ],
		'query_var'             => true,
		'supports'              => [ 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'author' ],
	];

	register_post_type( 'wp', $args );


	// Misc
	// ------------------------------------------------------------------------

	$labels = [
		'name'                  => 'Misc',
		'singular_name'         => 'Misc',
	];

	$args = [
		'label'                 => 'Misc',
		'labels'                => $labels,
		'description'           => '',
		'public'                => true,
		'publicly_queryable'    => true,
		'show_ui'               => true,
		'show_in_rest'          => true,
		'rest_base'             => 'misc',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
		'has_archive'           => 'misc',
		'show_in_menu'          => true,
		'show_in_nav_menus'     => true,
		'delete_with_user'      => false,
		'exclude_from_search'   => false,
		'capability_type'       => 'post',
		'map_meta_cap'          => true,
		'hierarchical'          => false,
		'rewrite'               => [ 'slug' => 'misc', 'with_front' => false ],
		'query_var'             => true,
		'supports'              => [ 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'author' ],
	];

	register_post_type( 'misc', $args );


	// MODS
	// ========================================================================

	// Bloodstained
	// ------------------------------------------------------------------------

	$labels = [
		'name'                  => 'Bloodstained Mods',
		'singular_name'         => 'Bloodstained Mod',
	];

	$args = [
		'label'                 => 'Bloodstained Mods',
		'labels'                => $labels,
		'description'           => '',
		'public'                => true,
		'publicly_queryable'    => true,
		'show_ui'               => true,
		'show_in_rest'          => true,
		'rest_base'             => 'mods_bloodstained',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
		'has_archive'           => 'mods_bloodstained',
		'show_in_menu'          => true,
		'show_in_nav_menus'     => true,
		'delete_with_user'      => false,
		'exclude_from_search'   => false,
		'capability_type'       => 'post',
		'map_meta_cap'          => true,
		'hierarchical'          => false,
		'rewrite'               => [ 'slug' => 'mods_bloodstained', 'with_front' => false ],
		'query_var'             => true,
		'supports'              => [ 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'author' ],
	];

	register_post_type( 'mods_bloodstained', $args );


	// DKAS
	// ------------------------------------------------------------------------

	$labels = [
		'name'                  => 'DKAS Mods',
		'singular_name'         => 'DKAS Mod',
	];

	$args = [
		'label'                 => 'DKAS Mods',
		'labels'                => $labels,
		'description'           => '',
		'public'                => true,
		'publicly_queryable'    => true,
		'show_ui'               => true,
		'show_in_rest'          => true,
		'rest_base'             => 'mods_dkas',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
		'has_archive'           => 'mods_dkas',
		'show_in_menu'          => true,
		'show_in_nav_menus'     => true,
		'delete_with_user'      => false,
		'exclude_from_search'   => false,
		'capability_type'       => 'post',
		'map_meta_cap'          => true,
		'hierarchical'          => false,
		'rewrite'               => [ 'slug' => 'mods_dkas', 'with_front' => false ],
		'query_var'             => true,
		'supports'              => [ 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'author' ],
	];

	register_post_type( 'mods_dkas', $args );


	// Deadbolt
	// ------------------------------------------------------------------------

	$labels = [
		'name'                  => 'Deadbolt Mods',
		'singular_name'         => 'Deadbolt Mod',
	];

	$args = [
		'label'                 => 'Deadbolt Mods',
		'labels'                => $labels,
		'description'           => '',
		'public'                => true,
		'publicly_queryable'    => true,
		'show_ui'               => true,
		'show_in_rest'          => true,
		'rest_base'             => 'mods_deadbolt',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
		'has_archive'           => 'mods_deadbolt',
		'show_in_menu'          => true,
		'show_in_nav_menus'     => true,
		'delete_with_user'      => false,
		'exclude_from_search'   => false,
		'capability_type'       => 'post',
		'map_meta_cap'          => true,
		'hierarchical'          => false,
		'rewrite'               => [ 'slug' => 'mods_deadbolt', 'with_front' => false ],
		'query_var'             => true,
		'supports'              => [ 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'author' ],
	];

	register_post_type( 'mods_deadbolt', $args );


	// GAMING
	// ============================================================================


	// Game Guides (Gaming)
	// ------------------------------------------------------------------------

	$labels = [
		"name"                  => "Game Guides",
		"singular_name"         => "Game Guide",
	];

	$args = [
		"label"                 => "Game Guides",
		"labels"                => $labels,
		"description"           => "",
		"public"                => true,
		"publicly_queryable"    => true,
		"show_ui"               => true,
		"show_in_rest"          => true,
		"rest_base"             => "gameguide",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive"           => "gameguides",
		"show_in_menu"          => true,
		"show_in_nav_menus"     => true,
		"delete_with_user"      => false,
		"exclude_from_search"   => false,
		"capability_type"       => "post",
		"map_meta_cap"          => true,
		"hierarchical"          => false,
		"rewrite"               => [ "slug" => "gameguide", "with_front" => true ],
		"query_var"             => true,
		"supports"              => [ "title", "editor", "thumbnail", "excerpt", "revisions", "author" ],
	];

	register_post_type( "gameguide", $args );


	// Spreadsheets (Gaming)
	// ------------------------------------------------------------------------

	$labels = [
		'name'                  => 'Spreadsheets',
		'singular_name'         => 'Spreadsheet',
	];

	$args = [
		'label'                 => 'Spreadsheets',
		'labels'                => $labels,
		'description'           => '',
		'public'                => true,
		'publicly_queryable'    => true,
		'show_ui'               => true,
		'show_in_rest'          => true,
		'rest_base'             => 'spreadsheet',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
		'has_archive'           => 'spreadsheets',
		'show_in_menu'          => true,
		'show_in_nav_menus'     => true,
		'delete_with_user'      => false,
		'exclude_from_search'   => false,
		'capability_type'       => 'post',
		'map_meta_cap'          => true,
		'hierarchical'          => false,
		'rewrite'               => [ 'slug' => 'spreadsheet', 'with_front' => false ],
		'query_var'             => true,
		'supports'              => [ 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'author' ],
	];

	register_post_type( 'spreadsheet', $args );
}

add_action( 'init', 'muffin_register_cpt' );
