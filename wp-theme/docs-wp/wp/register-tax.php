<?php

function muffin_register_tax()
{

	// JavaScript Cats
	// ------------------------------------------------------------------------

	$labels = [
		'name'                  => 'JS Cats',
		'singular_name'         => 'JS Cat',
	];

	$args = [
		'label'                 => 'JS Cats',
		'labels'                => $labels,
		'public'                => true,
		'publicly_queryable'    => true,
		'hierarchical'          => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'show_in_nav_menus'     => true,
		'query_var'             => true,
		'rewrite'               => [ 'slug' => 'js_categories', 'with_front' => false, ],
		'show_admin_column'     => true,
		'show_in_rest'          => true,
		'rest_base'             => 'js_categories',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
		'show_in_quick_edit'    => true,
	];

	register_taxonomy( 'js_categories', [ 'javascript' ], $args );


	// WP Cats
	// ------------------------------------------------------------------------

	$labels = [
		'name'                  => 'WP Cats',
		'singular_name'         => 'WP Cat',
	];

	$args = [
		'label'                 => 'WP Cats',
		'labels'                => $labels,
		'public'                => true,
		'publicly_queryable'    => true,
		'hierarchical'          => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'show_in_nav_menus'     => true,
		'query_var'             => true,
		'rewrite'               => [ 'slug' => 'wp_categories', 'with_front' => false, ],
		'show_admin_column'     => true,
		'show_in_rest'          => true,
		'rest_base'             => 'wp_categories',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
		'show_in_quick_edit'    => true,
	];

	register_taxonomy( 'wp_categories', [ 'wp' ], $args );


	// Mod Cats: Bloodstained
	// ------------------------------------------------------------------------

	$labels = [
		'name'                  => 'BS Mods Cats',
		'singular_name'         => 'BS Mods Cat',
	];

	$args = [
		'label'                 => 'BS Mods Cats',
		'labels'                => $labels,
		'public'                => true,
		'publicly_queryable'    => true,
		'hierarchical'          => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'show_in_nav_menus'     => true,
		'query_var'             => true,
		'rewrite'               => [ 'slug' => 'mods_bloodstained_categories', 'with_front' => false, ],
		'show_admin_column'     => true,
		'show_in_rest'          => true,
		'rest_base'             => 'mods_bloodstained_categories',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
		'show_in_quick_edit'    => true,
	];

	register_taxonomy( 'mods_bloodstained_categories', [ 'mods_bloodstained' ], $args );


	// Mod Cats: DKAS
	// ------------------------------------------------------------------------

	$labels = [
		'name'                  => 'DKAS Mods Cats',
		'singular_name'         => 'DKAS Mods Cat',
	];

	$args = [
		'label'                 => 'DKAS Mods Cats',
		'labels'                => $labels,
		'public'                => true,
		'publicly_queryable'    => true,
		'hierarchical'          => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'show_in_nav_menus'     => true,
		'query_var'             => true,
		'rewrite'               => [ 'slug' => 'mods_dkas_categories', 'with_front' => false, ],
		'show_admin_column'     => true,
		'show_in_rest'          => true,
		'rest_base'             => 'mods_dkas_categories',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
		'show_in_quick_edit'    => true,
	];

	register_taxonomy( 'mods_dkas_categories', [ 'mods_dkas' ], $args );


	// Mod Cats: Deadbolt
	// ------------------------------------------------------------------------

	$labels = [
		'name'                  => 'DB Mods Cats',
		'singular_name'         => 'DB Mods Cat',
	];

	$args = [
		'label'                 => 'DB Mods Cats',
		'labels'                => $labels,
		'public'                => true,
		'publicly_queryable'    => true,
		'hierarchical'          => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'show_in_nav_menus'     => true,
		'query_var'             => true,
		'rewrite'               => [ 'slug' => 'mods_deadbolt_categories', 'with_front' => false, ],
		'show_admin_column'     => true,
		'show_in_rest'          => true,
		'rest_base'             => 'mods_deadbolt_categories',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
		'show_in_quick_edit'    => true,
	];

	register_taxonomy( 'mods_deadbolt_categories', [ 'mods_deadbolt' ], $args );


	// Misc Cats
	// ------------------------------------------------------------------------

	$labels = [
		'name'                  => 'Misc Cats',
		'singular_name'         => 'Misc Cat',
	];

	$args = [
		'label'                 => 'Misc Cats',
		'labels'                => $labels,
		'public'                => true,
		'publicly_queryable'    => true,
		'hierarchical'          => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'show_in_nav_menus'     => true,
		'query_var'             => true,
		'rewrite'               => [ 'slug' => 'misc_categories', 'with_front' => false, ],
		'show_admin_column'     => true,
		'show_in_rest'          => true,
		'rest_base'             => 'misc_categories',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
		'show_in_quick_edit'    => true,
	];

	register_taxonomy( 'misc_categories', [ 'misc' ], $args );


	// Steam Guide Cats
	// ------------------------------------------------------------------------

	$labels = [
		"name"                  => "Game Guide Cats",
		"singular_name"         => "Game Guide Cat",
	];

	$args = [
		"label"                 => "Game Guide Cats",
		"labels"                => $labels,
		"public"                => true,
		"publicly_queryable"    => true,
		"hierarchical"          => true,
		"show_ui"               => true,
		"show_in_menu"          => true,
		"show_in_nav_menus"     => true,
		"query_var"             => true,
		"rewrite"               => [ 'slug' => 'gameguide_categories', 'with_front' => false, ],
		"show_admin_column"     => false,
		"show_in_rest"          => true,
		"rest_base"             => "gameguide_categories",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit"    => true,
	];

	register_taxonomy( "gameguide_categories", [ "gameguide" ], $args );


	// Spreadsheet Cats
	// ------------------------------------------------------------------------

	$labels = [
		'name'                  => 'Spreadsheet Cats',
		'singular_name'         => 'Spreadsheet Cat',
	];

	$args = [
		'label'                 => 'Spreadsheet Cats',
		'labels'                => $labels,
		'public'                => true,
		'publicly_queryable'    => true,
		'hierarchical'          => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'show_in_nav_menus'     => true,
		'query_var'             => true,
		'rewrite'               => [ 'slug' => 'spreadsheet_categories', 'with_front' => false, ],
		'show_admin_column'     => true,
		'show_in_rest'          => true,
		'rest_base'             => 'spreadsheet_categories',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
		'show_in_quick_edit'    => true,
	];

	register_taxonomy( 'spreadsheet_categories', [ 'spreadsheet' ], $args );
}

add_action( 'init', 'muffin_register_tax' );
