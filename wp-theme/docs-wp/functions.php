<?php

// Register CPT features
require_once( 'wp/theme-support.php' );

// Register CPTs & taxonomies
require_once( 'wp/register-cpt.php' );
require_once( 'wp/register-tax.php' );

// Prevent WP fixing URLs that aren't really broken
require_once( 'wp/disable-404-redirect.php' );
