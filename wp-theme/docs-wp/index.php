<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>CM Data Source</title>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
</head>
<body>

	<div class="page">
		<header>
			<h1>CM Data Source</h1>
		</header>

		<main>
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 14" width="100" height="100">
				<defs/>
				<path d="M2.75 11.09L0 8.18V4.15l2.63-2.08L5.27 0h5.46l2.64 2.07L16 4.15V8.18l-2.75 2.91L10.51 14H5.5zm8.9.37l.6-1.55-.68-.67-.68-.68-.82 1.33-.83 1.34-.54-1.37-.55-1.36-.95 1.36-.96 1.37-.48-1.37-.49-1.36-.76.7-.76.71.6 1.55.59 1.54h6.12zM15 6.3V4.1l-2.22-1.55L10.55 1h-5.1L3.22 2.56 1 4.1V8.5h14z"/>
			</svg>
		</main>

		<footer>
			<a href="https://codemuffin.com" target="_blank">codemuffin.com</a>
		</footer>
	</div>

</body>
</html>