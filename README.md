# Docs

SPA built from scratch. Made to understand JS framework design decisions.

Used on [codemuffin.com](https://codemuffin.com), pulling from the remote source [data.codemuffin.com](https://data.codemuffin.com/) via WordPress' REST API.

Features include:

- Templating (wi. `{{vars}}`)
- Routing (wi. title, meta, history, popstate)
- REST parsing
- JSON config
- Caching
- JWT support
- Developer tools

You can also change the live host to any WP site with a public API.


## HTML + JS

Styles uses Sass and uikit 2: [app.scss](https://bitbucket.org/codemuffin/docs-js/src/master/html-js/frontend/app/assets/src/styles/app/).

Scripts use custom JS and jQuery: [app.js](https://bitbucket.org/codemuffin/docs-js/src/master/html-js/frontend/app/assets/src/scripts/).

Markup is a single index.html file.


## WordPress

Theme includes CPT registration and a basic index.

ACF is supported, see acf-json in the theme directory. Requires the plugins [ACF](https://wordpress.org/plugins/advanced-custom-fields) and [ACF to REST API](https://wordpress.org/plugins/acf-to-rest-api).
